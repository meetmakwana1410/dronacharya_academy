<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('standard_id')->default(0)->unsigned()->comment('From table standards -> id');
            $table->smallInteger('subject_id')->default(0)->unsigned()->comment('From table subjects -> id');
            $table->timestamps();

            $table->index(['id','standard_id','subject_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standard_subjects');
    }
}
