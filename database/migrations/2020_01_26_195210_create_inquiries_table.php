<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable()->collation('utf8_general_ci');
            $table->string('primary_mobile_number', 20)->nullable()->collation('utf8_general_ci');
            $table->string('secondary_mobile_number', 20)->nullable()->collation('utf8_general_ci');
            $table->string('student_mobile_number', 20)->nullable()->collation('utf8_general_ci');
            $table->string('parents_email', 100)->nullable()->collation('utf8_general_ci');
            $table->string('students_email', 100)->nullable()->collation('utf8_general_ci');
            $table->string('residence_address', 255)->nullable()->collation('utf8_general_ci');
            $table->unsignedSmallInteger('school_id')->default(0)->comment('From table school -> id');
            $table->string('school_timings', 50)->nullable()->collation('utf8_general_ci');
            $table->unsignedSmallInteger('standard_id')->default(0)->comment('From table standard -> id');
            $table->string('school_type', 10)->nullable()->collation('utf8_general_ci')->comment('1-GSEB,2-CBSE');
            $table->text('remark', 10)->nullable()->collation('utf8_general_ci');
            $table->timestamps();
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Data, 1-Lead, 2-Confirm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}
