<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryFollowUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_follow_up', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inquiry_id')->default(0)->comment('From table inquiry -> id');
            $table->string('note',250)->nullable()->collation('utf8_general_ci');
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Unpublished, 1-Published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_follow_up');
    }
}
