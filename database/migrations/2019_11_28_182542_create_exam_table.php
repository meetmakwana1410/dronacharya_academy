<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('exam_category_id')->default(1)->comment('1-Weekly, 2-Midterm');
            $table->unsignedMediumInteger('branch_id')->default(0)->comment('From table user -> id');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->smallInteger('subject_id')->default(0)->unsigned()->comment('From table subjects -> id');
            $table->char('total_marks', 4)->default(0)->collation('utf8_general_ci');
            $table->date('exam_date')->nullable();
            $table->time('exam_time')->nullable();
            $table->date('to_exam_date')->nullable();
            $table->time('to_exam_time')->nullable();
            $table->string('exam_duration',12)->nullable();
            $table->string('chapter', 100)->nullable()->collation('utf8_general_ci');
            $table->smallInteger('teacher_id')->default(0)->comment('From table users -> id, Role 2');
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->tinyInteger('status')->default(0)->unsigned()->comment('0-Unpublished, 1-Published');

            $table->index(['id','exam_category_id','subject_id','batch_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam');
    }
}
