<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('school_name',100)->nullable()->collation('utf8_general_ci');
            $table->timestamps();
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Unpublished, 1-Published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school');
    }
}
