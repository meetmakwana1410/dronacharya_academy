<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('subject_name', 60)->nullable()->collation('utf8_general_ci');
            $table->string('subject_image', 255)->nullable()->collation('utf8_general_ci');
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->unsigned();

            $table->index(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
