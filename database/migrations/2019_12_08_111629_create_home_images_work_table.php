<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeImagesWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_work_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('home_work_id')->default(0)->comment('From table home_work -> id');
            $table->string('image', 250)->nullable()->collation('utf8_general_ci');
            $table->timestamps();

            $table->index(['id','home_work_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_work_images');
    }
}
