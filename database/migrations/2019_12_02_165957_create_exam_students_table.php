<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_id')->default(0)->comment('From table exam -> id');
            $table->unsignedInteger('student_id')->default(0)->comment('From table student -> id');
            $table->char('obtain_marks', 3)->default(0)->collation('utf8_general_ci');
            $table->char('percentage',3)->default(0)->collation('utf8_general_ci');
            $table->smallInteger('mark_fill_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->tinyInteger('status')->default(0)->unsigned()->comment('0-Pending, 1-Absent, 2-Present, 3-N/A');

            $table->index(['id','exam_id','student_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_students');
    }
}
