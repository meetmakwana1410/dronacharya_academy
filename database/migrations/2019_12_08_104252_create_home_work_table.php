<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_work', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->smallInteger('subject_id')->default(0)->unsigned()->comment('From table subjects -> id');
            $table->date('submission_date')->nullable();
            $table->string('document')->nullable()->collation('utf8_general_ci');
            $table->string('note', 250)->nullable()->collation('utf8_general_ci');
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Unpublished, 1-Published');

            $table->index(['id','batch_id', 'subject_id', 'created_by']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_work');
    }
}
