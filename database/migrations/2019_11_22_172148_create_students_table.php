<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('branch_id')->default(0)->comment('From table users -> id ROLE Branch');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->string('roll_number', 20)->nullable()->collation('utf8_general_ci');
            $table->string('student_name', 100)->nullable()->collation('utf8_general_ci');
            $table->string('student_mobile_number', 12)->nullable()->collation('utf8_general_ci');
            $table->string('student_email', 100)->nullable()->collation('utf8_general_ci');
            $table->date('student_date_of_birth')->nullable();
            $table->tinyInteger('gender')->default(1)->unsigned()->comment('1-Male,2-Female');
            $table->string('father_name', 100)->nullable()->collation('utf8_general_ci');
            $table->string('father_mobile_number', 12)->nullable()->collation('utf8_general_ci');
            $table->string('mother_mobile_number', 12)->nullable()->collation('utf8_general_ci');
            $table->string('landline_number', 20)->nullable()->collation('utf8_general_ci');
            $table->string('father_email', 100)->nullable()->collation('utf8_general_ci');
            $table->string('password', 250)->nullable()->collation('utf8_general_ci');
            $table->string('father_occupation', 100)->nullable()->collation('utf8_general_ci');
            $table->string('address', 250)->nullable()->collation('utf8_general_ci');
            $table->date('admission_date')->nullable();
            $table->smallInteger('school_id')->default(0)->unsigned()->comment('From table school -> id');
            $table->string('profile_image', 255)->nullable();
            $table->unsignedTinyInteger('device_type')->default(0)->comment('1-Android, 2-IOS');
            $table->string('device_token', 255)->nullable();
            $table->timestamps();
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Unpublished, 1-Published');

            $table->index(['id','batch_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
