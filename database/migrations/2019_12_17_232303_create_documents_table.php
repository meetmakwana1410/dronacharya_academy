<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->smallInteger('subject_id')->default(0)->unsigned()->comment('From table subjects -> id');
            $table->string('name',250)->nullable();
            $table->string('document',250)->nullable();
            $table->string('extension',10)->nullable();
            $table->text('note')->nullable()->collation('utf8_general_ci');
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->unsigned()->comment('0-Unpublished, 1-Published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
