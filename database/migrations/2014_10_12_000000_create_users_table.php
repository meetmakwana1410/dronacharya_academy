<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name',100);
            $table->string('email',150)->unique();
            $table->string('password');
            $table->string('contact_no',20)->nullable();
            $table->string('image')->nullable();
            $table->unsignedTinyInteger('device_type')->default(0)->comment('1-Android, 2-IOS');
            $table->string('device_token', 255)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->unsignedSmallInteger('created_by');
            $table->unsignedSmallInteger('updated_by');
            $table->unsignedTinyInteger('status')->default('1')->comment('1=>Active,0=> Inactive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
