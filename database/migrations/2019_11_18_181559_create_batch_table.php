<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedInteger('branch_id')->default(0)->comment('From table user table Branch Role');
            $table->string('batch_name', 30)->nullable()->collation('utf8_general_ci');
            $table->smallInteger('board_id')->default(0)->unsigned()->comment('From table board -> id');
            $table->smallInteger('standard_id')->default(0)->unsigned()->comment('From table standards -> id');
            $table->smallInteger('academic_year_id')->default(0)->unsigned()->comment('From table academic_year -> id');
            $table->smallInteger('medium_id')->default(0)->unsigned()->comment('From table medium -> id');
            $table->string('batch_code',50)->nullable()->collation('utf8_general_ci');
            $table->string('batch_image', 255)->nullable();
            $table->timestamps();
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->smallInteger('updated_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->tinyInteger('status')->default(1)->unsigned();

            $table->index(['id','board_id','standard_id','academic_year_id','medium_id']);
            //,'created_by'
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch');
    }
}
