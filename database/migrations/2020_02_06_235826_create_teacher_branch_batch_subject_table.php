<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherBranchBatchSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_branch_batch_subject', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('teacher_id')->default(0)->comment('From table user -> id');
            $table->unsignedSmallInteger('branch_id')->default(0)->comment('From table user table Branch Role');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->unsignedMediumInteger('subject_id')->default(0)->comment('From table subjects -> id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_branch_batch_subject');
    }
}
