<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('user_type')->default(1)->unsigned()->comment('1-Student,2-Teacher');
            $table->tinyInteger('type')->default(0)->unsigned()->comment('1-Normal, 2-Exam, 3-HomeWork,4-General,5-Attendance');
            $table->unsignedInteger('master_id')->default(0);
            $table->unsignedInteger('user_id')->default(0)->comment('From table students -> id');
            $table->string('message',255)->nullable()->collation('utf8_general_ci');
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->tinyInteger('status')->default(0)->unsigned()->comment('0-Unread, 1-Read');
            
            $table->index(['id','master_id','user_id','created_by']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
