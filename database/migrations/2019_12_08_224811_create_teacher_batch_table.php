<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_batch', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedSmallInteger('teacher_id')->default(0)->comment('From table user -> id');
            $table->unsignedMediumInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->timestamps();

            $table->index(['id','teacher_id','batch_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_batch');
    }
}
