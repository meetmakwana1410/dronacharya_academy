<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('board_name', 12)->nullable()->collation('utf8_general_ci');
            $table->string('full_name', 100)->nullable()->collation('utf8_general_ci');
            $table->timestamps();
            $table->tinyInteger('status')->default(1)->unsigned();

            $table->index(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board');
    }
}
