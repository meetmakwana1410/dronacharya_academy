<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('branch_id')->default(0)->comment('From table user table Branch Role');
            $table->unsignedSmallInteger('batch_id')->default(0)->comment('From table batch -> id');
            $table->unsignedSmallInteger('subject_id')->default(0)->comment('From table subject -> id');
            $table->unsignedInteger('student_id')->default(0)->comment('From table student -> id');
            $table->date('date')->nullable();
            $table->smallInteger('created_by')->default(0)->unsigned()->comment('From table users -> id');
            $table->timestamps();
            $table->unsignedTinyInteger('status')->default('1')->comment('1=>Present,0=>Absent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attendance');
    }
}
