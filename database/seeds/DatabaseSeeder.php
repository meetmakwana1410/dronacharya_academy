<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(SubjectsTableSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(RolesTableSeeder::class);
		$this->call(RoleusersTableSeeder::class);
		$this->call(MediumTableSeeder::class);
		$this->call(AcademicYearTableSeeder::class);
		$this->call(BoardTableSeeder::class);
		$this->call(StandardsTableSeeder::class);
		$this->call(ExamTypeTableSeeder::class);
		$this->call(StandardSubjectsTableSeeder::class);
		$this->call(SchoolTableSeeder::class);
    }
}
