<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            array(
                'name' => 'Dronacharya Academy - Admin',
                'email' => 'admin@dronacharyaacademy.com',
                'password' => Hash::make('123123'),
                'contact_no' => '9876543210',
                'image' => '',
                'status' => '1'
            ),
            [
                'name' => 'Swagat Status -  Branch',
                'email' => 'swagat@dronacharyaacademy.com',
                'password' => Hash::make('123123'),
                'contact_no' => '9999999999',
                'image' => '',
                'status' => '1'
            ],
            [
                'name' => 'Kudasan - Gandhinagar',
                'email' => 'kudasan@dronacharyaacademy.com',
                'password' => Hash::make('123123'),
                'contact_no' => '8888888888',
                'image' => '',
                'status' => '1'
            ],
            [
                'name' => 'Gandhinagar - Ahmedabad',
                'email' => 'gandhinagar@dronacharyaacademy.com',
                'password' => Hash::make('123123'),
                'contact_no' => '7777777777',
                'image' => '',
                'status' => '1'
            ]
        ]);
    }
}
