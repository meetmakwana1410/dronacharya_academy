<?php

use Illuminate\Database\Seeder;

class StandardSubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('standard_subjects')->insert([
            ['standard_id'=>1, 'subject_id'=>1],
            ['standard_id'=>2, 'subject_id'=>1],
            ['standard_id'=>3, 'subject_id'=>1],
            ['standard_id'=>4, 'subject_id'=>1],
            ['standard_id'=>5, 'subject_id'=>1],
            ['standard_id'=>1, 'subject_id'=>2],
            ['standard_id'=>2, 'subject_id'=>2],
            ['standard_id'=>3, 'subject_id'=>2],
            ['standard_id'=>4, 'subject_id'=>2],
            ['standard_id'=> 5, 'subject_id'=>2],
            ['standard_id'=> 1, 'subject_id'=>3],
            ['standard_id'=> 2, 'subject_id'=>3],
            ['standard_id'=> 1, 'subject_id'=>4],
            ['standard_id'=> 2, 'subject_id'=>4],
            ['standard_id'=> 3, 'subject_id'=>4],
            ['standard_id'=> 1, 'subject_id'=>5],
            ['standard_id'=> 2, 'subject_id'=>5],
            ['standard_id'=> 3, 'subject_id'=>5],
            ['standard_id'=> 4, 'subject_id'=>6],
            ['standard_id'=> 5, 'subject_id'=>6],
            ['standard_id'=> 4, 'subject_id'=>7],
            ['standard_id'=> 5, 'subject_id'=>7],
            ['standard_id'=> 4, 'subject_id'=>8],
            ['standard_id'=> 5, 'subject_id'=>8],
        ]);
    }
}
