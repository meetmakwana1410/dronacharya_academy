<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'role_name' => 'Super admin',
                'role' => 'super-admin',
                'status' => '1'
            ],
            [
                'role_name' => 'Branch Manager',
                'role' => 'branch-admin',
                'status' => '1'
            ],
            [
                'role_name' => 'Teacher',
                'role' => 'teacher',
                'status' => '1'
            ]
        ]);
    }
}
