<?php

use Illuminate\Database\Seeder;

class MediumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medium')->insert([
            ['medium_name' => 'English'],
            ['medium_name' => 'Gujarati']
        ]);
    }
}
