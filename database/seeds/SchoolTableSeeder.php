<?php

use Illuminate\Database\Seeder;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school')->insert([
            ['school_name' => 'School of Achiever'],
            ['school_name' => 'Radiant School Of Science'],
            ['school_name' => 'St. Xavier"s High School, Gandhinagar'],
            ['school_name' => 'Baps Swaminarayan Vidyamandir'],
            ['school_name' => 'Mount Carmel High School'],
        ]);
    }
}
