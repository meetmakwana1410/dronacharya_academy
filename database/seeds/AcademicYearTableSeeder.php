<?php

use Illuminate\Database\Seeder;

class AcademicYearTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_year')->insert([
            ['year' => '2017'],
            ['year' => '2018'],
            ['year' => '2019'],
            ['year' => '2020'],
        ]);
    }
}
