<?php

use Illuminate\Database\Seeder;

class StandardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('standards')->insert([
            ['standard_name' => '8th'],
            ['standard_name' => '9th'],
            ['standard_name' => '10th'],
            ['standard_name' => '11th'],
            ['standard_name' => '12th'],
        ]);
    }
}
