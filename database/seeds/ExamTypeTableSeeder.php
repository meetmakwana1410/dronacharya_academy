<?php

use Illuminate\Database\Seeder;

class ExamTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exam_type')->insert([
            ['exam_type_name' => 'Weekly'],
            ['exam_type_name' => 'Midterm'],
        ]);
    }
}
