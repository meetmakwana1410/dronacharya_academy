<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            ['subject_name' => 'English','subject_image'=>'1581268173.png'],
            ['subject_name' => 'Mathematics','subject_image'=>'1581268223.png'],
            ['subject_name' => 'Science','subject_image'=>'1581268246.png'],
            ['subject_name' => 'Social Science','subject_image'=>'1581268290.png'],
            ['subject_name' => 'Hindi','subject_image'=>'1581268173.png'],
            ['subject_name' => 'Physics','subject_image'=>'1581268335.png'],
            ['subject_name' => 'Chemistry','subject_image'=>'1581268354.png'],
            ['subject_name' => 'Biology','subject_image'=>'1581268398.png'],
        ]);
    }
}
