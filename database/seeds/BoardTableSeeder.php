<?php

use Illuminate\Database\Seeder;

class BoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('board')->insert([
            ['board_name' => 'CBSE','full_name'=>'Central Board of Secondary Education'],
            ['board_name' => 'GSEB','full_name'=>'Gujarat Secondary and Higher Secondary Education Board'],
        ]);
    }
}
