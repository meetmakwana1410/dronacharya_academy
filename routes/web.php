<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {

    return redirect()->guest('login');
})->name('home');

Route::get('getLocale', function () {
	//dd(app()->getLocale());
});

Route::get('command', function () {
    \Artisan::call('view:clear');
    \Artisan::call('cache:clear');
    //\Artisan::call('config:cache');
    //\Artisan::call('dump-autoload');
    dd("Done");
});

Route::get('privacy-policy', function () {
   return view('privacy-policy');
});

Route::get('migrate', function () {
    \Artisan::call('migrate');
    dd("Done");
});

Route::get('migratefresh', function () {
    Artisan::call('migrate:fresh', ['--seed'=>true]);
    dd("Done");
});

Route::get('migraterollbak', function () {
    \Artisan::call('migrate:rollback');
    dd("Done");
});


Route::post('/forgot-password', [
    'as' => 'send-reset-link', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);

Route::get('/password/reset/{token}', [
    'as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

Route::post('/password/reset', [
    'as' => 'reset.password.post', 'uses' => 'Auth\ResetPasswordController@reset'
]);



include 'superadmin_route.php';
include 'front_web.php';