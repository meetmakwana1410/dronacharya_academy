<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::group(['middleware' => ['auth','roles:super-admin' OR 'roles:branch-admin','prevent-back-history']], function () {

    Route::prefix('dashboard')->group(function(){
        Route::any('/', [
            'as' => 'dashboard.list', 'uses' => 'AdminPanel\DashboardController@index'
        ]);
    });
    
    

    /*********** CHANGE PASSWORD ***********/
    Route::get('/changepassword', [
        'as' => 'change.password.get', 'uses' => 'AdminPanel\ChangePasswordController@index'
    ]);   

    Route::post('/changepassword', [
        'as' => 'change.password.post', 'uses' => 'AdminPanel\ChangePasswordController@changePassword'
    ]);
   
    /*********** ACADEMIC YEAR ROUTE ****************/
    Route::prefix('academic_year')->group(function(){
        Route::any('/', [
            'as' => 'academic_year.list', 'uses' => 'AdminPanel\AcademicYearController@index'
        ]);

        Route::get('add', [
            'as' => 'academic_year.add', 'uses' => 'AdminPanel\AcademicYearController@create'
        ]);

        Route::post('add', [
            'as' => 'academic_year.add', 'uses' => 'AdminPanel\AcademicYearController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'academic_year.edit', 'uses' => 'AdminPanel\AcademicYearController@create'
        ])->where('post_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'academic_year.edit', 'uses' => 'AdminPanel\AcademicYearController@store'
        ])->where('post_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'academic_year.activedeactive', 'uses' => 'AdminPanel\AcademicYearController@changeStatus'
        ])->where('post_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'academic_year.delete', 'uses' => 'AdminPanel\AcademicYearController@destroy'
        ])->where('post_id', '[0-9]+')->middleware('signed');
    });

    /*********** NOTIFICATIONS ROUTE ****************/
    Route::prefix('notifications')->group(function(){
        Route::any('/', [
            'as' => 'notifications', 'uses' => 'AdminPanel\NotificationsController@index'
        ]);

        Route::post('add', [
            'as' => 'notifications.add', 'uses' => 'AdminPanel\NotificationsController@store'
        ]);

        Route::post('getBatchStudent', [
            'as' => 'notifications.getBatchStudent', 'uses' => 'AdminPanel\NotificationsController@getBatchStudent'
        ]);

    });

    /*********** MEDIUM ROUTE ****************/
    Route::prefix('medium')->group(function(){
        Route::any('/', [
            'as' => 'medium.list', 'uses' => 'AdminPanel\MediumController@index'
        ]);

        Route::get('add', [
            'as' => 'medium.add', 'uses' => 'AdminPanel\MediumController@create'
        ]);

        Route::post('add', [
            'as' => 'medium.add', 'uses' => 'AdminPanel\MediumController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'medium.edit', 'uses' => 'AdminPanel\MediumController@create'
        ])->where('medium_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'medium.edit', 'uses' => 'AdminPanel\MediumController@store'
        ])->where('medium_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'medium.activedeactive', 'uses' => 'AdminPanel\MediumController@changeStatus'
        ])->where('medium_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'medium.delete', 'uses' => 'AdminPanel\MediumController@destroy'
        ])->where('medium_id', '[0-9]+')->middleware('signed');
    });

    /*********** SCHOOL ROUTE ****************/
    Route::prefix('school')->group(function(){
        Route::any('/', [
            'as' => 'school.list', 'uses' => 'AdminPanel\SchoolController@index'
        ]);

        Route::get('add', [
            'as' => 'school.add', 'uses' => 'AdminPanel\SchoolController@create'
        ]);

        Route::post('add', [
            'as' => 'school.add', 'uses' => 'AdminPanel\SchoolController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'school.edit', 'uses' => 'AdminPanel\SchoolController@create'
        ])->where('school_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'school.edit', 'uses' => 'AdminPanel\SchoolController@store'
        ])->where('school_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'school.activedeactive', 'uses' => 'AdminPanel\SchoolController@changeStatus'
        ])->where('school_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'school.delete', 'uses' => 'AdminPanel\SchoolController@destroy'
        ])->where('medium_id', '[0-9]+')->middleware('signed');
    });

    /*********** MEDIUM ROUTE ****************/
    Route::prefix('exam_type')->group(function(){
        Route::any('/', [
            'as' => 'exam_type.list', 'uses' => 'AdminPanel\ExamTypeController@index'
        ]);

        Route::get('add', [
            'as' => 'exam_type.add', 'uses' => 'AdminPanel\ExamTypeController@create'
        ]);

        Route::post('add', [
            'as' => 'exam_type.add', 'uses' => 'AdminPanel\ExamTypeController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'exam_type.edit', 'uses' => 'AdminPanel\ExamTypeController@create'
        ])->where('exam_type_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'exam_type.edit', 'uses' => 'AdminPanel\ExamTypeController@store'
        ])->where('exam_type_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'exam_type.activedeactive', 'uses' => 'AdminPanel\ExamTypeController@changeStatus'
        ])->where('exam_type_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'exam_type.delete', 'uses' => 'AdminPanel\ExamTypeController@destroy'
        ])->where('exam_type_id', '[0-9]+')->middleware('signed');
    });

    /*********** BOARD ROUTE ****************/
    Route::prefix('board')->group(function(){
        Route::any('/', [
            'as' => 'board.list', 'uses' => 'AdminPanel\BoardController@index'
        ]);

        Route::get('add', [
            'as' => 'board.add', 'uses' => 'AdminPanel\BoardController@create'
        ]);

        Route::post('add', [
            'as' => 'board.add', 'uses' => 'AdminPanel\BoardController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'board.edit', 'uses' => 'AdminPanel\BoardController@create'
        ])->where('board_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'board.edit', 'uses' => 'AdminPanel\BoardController@store'
        ])->where('board_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'board.activedeactive', 'uses' => 'AdminPanel\BoardController@changeStatus'
        ])->where('board_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'board.delete', 'uses' => 'AdminPanel\BoardController@destroy'
        ])->where('board_id', '[0-9]+')->middleware('signed');
    });

    /*********** STANDARDS ROUTE ****************/
    Route::prefix('standards')->group(function(){
        Route::any('/', [
            'as' => 'standards.list', 'uses' => 'AdminPanel\StandardsController@index'
        ]);

        Route::get('add', [
            'as' => 'standards.add', 'uses' => 'AdminPanel\StandardsController@create'
        ]);

        Route::post('add', [
            'as' => 'standards.add', 'uses' => 'AdminPanel\StandardsController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'standards.edit', 'uses' => 'AdminPanel\StandardsController@create'
        ])->where('standards_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'standards.edit', 'uses' => 'AdminPanel\StandardsController@store'
        ])->where('standards_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'standards.delete', 'uses' => 'AdminPanel\StandardsController@destroy'
        ])->where('standards_id', '[0-9]+')->middleware('signed');
    });

    /*********** SUBJECTS ROUTE ****************/
    Route::prefix('subject')->group(function(){
        Route::any('/', [
            'as' => 'subject.list', 'uses' => 'AdminPanel\SubjectsController@index'
        ]);

        Route::get('add', [
            'as' => 'subject.add', 'uses' => 'AdminPanel\SubjectsController@create'
        ]);

        Route::post('add', [
            'as' => 'subject.add', 'uses' => 'AdminPanel\SubjectsController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'subject.edit', 'uses' => 'AdminPanel\SubjectsController@create'
        ])->where('subjects_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'subject.edit', 'uses' => 'AdminPanel\SubjectsController@store'
        ])->where('subjects_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'subject.delete', 'uses' => 'AdminPanel\SubjectsController@destroy'
        ])->where('subjects_id', '[0-9]+')->middleware('signed');
    });

    /*********** BATCH ROUTE ****************/
    Route::prefix('batch')->group(function(){
        Route::any('/', [
            'as' => 'batch.list', 'uses' => 'AdminPanel\BatchController@index'
        ]);

        Route::get('add', [
            'as' => 'batch.add', 'uses' => 'AdminPanel\BatchController@create'
        ]);

        Route::post('add', [
            'as' => 'batch.add', 'uses' => 'AdminPanel\BatchController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'batch.edit', 'uses' => 'AdminPanel\BatchController@create'
        ])->where('batch_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'batch.edit', 'uses' => 'AdminPanel\BatchController@store'
        ])->where('batch_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'batch.activedeactive', 'uses' => 'AdminPanel\BatchController@changeStatus'
        ])->where('batch_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'batch.delete', 'uses' => 'AdminPanel\BatchController@destroy'
        ])->where('batch_id', '[0-9]+')->middleware('signed');

        Route::get('students/{id}', [
            'as' => 'batch.getstudent', 'uses' => 'AdminPanel\BatchController@getStudent'
        ])->where('batch_id', '[0-9]+');
    });

    /*********** STUDENT ROUTE ****************/
    Route::prefix('students')->group(function(){
        Route::any('/', [
            'as' => 'students.list', 'uses' => 'AdminPanel\StudentsController@index'
        ]);

        Route::get('add', [
            'as' => 'students.add', 'uses' => 'AdminPanel\StudentsController@create'
        ]);

        Route::post('add', [
            'as' => 'students.add', 'uses' => 'AdminPanel\StudentsController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'students.edit', 'uses' => 'AdminPanel\StudentsController@create'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'students.edit', 'uses' => 'AdminPanel\StudentsController@store'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'students.activedeactive', 'uses' => 'AdminPanel\StudentsController@changeStatus'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'students.delete', 'uses' => 'AdminPanel\StudentsController@destroy'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('getBatchSubject', [
            'as' => 'students.getBatchSubject', 'uses' => 'AdminPanel\StudentsController@getBatchSubject'
        ]);
    });

    /*********** TEACHER ROUTE ****************/
    Route::prefix('teacher')->group(function(){
        Route::any('/', [
            'as' => 'teacher.list', 'uses' => 'AdminPanel\TeacherController@index'
        ]);

        Route::get('add', [
            'as' => 'teacher.add', 'uses' => 'AdminPanel\TeacherController@create'
        ]);

        Route::post('add', [
            'as' => 'teacher.add', 'uses' => 'AdminPanel\TeacherController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'teacher.edit', 'uses' => 'AdminPanel\TeacherController@create'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'teacher.edit', 'uses' => 'AdminPanel\TeacherController@store'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'teacher.activedeactive', 'uses' => 'AdminPanel\TeacherController@changeStatus'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'teacher.delete', 'uses' => 'AdminPanel\TeacherController@destroy'
        ])->where('student_id', '[0-9]+')->middleware('signed');

        Route::post('getBatchSubject', [
            'as' => 'teacher.getBatchSubject', 'uses' => 'AdminPanel\TeacherController@getBatchSubject'
        ]);

        Route::post('/deleteRecord', [
            'as' => 'teacher.deleteRecord', 'uses' => 'AdminPanel\TeacherController@deleteRecord'
        ]);
    });

    /*********** TEACHER ROUTE ****************/
    Route::prefix('exam')->group(function(){
        Route::any('/', [
            'as' => 'exam.list', 'uses' => 'AdminPanel\ExamController@index'
        ]);

        Route::get('add', [
            'as' => 'exam.add', 'uses' => 'AdminPanel\ExamController@create'
        ]);

        Route::post('add', [
            'as' => 'exam.add', 'uses' => 'AdminPanel\ExamController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'exam.edit', 'uses' => 'AdminPanel\ExamController@create'
        ])->where('exam_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'exam.edit', 'uses' => 'AdminPanel\ExamController@store'
        ])->where('exam_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'exam.activedeactive', 'uses' => 'AdminPanel\ExamController@changeStatus'
        ])->where('exam_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'exam.delete', 'uses' => 'AdminPanel\ExamController@destroy'
        ])->where('exam_id', '[0-9]+')->middleware('signed');

        Route::post('getBatchSubject', [
            'as' => 'exam.getBatchSubject', 'uses' => 'AdminPanel\ExamController@getBatchSubject'
        ]);
        
        Route::post('submitObtainMarks', [
            'as' => 'exam.submitObtainMarks', 'uses' => 'AdminPanel\ExamController@submitObtainMarks'
        ]);

        Route::any('getexamstudent/{id}', [
            'as' => 'exam.getexamstudent', 'uses' => 'AdminPanel\ExamController@getExamStudent'
        ])->where('exam_id', '[0-9]+');


        Route::post('submitMark/{id}', [
            'as' => 'exam.submitMark', 'uses' => 'AdminPanel\ExamController@submitMark'
        ])->where('exam_id', '[0-9]+');

        Route::post('getSubjectTeacher', [
            'as' => 'exam.getSubjectTeacher', 'uses' => 'AdminPanel\ExamController@getSubjectTeacher'
        ]);
    });

    /*********** DOCUMENTS ROUTE ****************/
    Route::prefix('documents')->group(function(){
        Route::any('/', [
            'as' => 'documents.list', 'uses' => 'AdminPanel\DocumentsController@index'
        ]);

        Route::get('add', [
            'as' => 'documents.add', 'uses' => 'AdminPanel\DocumentsController@create'
        ]);

        Route::post('allDocAjax', [
            'as' => 'documents.allDocAjax', 'uses' => 'AdminPanel\DocumentsController@allDocAjax'
        ]);

        Route::post('add', [
            'as' => 'documents.add', 'uses' => 'AdminPanel\DocumentsController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'documents.edit', 'uses' => 'AdminPanel\DocumentsController@create'
        ])->where('documents_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'documents.edit', 'uses' => 'AdminPanel\DocumentsController@store'
        ])->where('documents_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'documents.activedeactive', 'uses' => 'AdminPanel\DocumentsController@changeStatus'
        ])->where('documents_id', '[0-9]+')->middleware('signed');
        
        Route::post('delete/{id}', [
            'as' => 'documents.delete', 'uses' => 'AdminPanel\DocumentsController@destroy'
        ])->where('documents_id', '[0-9]+')->middleware('signed');
    });

    /*********** HOMEWORK ROUTE ****************/
    Route::prefix('homework')->group(function(){
        Route::any('/', [
            'as' => 'homework.list', 'uses' => 'AdminPanel\HomeworkController@index'
        ]);

        Route::get('gethomework/{id}', [
            'as' => 'homework.gethomework', 'uses' => 'AdminPanel\HomeworkController@getHomework'
        ])->where('homework_id', '[0-9]+');
    });

    /*********** BRANCH ROUTE ****************/
    Route::prefix('branch')->group(function(){
        Route::any('/', [
            'as' => 'branch.list', 'uses' => 'AdminPanel\BranchController@index'
        ]);

        Route::get('add', [
            'as' => 'branch.add', 'uses' => 'AdminPanel\BranchController@create'
        ]);

        Route::post('add', [
            'as' => 'branch.add', 'uses' => 'AdminPanel\BranchController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'branch.edit', 'uses' => 'AdminPanel\BranchController@create'
        ])->where('branch_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'branch.edit', 'uses' => 'AdminPanel\BranchController@store'
        ])->where('branch_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'branch.activedeactive', 'uses' => 'AdminPanel\BranchController@changeStatus'
        ])->where('branch_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'branch.delete', 'uses' => 'AdminPanel\BranchController@destroy'
        ])->where('branch_id', '[0-9]+')->middleware('signed');
    });

    /*********** Inquiry ROUTE ****************/
    Route::prefix('inquiry')->group(function(){
        Route::any('/', [
            'as' => 'inquiry.list', 'uses' => 'AdminPanel\InquiryController@index'
        ]);

        Route::get('add', [
            'as' => 'inquiry.add', 'uses' => 'AdminPanel\InquiryController@create'
        ]);

        Route::post('add', [
            'as' => 'inquiry.add', 'uses' => 'AdminPanel\InquiryController@store'
        ]);

        Route::get('edit/{id}', [
            'as' => 'inquiry.edit', 'uses' => 'AdminPanel\InquiryController@create'
        ])->where('inquiry_id', '[0-9]+')->middleware('signed');

        Route::post('edit/{id}', [
            'as' => 'inquiry.edit', 'uses' => 'AdminPanel\InquiryController@store'
        ])->where('inquiry_id', '[0-9]+')->middleware('signed');

        Route::post('activedeactive/{id}', [
            'as' => 'inquiry.activedeactive', 'uses' => 'AdminPanel\InquiryController@changeStatus'
        ])->where('inquiry_id', '[0-9]+')->middleware('signed');

        Route::post('delete/{id}', [
            'as' => 'inquiry.delete', 'uses' => 'AdminPanel\InquiryController@destroy'
        ])->where('inquiry_id', '[0-9]+')->middleware('signed');

        Route::get('show/{id}', [
            'as' => 'inquiry.show', 'uses' => 'AdminPanel\InquiryController@show'
        ])->where('inquiry_id', '[0-9]+');

        Route::post('inquiry-follow-up', [
            'as' => 'inquiry.inquiry-follow-up', 'uses' => 'AdminPanel\InquiryController@submitInquiryFollowUp'
        ]);
    });
});