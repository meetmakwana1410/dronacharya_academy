<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () 
{
	/*** USERS ****/
	Route::post('parentlogin', 'UserController@login');
	Route::post('userprofile', 'UserController@userProfile');
	//Route::get('logout', 'LoginController@logout');
	Route::post('changepassword', 'UserController@changePassword');
	Route::post('student-logout', 'UserController@studentLogout');
	Route::post('forgetpassword', 'UserController@forgetPassword');
	Route::post('removeprofileimage', 'UserController@removeProfileImage');
	Route::post('editprofile', 'UserController@editProfile');
	
	/*** DOCUMETNS ****/
	Route::post('userdocumetns', 'UserController@userDocumetns');

	/*** ATTENDANCE ****/
	Route::post('user-attendance', 'UserController@userAttendance');
	
	/*** MASTERS ****/
	Route::post('notificationlist', 'MasterController@notificationList');
	Route::post('timtable', 'MasterController@timTable');
	Route::post('getversiondetails', 'MasterController@getVersionDetails');
	Route::post('getteacherlist', 'MasterController@getTeacherList');
	Route::post('getsubjectslist', 'MasterController@getSubjectsList');
	Route::get('getparentsversion', 'MasterController@getParentsVersion');
	Route::get('get-exam-type-list', 'MasterController@getExamTypeList');
	

	
	/*** DASHBOARD ****/
	Route::post('parenstsdashboard', 'DashboardController@parenstsDashboard');
		

	/*** EXAM ****/
	Route::post('examlist', 'ExamController@examList');
	Route::post('examdetails', 'ExamController@examDetails');

	/*** HOMW WORK ****/
	Route::post('homeworklist', 'HomeworkController@list');
	Route::post('homeworkdetails', 'HomeworkController@homeWorkDetails');
	
	////////////// TEACHER //////////////

	/*** LOGIN ****/
	Route::post('teacherlogin', 'TeacherController@login');
	Route::post('teacher-logout', 'TeacherController@teacherLogout');
	Route::post('teacherprofile', 'TeacherController@teacherProfile');
	Route::post('teachereditprofile', 'TeacherController@teacherEditProfile');
	Route::post('teacherbatch', 'TeacherController@teacherBatch');
	Route::post('teacherbatchsubject', 'TeacherController@teacherBatchSubject');
	Route::post('teacherchangepassword', 'TeacherController@teacherChangePassword');

	/*** MASTER ****/
	Route::post('getteachermasterlist', 'MasterController@getTeacherMasterList');
	Route::post('get-batch-students', 'MasterController@getBatchStudentsList');
	Route::post('getteachernotification', 'MasterController@getTeacherNotification');
	Route::get('getteacherversion', 'MasterController@getTeacherVersion');

	/*** HOME WORK ****/
	Route::post('techaddedithomework', 'teacher\TeacherHoweworkController@addEditHomeWork');
	Route::post('teachhomeworklist', 'teacher\TeacherHoweworkController@homeWorkList');

	/*** Teacher Attendance ****/
	Route::post('techaddedithomework', 'teacher\TeacherHoweworkController@addEditHomeWork');
	Route::post('teachhomeworklist', 'teacher\TeacherHoweworkController@homeWorkList');
	
	Route::post('removehomeworkimage', 'teacher\TeacherHoweworkController@removeHomeWorkImage');
	Route::post('deletehomework', 'teacher\TeacherHoweworkController@deleteHomeWork');
	
	Route::post('teacherexamlist', 'teacher\TeacherExamController@teacherExamList');

	/*** Teacher Attendance ****/
	Route::post('submit-attendance', 'teacher\TeacherAttendanceController@submitAttendance');
});