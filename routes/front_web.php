<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/************************** Frontend routes : START **************************/
Route::group(['namespace' => 'Front'], function () {
   
    // Home Page
    Route::get('/', [
        'as' => 'home', 'uses' => 'HomeController@index'
    ]);
});
/************************** Frontend routes : END **************************/