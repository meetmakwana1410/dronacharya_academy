<!DOCTYPE html>
<html>
    <head>
        <title>501 not implemented</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'index.css')}}">
        @include('admin.common.favicon')
    </head>
    <body class="not-found-page">
        <img src="{{ asset(IMAGES_FOLDER_URL.'logo.svg') }}" style="width: 30%;" alt="logo" />
        <ul class="err-msg-ul">
            <li>
                <p class="big-text">Oops...</p>
            </li>
            <li>
                <p class="err-msg">LOOKS LIKE SOMETHING WAS WRONG<br> WE’RE WORKING ON IT</p>
            </li>
        </ul>
        <figure>
            <img src="{{ asset(IMAGES_FOLDER_URL.'butterfly.png') }}" alt="404" class="img-fluid notfound-img" />
            <figcaption>501<small>not implemented</small></figcaption>		
        </figure>
        <a href="{{route('home')}}" class="btn btn-custom font-size-sm btn-secondary-1 go-back">go back</a>
    </body>
</html>