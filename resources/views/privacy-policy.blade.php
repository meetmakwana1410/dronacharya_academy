<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #4f5152;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                line-height: 25px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            

            <div class="content">
                <div class="title m-b-md">
                    Privacy Policy
                </div>

                <p>This privacy policy sets out how The Dronacharya Academy uses and protects any information that you give to The Dronacharya Academy when you use this website.</p>

                <p>This Privacy Policy regulates the types of information or data shared by you and/or collected from you in the course of your usage of the website and/or the Services. The Dronacharya Academy at its fullest endeavor committed to you and ensures you that your privacy is protected. The Dronacharya Academy may change this policy from time to time by updating this page at any time and without any notice hence,  The Dronacharya Academy encourages you to visit this page periodically to review the current terms and conditions. </p>

                <p>It may be possible that the other users (including unauthorized users) may post offensive materials, obscene, pornography, uncultured images on the website. Further possible that the other user may obtain your personal information by using the website that may cause harassment or injure you, The Dronacharya Academy does not allow such unauthorized uses, notwithstanding the foregoing you acknowledge and agree that The Dronacharya Academy is not responsible or liable for the misuse of any personal information that you publicly disclose on the website. Hence, kindly apply carefulness about the information disclosed publically while using the website. </p>

                <h2>Security</h2>

                <p>The Dronacharya Academy shall at its fullest endeavor ensure you safeguard the confidentiality of your Personal Information, however, the transmissions made via the Internet cannot be made completely secure by the website guards. Voluntarily you agreed and acknowledged that the The Dronacharya Academy shall not be responsible for the disclosure of any Information due to errors in transmission or any unauthorized acts of third parties. We should always endeavor to take all precautions and appropriate security measures to safeguard you against any unauthorized access to or unauthorized alteration, disclosure, or destruction of any information and/or data. The said process includes internal reviews of our information/data collection, storage and processing practices, and security measures, including appropriate encryption and physical security measures to protect against any unauthorized access to systems where we store personal information/data. Notwithstanding, we cannot guarantee or warranty the complete security of our information database and we shall not be held responsible for any loss, damage or misuse of the Information caused to you.</p>
            </div>
        </div>
    </body>
</html>
