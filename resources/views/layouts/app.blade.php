<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title') - {{ config('app.name', 'Ran Sarovar') }}</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset(CSS_FOLDER_URL.'bootstrap-multiselect.css')}}"/>
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'bootstrap.min.css')}}">
    @yield('page_css')
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'font-awesome.min.css')}}">    
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'index.css')}}">
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'custom.css')}}">
    <script type="text/javascript"  src="{{asset(JS_FOLDER_URL.'sweetalert.min.js')}}"></script>


    @include('admin.common.favicon')
</head>

<body>
    
    @include('admin.common.loaders.fullscreen')
    <!-- Header Start -->
    @include('admin.common.header')
    <!-- Header End -->
    <!-- Main Navigation Start -->
    @include('admin.common.sidebar')
    <!-- Main Navigation End -->
    <div class="main-content">
        <div class="client-information">
            @yield('content')
        </div>
    </div> 
    
    <!-- The Modal -->
    <div class="modal modal-employee-deactive fade" id="dynamic_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal body -->
                <div class="modal-body">
                    <h4 class="title"></h4>
                    <div id="dynamic_modal_content"></div>
                    <div class="form-btn-group dynamic-confirm-btn">
                        <button class="btn btn-custom btn-theme font-size-sm primary_dynamic_btn">@lang('common.label.confirm')</button>
                        <button type="button" class="btn btn-custom btn-secondary-2 font-size-sm secondary_dynamic_btn" data-dismiss="modal">@lang('common.label.cancel')</button>  
                    </div>

                </div>
            </div>
        </div>
    </div>
    
    
    @yield('content_footer')
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->    
    <script type="text/javascript"  src="{{asset(JS_FOLDER_URL.'jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset(JS_FOLDER_URL.'popper.min.js')}}"></script>    
    <script type="text/javascript" src="{{asset(JS_FOLDER_URL.'bootstrap-multiselect.min.js')}}"></script>
    <script type="text/javascript"  src="{{asset(JS_FOLDER_URL.'bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset(JS_FOLDER_URL.'jquery.slimscroll.js')}}"></script>
    <script src="{{asset(JS_FOLDER_URL.'parsley.js')}}"></script>
    <script type="text/javascript">
        var BASE_URL ='<?php echo url('/').'/'; ?>';
    </script>
    <script src="{{asset(JS_FOLDER_URL.'common.js')}}"></script>
    <script src="{{asset(JS_FOLDER_URL.'super_admin_custom.js')}}"></script>
    @yield('page_script')
</body>
</html>