<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title') - {{ config('app.name', 'Ran Sarovar') }}</title>  
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'index.css')}}">
    <link rel="stylesheet" href="{{asset(CSS_FOLDER_URL.'custom.css')}}">
    @yield('page_css')

    @include('admin.common.favicon')
</head>

<body>
    @include('admin.common.loaders.fullscreen')
    @yield('content')
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->    
    <script type="text/javascript"  src="{{asset(JS_FOLDER_URL.'jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset(JS_FOLDER_URL.'popper.min.js')}}"></script>    
    <script type="text/javascript"  src="{{asset(JS_FOLDER_URL.'bootstrap.min.js')}}"></script>
    <script src="{{asset(JS_FOLDER_URL.'parsley.js')}}"></script>
    <script type="text/javascript">
        var BASE_URL ='<?php echo url('/').'/'; ?>';
    </script>
    <script src="{{asset(JS_FOLDER_URL.'common.js')}}"></script>
    
    @yield('page_script')
</body>
</html>