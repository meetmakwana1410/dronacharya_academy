<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Use the latest (edge) version of IE rendering engine -->
        <title>Welcome</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
    </head>
    <body width="100%" height="100%" bgcolor="#e0e0e0" style="margin: 0;" yahoo="yahoo">
        <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#e0e0e0" style="border-collapse:collapse;font-family: 'Open Sans', sans-serif;">
            <tr>
                <td>
                    <tr>
                        <td height="30"></td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" bgcolor="#fff" style="max-width: 600px;">                             
                                <tr>
                                    <td width="20"></td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                                            <tr><td height="40"></td></tr>
                                            <tr>
                                                <td align="center">
                                                    <p style="font-size: 60px; font-weight: 300; color: #008ecc;line-height: normal; text-transform: uppercase;margin: 0;">Welcome</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20"></td>
                                </tr>
                                <tr>
                                    <td width="20"></td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                                            <tr><td height="10"></td></tr>
                                            <tr>
                                                <td align="center">
                                                    <p style="font-size: 24px; color: #191919; font-weight: 300;margin: 0;">Admin has invited you to join Dronacharya academy client to get started sign in with your email address and password.</p>
                                                </td>
                                            </tr>
                                            <tr><td height="50"></td></tr>
                                        </table>
                                    </td>
                                    <td width="20"></td>
                                </tr>
                                <tr>
                                    <td width="20"></td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
                                            <tr><td height="10"></td></tr>
                                            <tr>
                                                <td align="center">
                                                    <a href="<?php echo url('/').'/'; ?>" style="font-size: 24px; color: #191919; font-weight: 300;margin: 0;">Click here</a>
                                                </td>
                                            </tr>
                                            <tr><td height="50"></td></tr>
                                        </table>
                                    </td>
                                    <td width="20"></td>
                                </tr>
                                <tr>
                                    <td width="20"></td>
                                    <td  bgcolor="#e5007e" valign="middle">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="margin: auto;">
                                            <tr>
                                                <td colspan="3" height="30"></td>
                                            </tr>
                                            <tr>
                                                <td width="10"></td>
                                                <td valign="middle" style="text-align: center; font-size: 24px;font-weight: 300; color: #ffffff;"> Your Temporary Password</td>
                                                <td width="10"></td>
                                            </tr>
                                            <tr>
                                                <td width="10"></td>
                                                <td valign="middle" style="text-align: center; font-size: 40px;  color: #ffffff;"> {{$client['password']}}</td>
                                                <td width="10"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="30"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" height="20"></td>
                                </tr>
                                <tr>
                                    <td width="20"></td>
                                    <td  bgcolor="#000000" valign="middle">
                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="margin: auto;">
                                            <tr><td colspan="3" height="30"></td></tr>
                                            <tr>
                                                <td width="10"></td>
                                                <td align="center">
                                                    <img src="{{ asset(IMAGES_FOLDER_URL.'images/logo.svg') }}" style="max-width: 100%;" alt="logo" />
                                                    
                                                </td>
                                                <td width="10"></td>
                                            </tr>
                                            <tr><td colspan="3" height="30"></td></tr>
                                        </table>
                                    </td>
                                    <td width="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" height="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="30"></td>
                    </tr>
                </td>
            </tr>
        </table>
    </body>
</html>