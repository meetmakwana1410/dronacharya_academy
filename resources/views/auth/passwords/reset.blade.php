@extends('layouts.auth_layout')
@section('title') 
{{ __('Reset Password') }}
@endsection
@section('content')
<div class="login-container">
    <div class="t-cell">
        <div class="login-card">
            <img src="{{asset(IMAGES_FOLDER_URL.'logo.svg')}}" alt="Logo" class="logo img-fluid" />
            <form action="{{route('reset.password.post')}}" id="loginForm" method="post" data-parsley-validate="" novalidate>
                @csrf
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <input type="hidden" name="token" value="{{ $token }}">

                 <div class="form-group">
                    <i class="login-icon icon-user"></i>
                    <input type="email" class="form-control form-control-danger notallowspace" placeholder="{{ __('E-mail address') }}"" name="email" id="email" required="" data-parsley-required-message="Please enter email id.">
                    @if ($errors->has('email'))
                            <div class="parsley-errors-list">{{ $errors->first('email') }}</div>
                    @endif
                </div>

                <div class="form-group">
                    <i class="login-icon icon-password"></i>
                    <input type="password" class="form-control form-control-danger notallowspace" placeholder="Password" name="password" id="password" required="" data-parsley-minlength="8" data-parsley-maxlength="30" data-parsley-pattern="(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" data-parsley-pattern-message="Password should have minimum 8 characters which contain 1 upper case, 1 lower case & 1 number." data-parsley-required-message="Please enter password.">
                </div>

                <div class="form-group">
                    <i class="login-icon icon-password"></i>
                    <input type="password" class="form-control form-control-danger notallowspace" placeholder="Confirm password" name="password_confirmation"  required="" data-parsley-equalto="#password" data-parsley-required-message="Please enter confirm password." data-parsley-equalto-message="Password and confirm password must be same.">
                </div>

                <button class="btn btn-login btn-theme with-arrow">{{ __('Reset password') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page_script')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('#loginForm').parsley({
                excluded: ' input[type=reset], input[type=hidden], :disabled'
            });

            // HAS UPPERCASE
            window.Parsley.addValidator('uppercase', {
              requirementType: 'number',
              validateString: function(value, requirement) {
                var uppercases = value.match(/[A-Z]/g) || [];
                return uppercases.length >= requirement;
              },
              messages: {
                en: 'Your password must contain at least (%s) uppercase letter.'
              }
            });

            // HAS LOWERCASE
            window.Parsley.addValidator('lowercase', {
              requirementType: 'number',
              validateString: function(value, requirement) {
                var lowecases = value.match(/[a-z]/g) || [];
                return lowecases.length >= requirement;
              },
              messages: {
                en: 'Your password must contain at least (%s) lowercase letter.'
              }
            });

            // HAS NUMBER
            window.Parsley.addValidator('number', {
              requirementType: 'number',
              validateString: function(value, requirement) {
                var numbers = value.match(/[0-9]/g) || [];
                return numbers.length >= requirement;
              },
              messages: {
                en: 'Your password must contain at least (%s) number.'
              }
            });

            // HAS SPECIAL CHAR
            window.Parsley.addValidator('special', {
              requirementType: 'number',
              validateString: function(value, requirement) {
                var specials = value.match(/[^a-zA-Z0-9]/g) || [];
                return specials.length >= requirement;
              },
              messages: {
                en: 'Your password must contain at least (%s) special characters.'
              }
            });
        });

        $("#loginForm").submit(function( event ) {
            if(!$('#loginForm').parsley().validate()){
                return false;
                event.preventDefault();
            }
        });
    </script>
@endsection