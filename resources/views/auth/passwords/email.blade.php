@extends('layouts.auth_layout')
@section('title') 
{{ __('Forgot password') }}
@endsection
@section('content')
<div class="login-container">
    <div class="t-cell">
        <div class="login-card">
            <img src="{{asset(IMAGES_FOLDER_URL.'logo.svg')}}" alt="Logo" class="logo img-fluid" />
            <div>
                @include('admin.common.flash_message')
            </div>
            <form action="{{route('send-reset-link')}}" id="loginForm" method="post" data-parsley-validate="" novalidate>
                @csrf
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ session('status') }}
                    </div>
                    @endif
                     <div class="form-group">

                    <i class="login-icon icon-user"></i>
                    <input type="email" class="form-control" placeholder="{{ __('E-mail address') }}" name="email" required="" value="{{ old('email') }}" data-parsley-required-message="Please enter email id.">
                    <!-- @if (session('error'))
                        <div class="parsley-errors-list">{{session('error')}}</div>
                    @endif -->
                </div>

                <button class="btn btn-login btn-theme with-arrow">Send</button>

                <div class="form-group">
                    <a href="{{route('login')}}" class="forgot-link">Back to login</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection