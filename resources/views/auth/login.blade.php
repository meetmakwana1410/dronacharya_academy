@extends('layouts.auth_layout')
@section('title') 
{{ __('Login') }}
@endsection
@section('content')
<div class="login-container">
    <div class="t-cell">
        <div class="login-card">
            <img style="width: 30%" src="{{asset(IMAGES_FOLDER_URL.'logo.svg')}}" alt="Logo" class="logo img-fluid" />
            <div>
                <h3>Dronacharya Academy</h3>
            <br>
            </div>
            <div>
                @include('admin.common.flash_message')
            </div>
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" id="loginForm" data-parsley-validate="" novalidate>
                @csrf
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <i class="login-icon icon-user"></i>
                    <input type="email" class="form-control" placeholder="{{ __('E-mail address') }}" name="email" required="" data-parsley-maxlength="255" value="{{ old('email') }}" data-parsley-required-message="Please enter email id.">
                </div>

                <div class="form-group">
                    <i class="login-icon icon-password"></i>
                    <input type="password" class="form-control notallowspace" placeholder="{{ __('Password') }}" name="password" required="" data-parsley-minlength="6" data-parsley-maxlength="30" value="{{ old('password') }}" data-parsley-required-message="Please enter password." data-parsley-minlength-message="Password contains at least 6 characters.">
                    @if ($errors->has('email'))
                        <div class="parsley-errors-list">{{ $errors->first('email') }}</div>
                    @endif
                    @if ($errors->has('password'))
                        <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                    @endif
                </div>

                <button class="btn btn-login btn-theme with-arrow">{{ __('Login') }}</button>

                <a href="{{ route('password.request') }}" class="forgot-link">{{ __('Forgot your password?') }}</a>
            </form>
        </div>
    </div>
</div>
@endsection