@extends('layouts.app')
@section('title') 
@if (isset($students) && !empty($students))
    @lang('common.label.edit_student')
@else
    @lang('common.label.add_student')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<link href="{{asset(CSS_FOLDER_URL.'bootstrap-3.min.css')}}" rel="stylesheet" />
<link href="{{asset(CSS_FOLDER_URL.'bootstrap-datetimepicker.css')}}" rel="stylesheet" />

<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($students) && !empty($students))
                @lang('common.label.edit_student')
            @else
                @lang('common.label.add_student')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateStudents','autocomplete'=>'off','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($students) && !empty($students))?URL::signedRoute('students.edit',[$students->id]): route('students.add')]) !!}  
        @if (isset($students) && !empty($students))
            {{FORM::hidden('id',$students->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <h4>{{__('common.label.identify_details')}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.roll_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('roll_number', (isset($students) && !empty($students))?$students->roll_number:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('roll_number'))
                        <div class="parsley-errors-list">{{ $errors->first('roll_number') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                @if(!empty($students->profile_image) && public_path('uploads') . "/students/".$students->profile_image)
                    <img src="{{asset('/uploads/students/'.$students->profile_image)}}" class="default_logo"  style="width :20%;height: 70%;">
                @else
                    <img src="{{asset('img/noimages.png')}}" class="default_logo"  style="width :20%">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <h4>{{__('common.label.student_details')}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.student_name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('student_name', (isset($students) && !empty($students))?$students->student_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('student_name'))
                        <div class="parsley-errors-list">{{ $errors->first('student_name') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.date_of_birth'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('student_date_of_birth', (isset($students) && !empty($students))?date('d-m-Y',strtotime($students->student_date_of_birth)):'',['class' => 'form-control custom-input-style2 parsley-validated','id'=>'student_date_of_birth','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('student_date_of_birth'))
                        <div class="parsley-errors-list">{{ $errors->first('student_date_of_birth') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.gender'),['class' => 'custom-label asterisk'])}}
                    <div class="radio-answer">
                        <input type="radio" id="gender_male" class="ans" name="gender" value="1"required data-parsley-required-message="Please select at least one gender." checked="checked">
                        <label for="gender_male">
                            <span class="opt-character">Male</span>
                        </label>
                       
                        <input type="radio" id="gender_female" class="ans" name="gender" value="2" required data-parsley-required-message="Please select at least one gender.">
                        <label for="gender_female">
                            <span class="opt-character">Female</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.studnet_mobile_number'),['class' => 'custom-label'])}}
                    {{Form::text('student_mobile_number', (isset($students) && !empty($students))?$students->student_mobile_number:'',['class' => 'form-control custom-input-style2','minlength'=>'10','maxlength'=>'10','onkeypress'=>'return isNumberKey(event);','autocomplete'=>'false'])}}
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.email'),['class' => 'custom-label'])}}
                    {{Form::email('student_email', (isset($students) && !empty($students))?$students->student_email:'',['class' => 'form-control custom-input-style2 parsley-validated','autocomplete'=>'false'])}}
                    @if ($errors->has('student_email'))
                        <div class="parsley-errors-list">{{ $errors->first('student_email') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <h4>{{__('common.label.parents_details')}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('father_name', (isset($students) && !empty($students))?$students->father_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('father_name'))
                        <div class="parsley-errors-list">{{ $errors->first('father_name') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.mobile_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('father_mobile_number', (isset($students) && !empty($students))?$students->father_mobile_number:'',['class' => 'form-control custom-input-style2 parsley-validated','onkeypress'=>'return isNumberKey(event);','required'=>'','data-parsley-required-message'=>__('common.message.field_required'),'minlength'=>'10','maxlength'=>'10'])}}
                    @if ($errors->has('father_mobile_number'))
                        <div class="parsley-errors-list">{{ $errors->first('father_mobile_number') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.mother_mobile_number'),['class' => 'custom-label'])}}
                    {{Form::text('mother_mobile_number', (isset($students) && !empty($students))?$students->mother_mobile_number:'',['class' => 'form-control custom-input-style2 parsley-validated','minlength'=>'10','maxlength'=>'10','onkeypress'=>'return isNumberKey(event);'])}}
                    @if ($errors->has('mother_mobile_number'))
                        <div class="parsley-errors-list">{{ $errors->first('mother_mobile_number') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.email'),['class' => 'custom-label'])}}
                    {{Form::email('father_email', (isset($students) && !empty($students))?$students->father_email:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                    @if ($errors->has('father_email'))
                        <div class="parsley-errors-list">{{ $errors->first('father_email') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.occupation'),['class' => 'custom-label'])}}
                    {{Form::text('father_occupation', (isset($students) && !empty($students))?$students->father_occupation:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                    @if ($errors->has('father_occupation'))
                        <div class="parsley-errors-list">{{ $errors->first('father_occupation') }}</div>
                    @endif
                </div>
            </div>
        </div>

        @if(!$students)
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($students) && !empty($students))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($students) && !empty($students))?false:true,'data-type'=>'passwordcheck','data-parsley-required-message'=>__('common.message.password_required_messages')])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('confirm_password', __('common.label.confirm_password'),['class' =>(isset($client) && !empty($client))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password_confirmation',  ['id'=>'password_confirmation','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($client) && !empty($client))?false:true,'data-parsley-equalto'=>'#password','data-parsley-required-message'=>__('common.message.confirm_password_required_messages')])}}
                        @if ($errors->has('password_confirmation'))
                            <div class="parsley-errors-list">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                </div>  
            </div>
        @else
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($students) && !empty($students))?'custom-label': 'custom-label'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','data-type'=>'passwordcheck'])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>                
            </div>
        @endif

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.address'),['class' => 'custom-label asterisk'])}}
                    {{Form::textarea('address', (isset($students) && !empty($students))?$students->address:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                    @if ($errors->has('address'))
                        <div class="parsley-errors-list">{{ $errors->first('address') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.landline_number'),['class' => 'custom-label'])}}
                    {{Form::text('landline_number', (isset($students) && !empty($students))?$students->landline_number:'',['class' => 'form-control custom-input-style2 parsley-validated','onkeypress'=>'return isNumberKey(event);'])}}
                    @if ($errors->has('landline_number'))
                        <div class="parsley-errors-list">{{ $errors->first('landline_number') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <h4>{{__('common.label.batch_details')}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label asterisk'])}}
                    <select name="batch_id" id="batch_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required.">
                    <option value="">Select Batch Code</option>
                        @if(isset($batch))
                            @foreach($batch as $btc)
                                <option value="{{$btc->id}}" {{(isset($students->batch_id)) && $students->batch_id == $btc->id ? 'selected':'' }}>{{$btc->batch_name}} ({{$btc->branch_name}})</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.admission_date'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('admission_date', (isset($students) && !empty($students))?date('d-m-Y',strtotime($students->admission_date)):'',['class' => 'form-control custom-input-style2 parsley-validated','id'=>'admission_date','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('admission_date'))
                        <div class="parsley-errors-list">{{ $errors->first('admission_date') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.subject'),['class' => 'custom-label asterisk'])}}
                    <select name="subjects[]" id="subjects" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required." multiple="">
                    <option value="">Select Subject</option>
                        @if(isset($batchSubject))
                            @foreach($batchSubject as $btcs)
                                <option value="{{$btcs->subject_id}}"  @if(!empty($studentsSubjects) && in_array($btcs->subject_id,$studentsSubjects))  selected  @endif  >{{$btcs->subject_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('school', __('common.label.school'),['class' => 'custom-label'])}}
                    <select name="school_id" id="school_id" class="form-control select2"  data-parsley-trigger="change focusout">
                    <option value="">Select School</option>
                        @if(isset($school))
                            @foreach($school as $scl)
                                <option value="{{$scl->id}}" {{(isset($students->school_id)) && $students->school_id == $scl->id ? 'selected':'' }}>{{$scl->school_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <input type="hidden" id="url" name="url" value="{{asset('/')}}">
        </div>
        
        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                <button id="btnUpdateButton" type="submit" class="btn btn-custom font-size-sm btn-secondary-1">@lang('common.label.save')</button>
                {{link_to_route('students.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>


        
        {!! Form::close() !!}
    </div>
@endsection


@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'moment-with-locales.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'students/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection