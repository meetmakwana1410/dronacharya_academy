@extends('layouts.app')
@section('title') 
@if (isset($subjects) && !empty($subjects))
    @lang('common.label.edit_subjects')
@else
    @lang('common.label.add_subjects')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
<style>

    /* Limit image width to avoid overflow the container */
    img {
        max-width: 100%; /* This rule is very important, please do not ignore this! */
    }

    #canvas {
        height: 600px;
        width: 600px;
        background-color: #ffffff;
        cursor: default;
        border: 1px solid black;
    }
    #image-crop-canvas{
        width: 100%;
    }
    #toggle-aspect-ratio .active{border:1px solid red;}

</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($subjects) && !empty($subjects))
                @lang('common.label.edit_subjects')
            @else
                @lang('common.label.add_subjects')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateSubjects','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($subjects) && !empty($subjects))?URL::signedRoute('subject.edit',[$subjects->id]): route('subject.add')]) !!}  
        @if (isset($subjects) && !empty($subjects))
            {{FORM::hidden('id',$subjects->id)}}
        @endif 
        {{csrf_field()}}
       
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.subjects'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('subject_name', (isset($subjects) && !empty($subjects))?$subjects->subject_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('subject_name'))
                        <div class="parsley-errors-list">{{ $errors->first('subject_name') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-xl-3 col-lg-4 col-sm-4">
                <div class="form-group text-center">
                    <div class="image-container add_logo">

                        @if(!empty($subjects->subject_image) && file_exists(public_path('uploads/subjects') . "/".$subjects->subject_image))
                            <img src="{{asset('/uploads/subjects/'.$subjects->subject_image)}}" id="edit_image">
                            <span id="delete_btn" class="close-btn remove_client_logo">×</span>
                        @else
                            <img src="{{asset('img/client-logo.jpg')}}" style="width: 50%" id="default_logo">
                        @endif
                    </div>
                    <div class="image-grid">
                        <div class="image-container"></div>
                    </div>
                    <input id="logo_remove_flag" type="hidden" name="logo_remove_flag" />
                    <div class="custom-file">
                        <input class="image" id="image" type="file" name="image" placeholder="image" accept="image/*"  />
                        <label for="image" class="action-link">@lang('common.label.subject_image')</label>
                    </div>
                    <input id="image_name" type="hidden" name="image_name" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.select_standard'),['class' => 'custom-label asterisk'])}}
                    <select name="standard[]" id="standard" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Standard is required." multiple="">
                    <option value="">Select Standard</option>
                        @if(isset($standards))
                            @foreach($standards as $standard)
                                    <option value="{{$standard->id}}"  @if(!empty($standardSubjects) && in_array($standard->id,$standardSubjects))  selected  @endif  >{{$standard->standard_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateSubjects','title'=> __('common.label.save')])}}
                {{link_to_route('subject.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>

    <div class="modal fade in" id="image-crop-modal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Crop Image</strong></h4>
                </div>
                <div class="modal-body">
                    <canvas id="image-crop-canvas" width="100%"></canvas>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom btn-theme font-size-sm crop-image">Crop</button>
                    <button type="button" class="btn btn-custom btn-secondary-2 font-size-sm close-popup" >Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_css')
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/main.css')}}" rel="stylesheet">
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/cropper.css')}}" rel="stylesheet">
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/js/cropper.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'subjects/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $("select").select2();
</script>
@endsection