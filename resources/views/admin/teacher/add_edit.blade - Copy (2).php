@extends('layouts.app')
@section('title') 
@if (isset($teacher) && !empty($teacher))
    @lang('common.label.edit_teacher')
@else
    @lang('common.label.add_teacher')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />

<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($teacher) && !empty($teacher))
                @lang('common.label.edit_teacher')
            @else
                @lang('common.label.add_teacher')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateTeacher','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($teacher) && !empty($teacher))?URL::signedRoute('teacher.edit',[$teacher->id]): route('teacher.add')]) !!}  
        @if (isset($teacher) && !empty($teacher))
            {{FORM::hidden('id',$teacher->id)}}
        @endif 
        {{csrf_field()}}
        
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('name', (isset($teacher) && !empty($teacher))?$teacher->name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('name'))
                        <div class="parsley-errors-list">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.email'),['class' => 'custom-label asterisk'])}}
                    {{Form::email('email', (isset($teacher) && !empty($teacher))?$teacher->email:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('email'))
                        <div class="parsley-errors-list">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.mobile_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('contact_no', (isset($teacher) && !empty($teacher))?$teacher->contact_no:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required'),'onkeypress'=>'return isNumberKey(event);'])}}
                    @if ($errors->has('contact_no'))
                        <div class="parsley-errors-list">{{ $errors->first('contact_no') }}</div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label asterisk'])}}
                    <select name="batch_id[]" id="batch_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required." multiple>
                    <option value="">Select Batch Code</option>
                        @if(isset($batch))
                            @foreach($batch as $btcs)
                                <option value="{{$btcs->id}}"  @if(!empty($teacherBatch) && in_array($btcs->id,$teacherBatch))  selected  @endif  >{{$btcs->batch_code}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.subjects'),['class' => 'custom-label asterisk'])}}
                    <select name="subject_id[]" id="subject_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required." multiple>
                    <option value="">Select Subject</option>
                        @if(isset($subjects))
                            @foreach($subjects as $subj)
                                <option value="{{$subj->id}}"  @if(!empty($teacherSubject) && in_array($subj->id,$teacherSubject))  selected  @endif  >{{$subj->subject_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            
        </div>

        @if(!$teacher)
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($teacher) && !empty($teacher))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($teacher) && !empty($teacher))?false:true,'data-type'=>'passwordcheck','data-parsley-required-message'=>__('common.message.password_required_messages')])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('confirm_password', __('common.label.confirm_password'),['class' =>(isset($teacher) && !empty($teacher))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password_confirmation',  ['id'=>'password_confirmation','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($teacher) && !empty($teacher))?false:true,'data-parsley-equalto'=>'#password','data-parsley-required-message'=>__('common.message.confirm_password_required_messages')])}}
                        @if ($errors->has('password_confirmation'))
                            <div class="parsley-errors-list">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                </div>  
            </div>
        @endif

        
        

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateTeacher','title'=> __('common.label.save')])}}
                {{link_to_route('teacher.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'teacher/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection