@extends('layouts.app')
@section('title') 
@if (isset($teacher) && !empty($teacher))
    @lang('common.label.edit_teacher')
@else
    @lang('common.label.add_teacher')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />

<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
    button.btn.btn-danger {
    top: 50px;
    margin: 25px auto;
}
</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($teacher) && !empty($teacher))
                @lang('common.label.edit_teacher')
            @else
                @lang('common.label.add_teacher')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateTeacher','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($teacher) && !empty($teacher))?URL::signedRoute('teacher.edit',[$teacher->id]): route('teacher.add')]) !!}  
        @if (isset($teacher) && !empty($teacher))
            {{FORM::hidden('id',$teacher->id)}}
        @endif 
        {{csrf_field()}}
        
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('name', (isset($teacher) && !empty($teacher))?$teacher->name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('name'))
                        <div class="parsley-errors-list">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.email'),['class' => 'custom-label asterisk'])}}
                    {{Form::email('email', (isset($teacher) && !empty($teacher))?$teacher->email:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('email'))
                        <div class="parsley-errors-list">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.mobile_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('contact_no', (isset($teacher) && !empty($teacher))?$teacher->contact_no:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','minlength'=>'10','maxlength'=>'10','data-parsley-required-message'=>__('common.message.field_required'),'onkeypress'=>'return isNumberKey(event);'])}}
                    @if ($errors->has('contact_no'))
                        <div class="parsley-errors-list">{{ $errors->first('contact_no') }}</div>
                    @endif
                </div>
            </div>
        </div>

        @if(!$teacher)
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($teacher) && !empty($teacher))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($teacher) && !empty($teacher))?false:true,'data-type'=>'passwordcheck','data-parsley-required-message'=>__('common.message.password_required_messages')])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('confirm_password', __('common.label.confirm_password'),['class' =>(isset($teacher) && !empty($teacher))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password_confirmation',  ['id'=>'password_confirmation','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($teacher) && !empty($teacher))?false:true,'data-parsley-equalto'=>'#password','data-parsley-required-message'=>__('common.message.confirm_password_required_messages')])}}
                        @if ($errors->has('password_confirmation'))
                            <div class="parsley-errors-list">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                </div>  
            </div>
        @else
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($teacher) && !empty($teacher))?'custom-label': 'custom-label'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','data-type'=>'passwordcheck'])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>                
            </div>
        @endif
        
        
        <?php 
            if(!empty($teacherBBS) && isset($teacherBBS)){
                foreach ($teacherBBS as $key => $value) {
                ?>
                <div class="row" id="editTeacherBatch_<?php echo $value->id; ?>">
                    <div class="col-lg-4 col-sm-6">
                        <div class="form-group">
                            {{ Form::label('name', __('common.label.branch'),['class' => 'custom-label asterisk'])}}
                            <select style="height: 32px;" name="branch[]" id="branch_<?php echo $key; ?>" class="form-control" >
                                @if(isset($branch))
                                    @foreach($branch as $bran)
                                        <option <?php if(!empty($value->branch_id) && ($value->branch_id == $bran->id)) { echo 'selected'; } ?> value="{{$bran->id}}">{{$bran->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="form-group">
                            {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label asterisk'])}}
                            <select style="height: 32px;" name="batch_id[]" id="batch_id_<?php echo $key; ?>" class="form-control select2">
                                @if(isset($batch))
                                    @foreach($batch as $btcs)
                                        <option <?php if(!empty($value->batch_id) && ($value->batch_id == $btcs->id)) { echo 'selected'; } ?> value="{{$btcs->id}}">{{$btcs->batch_name}} ({{$btcs->branch_name}})</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group">
                            {{ Form::label('name', __('common.label.subjects'),['class' => 'custom-label asterisk'])}}
                            <select style="height: 32px;" name="subject_id[]" id="subject_id_<?php echo $key; ?>" class="form-control select2">
                            <option value="">Select Subject</option>
                                @if(isset($subjects))
                                    @foreach($subjects as $subj)
                                        <option <?php if(!empty($value->subject_id) && ($value->subject_id == $subj->id)) { echo 'selected'; } ?> value="{{$subj->id}}">{{$subj->subject_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6">
                        <button type="button" class="btn btn-danger" onclick="removeRow(<?php echo $key; ?>,<?php echo $value->id; ?>);" ><i class="fa fa-trash"></i></button>
                    </div>
                </div>
                <?php
                }
            }
        ?>
            
        <div class="addbranchsubjectbatch"></div>
        <div class="clearfix">
            <div class="pull-left">
                <a href="javascript:void(0);" class="btn btn-primary addBranchSubjectBatch">Add more</a>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateTeacher','title'=> __('common.label.save')])}}
                {{link_to_route('teacher.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'teacher/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();

    //Add more Mobile screen.
        var counterMobile = 1;
        $(document).on('click','.addBranchSubjectBatch',function(e){
           

        var append = '<div class="row" id=existingTeacherBatch_'+counterMobile+'><div class="col-lg-4 col-sm-6"><div class="form-group"><select style="height: 32px;" name="branch[]" id="branch" class="form-control select2" required data-parsley-required-message="Branch is required."><?php if(isset($branch)){ foreach($branch as $bran) { ?> <option value="<?php echo $bran->id ?>"><?php echo $bran->name; ?></option><?php } } ?></select></div></div><div class="col-lg-4 col-sm-6"><div class="form-group"><select style="height: 32px;" name="batch_id[]" id="batch_id" class="form-control select2" required data-parsley-required-message="Branch is required."><?php if(isset($batch)){ foreach($batch as $btcs) { ?> <option value="<?php echo $btcs->id ?>"><?php echo $btcs->batch_name.' ('.$btcs->branch_name.')'; ?></option><?php } } ?></select></div></div><div class="col-lg-3 col-sm-6"><div class="form-group"><select style="height: 32px;"  name="subject_id[]" id="subject_id" class="form-control select2" required data-parsley-required-message="subjects is required."><?php if(isset($subjects)){ foreach($subjects as $subj) { ?> <option value="<?php echo $subj->id ?>"><?php echo $subj->subject_name; ?></option><?php } } ?></select></div></div><div class="col-lg-1 col-sm-6"><button type="button" class="btn btn-danger" onclick="removeRow('+counterMobile+',null);" ><i class="fa fa-trash"></i></button></div></div>';

            $(".addbranchsubjectbatch").append(append);
            counterMobile++;
        });

        //Remove row.
        function removeRow(counterId,recordsId) {
            //alert("counterId : "+counterId + ' | '+ "recordsId : "+recordsId)
            if(counterId != '' && recordsId != null){
               
                swal({
                    title: 'Are you sure?',
                    text: "Are you sure you want to delete this record? If yes it will be removed permanently",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success margin-5',
                    cancelButtonClass: 'btn btn-danger margin-5',
                    buttonsStyling: false,
                    allowOutsideClick: false
                }).then(function (data) {
                    if (data.dismiss === 'cancel') {

                    }else{
                       
                        jQuery.ajax({
                            url: '{{ route("teacher.deleteRecord") }}',
                            type:"POST",
                            data: {
                                '_token': '{{ csrf_token() }}',
                                'recordsId': recordsId,
                            },
                            success: function(response) {
                                if(response.status){
                                    swal({
                                        title:'Deleted!',
                                        text: 'Your record has been deleted.',
                                        type:'success',
                                        allowOutsideClick: false,
                                    });
                                    $('#editTeacherBatch_'+recordsId).remove();
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                alert(textStatus, errorThrown);
                            }
                        });
                    }
                });
            }else{
                $('#existingTeacherBatch_'+counterId).remove();
            }
        }
</script>
@endsection