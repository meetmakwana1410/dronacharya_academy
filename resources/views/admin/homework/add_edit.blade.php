@extends('layouts.app')
@section('title') 
@if (isset($documents) && !empty($documents))
    @lang('common.label.edit_documents')
@else
    @lang('common.label.add_documents')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($documents) && !empty($documents))
                @lang('common.label.edit_documents')
            @else
                @lang('common.label.add_documents')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($documents) && !empty($documents))?URL::signedRoute('documents.edit',[$documents->id]): route('documents.add')]) !!}  
        @if (isset($documents) && !empty($documents))
            {{FORM::hidden('id',$documents->id)}}
        @endif 
        {{csrf_field()}}
       
        <div class="row">
            <div class="col-lg-8 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('name', (isset($documents) && !empty($documents))?$documents->name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('name'))
                        <div class="parsley-errors-list">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>

            
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label asterisk'])}}
                    <select name="batch_id" id="batch_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required.">
                    <option value="">Select Batch Code</option>
                        @if(isset($batch))
                            @foreach($batch as $btc)
                                <option value="{{$btc->id}}" {{(isset($documents->batch_id)) && $documents->batch_id == $btc->id ? 'selected':'' }}>{{$btc->batch_code}} ({{$btc->branch_name}})</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.subjects'),['class' => 'custom-label asterisk'])}}
                    <select name="subject_id" id="subject_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required.">
                    <option value="">Select Subject</option>
                        @if(isset($subjects))
                            @foreach($subjects as $subj)
                                <option value="{{$subj->id}}"  @if(!empty($documents->subject_id) && in_array($subj->id,[$documents->subject_id]))  selected  @endif  >{{$subj->subject_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('note', __('common.label.note'),['class' => 'custom-label'])}}
                    {{Form::textarea('note', (isset($documents) && !empty($documents))?$documents->note:'',['class' => 'form-control','rows'=>4])}}
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group text-center">
                    <input id="document" type="file" name="image" placeholder="document"/>
                    @if ($errors->has('image'))
                        <div class="parsley-errors-list">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                {{link_to_route('subject.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    
@endsection

@section('page_css')
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/main.css')}}" rel="stylesheet">
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/cropper.css')}}" rel="stylesheet">
@endsection

@section('page_script')

<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'subjects/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $("select").select2();
</script>
@endsection