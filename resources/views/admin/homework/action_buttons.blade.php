<div class="action-column">
    <a href="{{URL::route('homework.gethomework',[$object->id])}}" class="view-action" title="@lang('common.label.show')">
        <span class="view-icon"></span>
    </a>
</div>