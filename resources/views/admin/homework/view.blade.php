@extends('layouts.app')
@section('title') 
    @lang('common.label.homework_details')
@endsection

@section('content')
<!-- Client Info -->
<div class="custom-card">
    <div class="row">        
        <div class="col-lg-4 col-sm-6 order-lg-8 order-sm-6 text-right">
           
            <a href="{{URL::route('homework.list')}}" class="btn btn-custom font-size-sm btn-secondary-2" id="reset_btn"title="{{ __('common.label.back_btn') }}">{{ __('common.label.back_btn') }}</a>

        </div>
        <div class="col-lg-8 col-sm-6">
            <h3 class="title">@lang('common.label.homework_details')</h3>
        </div>
    </div>
    <div class="logo-detail">
        
        <div class="detail view-detail">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group view-detail">
                        <label>@lang('common.label.batch')</label>
                        <p>{{  !empty($homework)?$homework[0]->batch_code:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group view-detail">
                        <label>@lang('common.label.subject')</label>
                        <p>{{  !empty($homework)?$homework[0]->subject_name:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group view-detail">
                        <label>@lang('common.label.submission_date')</label>
                        <p>{{  !empty($homework)?$homework[0]->submission_date:'-' }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if(!empty($homework[0]->document) && file_exists(public_path().'/uploads/homework/'.$homework[0]->document))
                    {
                    ?>
                        <div class="col-lg-6 col-sm-6">
                            <div class="form-group view-detail">
                                <label>@lang('common.label.documents')</label>
                                <p>
                                    <a target="_blank" href="{{asset('/uploads/homework/'.$homework[0]->document)}}">
                                        <i class="fa fa-file-text" aria-hidden="true"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                <?php } ?>
                        
                <div class="col-lg-6 col-sm-6">

                    <?php 
                        if(!empty($homework) && $homework[0]->images){
                            $exp = explode(',', $homework[0]->images);

                            foreach ($exp as $key => $value) {
                                if(!empty($value) && file_exists(public_path().'/uploads/homework/'.$value))
                                {   
                            ?>
                                <img src="{{asset('/uploads/homework/'.$value)}}" style="width: 20%">
                            <?php
                                }
                            }
                        }
                    ?>
                    
                </div>
            </div>
        </div> 
    </div>
</div>

@endsection
