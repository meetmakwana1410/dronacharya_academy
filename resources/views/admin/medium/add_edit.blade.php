@extends('layouts.app')
@section('title') 
@if (isset($medium) && !empty($medium))
    @lang('common.label.edit_medium')
@else
    @lang('common.label.add_medium')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($medium) && !empty($medium))
                @lang('common.label.edit_medium')
            @else
                @lang('common.label.add_medium')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateMedium','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($medium) && !empty($medium))?URL::signedRoute('medium.edit',[$medium->id]): route('medium.add')]) !!}  
        @if (isset($medium) && !empty($medium))
            {{FORM::hidden('id',$medium->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.medium'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('medium_name', (isset($medium) && !empty($medium))?$medium->medium_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('medium_name'))
                        <div class="parsley-errors-list">{{ $errors->first('medium_name') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateMedium','title'=> __('common.label.save')])}}
                {{link_to_route('medium.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'medium/add_edit.js')}}"></script>
@endsection