@extends('layouts.app')
@section('title') 
@if (isset($examtype) && !empty($examtype))
    @lang('common.label.edit_exam_type')
@else
    @lang('common.label.add_exam_type')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($examtype) && !empty($examtype))
                @lang('common.label.edit_exam_type')
            @else
                @lang('common.label.add_exam_type')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($examtype) && !empty($examtype))?URL::signedRoute('exam_type.edit',[$examtype->id]): route('exam_type.add')]) !!}  
        @if (isset($examtype) && !empty($examtype))
            {{FORM::hidden('id',$examtype->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.exam_type'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('exam_type_name', (isset($examtype) && !empty($examtype))?$examtype->exam_type_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('exam_type_name'))
                        <div class="parsley-errors-list">{{ $errors->first('exam_type_name') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                {{link_to_route('exam_type.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'exam_type/add_edit.js')}}"></script>
@endsection