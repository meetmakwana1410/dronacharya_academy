<div class="action-column">
    
    <a href="{{URL::signedRoute('exam_type.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
        <span class="edit-icon"></span>
    </a>
    <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('exam_type.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
        <span class="delete-icon"></span>
    </a>
</div>