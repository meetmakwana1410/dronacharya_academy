@extends('layouts.app')
@section('title') 
@if (isset($batch) && !empty($batch))
    @lang('common.label.edit_batch')
@else
    @lang('common.label.add_batch')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
<style>

    /* Limit image width to avoid overflow the container */
    img {
        max-width: 100%; /* This rule is very important, please do not ignore this! */
    }

    #canvas {
        height: 600px;
        width: 600px;
        background-color: #ffffff;
        cursor: default;
        border: 1px solid black;
    }
    #image-crop-canvas{
        width: 100%;
    }
    #toggle-aspect-ratio .active{border:1px solid red;}

</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($batch) && !empty($batch))
                @lang('common.label.edit_batch')
            @else
                @lang('common.label.add_batch')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateBatch','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($batch) && !empty($batch))?URL::signedRoute('batch.edit',[$batch->id]): route('batch.add')]) !!}  
        @if (isset($batch) && !empty($batch))
            {{FORM::hidden('id',$batch->id)}}
        @endif 
        {{csrf_field()}}
       
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('batch_name', (isset($batch) && !empty($batch))?$batch->batch_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('batch_name'))
                        <div class="parsley-errors-list">{{ $errors->first('batch_name') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.select_baord'),['class' => 'custom-label asterisk'])}}
                    <select name="board" id="board" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Standard is required.">
                    <option value="">Select Baord</option>
                        @if(isset($board))
                            @foreach($board as $brd)
                                <option value="{{$brd->id}}" {{(isset($batch->board_id)) && $batch->board_id == $brd->id ? 'selected':'' }}>{{$brd->board_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_select_standard'),['class' => 'custom-label asterisk'])}}
                    <select name="standard" id="standard" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Standard is required.">
                    <option value="">Select Standard</option>
                        @if(isset($standards))
                            @foreach($standards as $standard)
                                    <option value="{{$standard->id}}" {{(isset($batch->standard_id)) && $batch->standard_id == $standard->id ? 'selected':'' }}>{{$standard->standard_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.academic_year'),['class' => 'custom-label asterisk'])}}
                    <select name="academic_year" id="academic_year" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Academic Year is required.">
                    <option value="">Select Academic Year</option>
                        @if(isset($academic_year))
                            @foreach($academic_year as $years)
                                <option value="{{$years->id}}" {{(isset($batch->academic_year_id)) && $batch->academic_year_id == $years->id ? 'selected':'' }}>{{$years->year}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.medium'),['class' => 'custom-label asterisk'])}}
                    <select name="medium_id" id="medium_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Medium is required.">
                    <option value="">Select Medium</option>
                        @if(isset($medium))
                            @foreach($medium as $med)
                                <option value="{{$med->id}}" {{(isset($batch->medium_id)) && $batch->medium_id == $med->id ? 'selected':'' }}>{{$med->medium_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            @if(Auth::user()->hasRole(SUPER_ADMIN_ROLE) == 1)
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('name', __('common.label.branch'),['class' => 'custom-label asterisk'])}}
                        <select name="branch_id" id="branch_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="branch is required.">
                        <option value="">Select branch</option>
                            @if(isset($branch))
                                @foreach($branch as $bra)
                                    <option value="{{$bra->id}}" {{(isset($batch->branch_id)) && $batch->branch_id == $bra->id ? 'selected':'' }}>{{$bra->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-sm-4">
                <div class="form-group text-center">
                    <div class="image-container add_logo">
                        
                        @if(!empty($batch->batch_image) && file_exists(public_path('uploads/batch') . "/".$batch->batch_image))
                            <img src="{{asset('/uploads/batch/'.$batch->batch_image)}}" id="edit_image">
                            <span id="delete_btn" class="close-btn remove_client_logo">×</span>
                        @else
                            <img src="{{asset('img/client-logo.jpg')}}" style="width: 50%" id="default_logo">
                        @endif
                    </div>
                    <div class="image-grid">
                        <div class="image-container"></div>
                    </div>
                    <input id="logo_remove_flag" type="hidden" name="logo_remove_flag" />
                    <div class="custom-file">
                        <input class="image" id="image" type="file" name="image" placeholder="image" accept="image/*"  />
                        <label for="image" class="action-link">@lang('common.label.timetable_image')</label>
                    </div>
                    <input id="image_name" type="hidden" name="image_name" />
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateBatch','title'=> __('common.label.save')])}}
                {{link_to_route('batch.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>

    <div class="modal fade in" id="image-crop-modal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Crop Image</strong></h4>
                </div>
                <div class="modal-body">
                    <canvas id="image-crop-canvas" width="100%"></canvas>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom btn-theme font-size-sm crop-image">Crop</button>
                    <button type="button" class="btn btn-custom btn-secondary-2 font-size-sm close-popup" >Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_css')
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/main.css')}}" rel="stylesheet">
<link  href="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/css/cropper.css')}}" rel="stylesheet">
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'jquery-cropper-master/docs/js/cropper.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'batch/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection