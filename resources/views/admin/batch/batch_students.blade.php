@extends('layouts.app')
@section('title') 
    @lang('common.label.batch')
@endsection
@section('content')

<div class="custom-card">
    <div class="row">
        <div class="col-lg-2 col-sm-2">
            <h3 class="title">@lang('common.label.batch')</h3> Total Students : {{$results->count()}} 
        </div>
        <div class="col-lg-10 col-sm-10">
            <div class="table-header">
                <ul>
                    @if(count($results) > 0)
                        <li>
                            <h3 class="text-primary">{{$results[0]->year}} / {{$results[0]->batch_code}} / {{$results[0]->board_name}} / {{$results[0]->medium_name}} / {{$results[0]->branch_name}}</h3>
                        </li>
                    @endif
                    <li>
                        {{link_to_route('batch.list', __('common.label.back_btn'),['selected_tab'=>'bulk_import'], ['class'=>'btn btn-custom font-size-sm btn-secondary-2 with-arrow-left-20'])}}
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12">
            @include('admin.common.form_error_message')
            @include('admin.common.flash_message')
        </div>
    </div>
    <div class="">
        <div class="table-responsive">
            <table id="exam_student_table" class="table table-custom table-bordered" style="word-wrap:break-word;">
                <thead>
                    <tr>
                        <th>@lang('common.label.roll_number')</th>
                        <th>@lang('common.label.student_name')</th>
                        <th>@lang('common.label.email')</th>
                        <th>@lang('common.label.mobile_number')</th>
                        <th>@lang('common.label.father_name')</th>
                        <th>@lang('common.label.mobile_number')</th>
                        <th>@lang('common.label.school_name')</th>
                        <th>@lang('common.label.admission_date')</th>
                    </tr>
                </thead>
                    @if(count($results) > 0)
                        @foreach ($results as $res)
                        <tr>
                            <td>{{$res->roll_number}}</td>
                            <td>{{$res->student_name}}</td>
                            <td>{{$res->student_email}}</td>
                            <td>{{$res->student_mobile_number}}</td>
                            <td>{{$res->father_name}}</td>
                            <td>{{$res->father_mobile_number}}</td>
                            <td>{{$res->school_name}}</td>
                            <td>{{date('d-M-Y',strtotime($res->admission_date))}}</td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('common.label.no_record_found')</td>
                        </tr>
                    @endif
            </table>
            
            
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'exam/list.js')}}"></script>

<script type="text/javascript">
    <?php /*
    function enter_obtain_marks(id,totalMarks,marks)
    {
        if(id != '')
        {  
            var attendance_status = $('#attendance_status_'+id).val();
            $.ajax({
                type: "POST",
                url: BASE_URL +'exam/submitObtainMarks',
                dataType: 'json',
                data: {'id': id, 'total_marks': totalMarks,'marks': marks,'attendance_status': attendance_status},
                beforeSend: function ()
                {
                    $('.loader-outer').show();
                },
                success: function(data)
                {
                    $('.loader-outer').hide();
                    if(data.status == 1){
                        swal("Sucess",data.message);
                    }else if(data.status == 2){
                        swal("Warning",data.message);
                    }else{
                        swal("Error",data.message);
                    }
                }
            });
            return false;
        }
    }
    */ ?>

    
    $(".attendance_status").on('change',function(){
        var attendance_status = $(this).val();
        var id = $(this).attr('data-id');
        //alert(attendance_status)
        if(attendance_status == 3 || attendance_status == 1){
            $('#attendance_mark_'+id).val('0');
            $('#attendance_mark_'+id).attr("disabled", true);
            /*$.ajax({
                type: "POST",
                url: BASE_URL +'exam/submitObtainMarks',
                dataType: 'json',
                data: {'id': id, 'marks': 0,'attendance_status': attendance_status},
                beforeSend: function ()
                {
                    $('.loader-outer').show();
                },
                success: function(data)
                {
                    $('.loader-outer').hide();
                    if(data.status == 1){
                        swal("Sucess",data.message);
                    }else if(data.status == 2){
                        swal("Warning",data.message);
                    }else{
                        swal("Error",data.message);
                    }
                }
            });*/
        }else{
            $('#attendance_mark_'+id).val('');
            $('#attendance_mark_'+id).attr("disabled", false);
        }
      });
   
</script>
@endsection