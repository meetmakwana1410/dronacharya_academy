<div class="action-column">
	<div class="custom-switch">
        <input type="checkbox" id="post_status_{{$object->id}}" name="" class="switch-input action-status" data-action-href="{{URL::signedRoute('batch.activedeactive',[$object->id])}}" @if($object->status) checked="" @endif    >
        <label for="post_status_{{$object->id}}" class="switch-label"></label>
    </div>
    <a href="{{URL::signedRoute('batch.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
        <span class="edit-icon"></span>
    </a>
    <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('batch.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
    <a href="{{URL::route('batch.getstudent',[$object->id])}}" class="view-action" title="{{ __('students') }}">
        <span class="view-icon"></span>
    </a>
</div>