@extends('layouts.app')
@section('title') 
@if (isset($academic_year) && !empty($academic_year))
    @lang('common.label.edit_academic_year')
@else
    @lang('common.label.add_academic_year')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($academic_year) && !empty($academic_year))
                @lang('common.label.edit_academic_year')
            @else
                @lang('common.label.add_academic_year')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateAcademicYear','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($academic_year) && !empty($academic_year))?URL::signedRoute('academic_year.edit',[$academic_year->id]): route('academic_year.add')]) !!}  
        @if (isset($academic_year) && !empty($academic_year))
            {{FORM::hidden('id',$academic_year->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.year'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('year', (isset($academic_year) && !empty($academic_year))?$academic_year->year:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('year'))
                        <div class="parsley-errors-list">{{ $errors->first('year') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateAcademicYear','title'=> __('common.label.save')])}}
                {{link_to_route('academic_year.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'academic_year/add_edit.js')}}"></script>
@endsection