<div class="action-column">
    <a href="{{URL::signedRoute('standards.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
        <span class="edit-icon"></span>
    </a>
    <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('standards.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
        <span class="delete-icon"></span>
    </a>
</div>