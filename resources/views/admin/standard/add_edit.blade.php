@extends('layouts.app')
@section('title') 
@if (isset($standards) && !empty($standards))
    @lang('common.label.edit_standards')
@else
    @lang('common.label.add_standards')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($standards) && !empty($standards))
                @lang('common.label.edit_standards')
            @else
                @lang('common.label.add_standards')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateStandards','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($standards) && !empty($standards))?URL::signedRoute('standards.edit',[$standards->id]): route('standards.add')]) !!}  
        @if (isset($standards) && !empty($standards))
            {{FORM::hidden('id',$standards->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.standards_name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('standard_name', (isset($standards) && !empty($standards))?$standards->standard_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('standard_name'))
                        <div class="parsley-errors-list">{{ $errors->first('standard_name') }}</div>
                    @endif
                </div>
            </div>
            
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateStandards','title'=> __('common.label.save')])}}
                {{link_to_route('standards.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'standards/add_edit.js')}}"></script>
@endsection