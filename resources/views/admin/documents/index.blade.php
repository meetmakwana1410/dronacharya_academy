@extends('layouts.app')
@section('title') 
    @lang('common.label.documents')
@endsection
@section('content')

<div class="custom-card">
    <div class="row">
        <div class="col-lg-4 col-sm-4">
            <h3 class="title">@lang('common.label.documents')</h3>
        </div>
        <div class="col-lg-8 col-sm-8">
            <div class="table-header">
                <ul>
                    <li>
                        @include('admin.common.record_per_page_option')
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="btn btn-custom font-size-sm btn-secondary-2" id="reset_btn">@lang('common.label.reset')</a>
                    </li>
                    <li>
                        <a href="{{route('documents.add')}}" class="btn btn-custom font-size-sm btn-secondary-1">@lang('common.label.add_documents')</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12">
            @include('admin.common.form_error_message')
            @include('admin.common.flash_message')
        </div>
    </div>
    <div class="">
        <div class="table-responsive">
            <table id="documents_table" class="table table-custom table-bordered">
                <thead>
                    <tr>
                        <th>
                            @lang('common.label.batch')
                        </th>
                        <th>
                            @lang('common.label.subjects')
                        </th>
                        <th>
                            @lang('common.label.name')
                        </th>
                        <th>
                            @lang('common.label.documents')
                        </th>
                        <th>
                            @lang('common.label.uploaded')
                        </th>
                        <th class="non-searchable action-column action-one">@lang('common.label.action')</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{asset(JS_FOLDER_URL.'datatables/datatables.min.css')}}"/>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'documents/list.js')}}"></script>
@endsection