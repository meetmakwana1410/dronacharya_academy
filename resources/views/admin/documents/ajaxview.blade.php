
<table id="documents_table" class="table table-custom table-bordered">
    <thead>
        <tr>
            <th>
                @lang('common.label.batch')
            </th>
            <th>
                @lang('common.label.subjects')
            </th>
            <th>
                @lang('common.label.name')
            </th>
            <th>
                @lang('common.label.documents')
            </th>
            <th>
                @lang('common.label.uploaded')
            </th>
            <th class="non-searchable action-column action-three">@lang('common.label.action')</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(!empty($result)){
                //print_r($result);
                foreach ($result as $key => $value) {
                    ?>
                        <tr>
                            <td><?php echo $value->batch_code;?></td>
                            <td><?php echo $value->subject_name;?></td>
                            <td><?php echo $value->name;?></td>
                            <td>
                                <?php 
                                    if(!empty($value->document) && file_exists(public_path().'/'.$this->foldername.'/'.$value->document))
                                    { 
                                        if (in_array($value->extension, ["jpg", "png", "jpeg"])){
                                            $exp = '<a href="'.asset($this->foldername.'/'.$value->document).'" target="_blank"><img src="'.asset('/img'.'/doc_img.png').'" style="width: 70px;" id="default_logo"></a>';
                                        }elseif($value->extension == 'pdf'){
                                            $exp = '<a href="'.asset($this->foldername.'/'.$value->document).'" target="_blank" ><img src="'.asset('/img'.'/doc_pdf.png').'" style="width: 70px;" id="default_logo"></a>';
                                        }
                                        
                                    }
                                ?>
                            </td>
                            <td><?php echo date('j F, Y',strtotime($value->created_at));?></td>
                            <td>asd</td>
                        </tr>
                    <?php
                }
            }

        ?>
    </tbody>
</table>
        