@extends('layouts.app')
@section('title') 
@if (isset($exam) && !empty($exam))
    @lang('common.label.edit_exam')
@else
    @lang('common.label.add_exam')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<link href="{{asset(CSS_FOLDER_URL.'bootstrap-3.min.css')}}" rel="stylesheet" />
<link href="{{asset(CSS_FOLDER_URL.'bootstrap-datetimepicker.css')}}" rel="stylesheet" />

<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>

    <div class="custom-card">
        <h3 class="title">
            @if (isset($exam) && !empty($exam))
                @lang('common.label.edit_exam')
            @else
                @lang('common.label.add_exam')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateExam','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($exam) && !empty($exam))?URL::signedRoute('exam.edit',[$exam->id]): route('exam.add')]) !!}  
        @if (isset($exam) && !empty($exam))
            {{FORM::hidden('id',$exam->id)}}
        @endif 
        {{csrf_field()}}
        

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label asterisk'])}}
                    <select name="batch_id" id="batch_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required.">
                    <option value="">Select Batch Code</option>
                        @if(isset($batch))
                            @foreach($batch as $btc)
                                <option value="{{$btc->id}}" {{(isset($exam->batch_id)) && $exam->batch_id == $btc->id ? 'selected':'' }}>{{$btc->batch_name}} ({{$btc->branch_name}})</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.total_marks'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('total_marks', (isset($exam) && !empty($exam))?$exam->total_marks:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('total_marks'))
                        <div class="parsley-errors-list">{{ $errors->first('total_marks') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.exam_category'),['class' => 'custom-label asterisk'])}}
                    <select name="exam_category_id" id="exam_category_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Exam category is required.">
                    <option value="">Select Exam category</option>
                        @if(isset($examType))
                            @foreach($examType as $et)
                                <option value="{{$et->id}}"  @if(!empty($exam->exam_category_id) && ($exam->exam_category_id == $et->id))  selected  @endif  >{{$et->exam_type_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.exam_date'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('exam_date', (isset($exam) && !empty($exam))?date('d-m-Y',strtotime($exam->exam_date)):'',['class' => 'form-control custom-input-style2 parsley-validated','id'=>'exam_date','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('exam_date'))
                        <div class="parsley-errors-list">{{ $errors->first('exam_date') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.exam_time'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('exam_time', (isset($exam) && !empty($exam))?date('h:i A',strtotime($exam->exam_time)):'',['class' => 'form-control custom-input-style2 parsley-validated','id'=>'exam_time','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('exam_time'))
                        <div class="parsley-errors-list">{{ $errors->first('exam_time') }}</div>
                    @endif
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.to_exam_time'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('to_exam_time', (isset($exam) && !empty($exam))?date('h:i A',strtotime($exam->to_exam_time)):'',['class' => 'form-control custom-input-style2 parsley-validated','id'=>'to_exam_time','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('to_exam_time'))
                        <div class="parsley-errors-list">{{ $errors->first('to_exam_time') }}</div>
                    @endif
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.subject'),['class' => 'custom-label asterisk'])}}
                    <select name="subject_id" id="subject_id" class="form-control select2"  data-parsley-trigger="change focusout" required data-parsley-required-message="Batch code is required.">
                    <option value="">Select Subject</option>
                        @if(isset($batchSubject))
                            @foreach($batchSubject as $btcs)
                                <option value="{{$btcs->subject_id}}"  @if(!empty($exam->subject_id) && ($btcs->subject_id == $exam->subject_id))  selected  @endif  >{{$btcs->subject_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.chapter'),['class' => 'custom-label'])}}
                    {{Form::text('chapter', (isset($exam) && !empty($exam))?$exam->chapter:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                </div>
            </div>

            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.exam_paper_prepared_by'),['class' => 'custom-label'])}}
                    <select name="teacher_id" id="teacher_id" class="form-control select2"  data-parsley-trigger="change focusout">
                    <option value="">Select teacher</option>
                        @if(isset($teacher))
                            @foreach($teacher as $btc)
                                <option value="{{$btc->id}}" {{(isset($exam->teacher_id)) && $exam->teacher_id == $btc->id ? 'selected':'' }}>{{$btc->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <input type="hidden" id="url" name="url" value="{{asset('/')}}">
        </div>
        
        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">

                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateExam','title'=> __('common.label.save')])}}
               
                {{link_to_route('exam.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection


@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'moment-with-locales.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'exam/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection