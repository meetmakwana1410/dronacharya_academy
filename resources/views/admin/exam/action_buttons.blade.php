<div class="action-column">
	<div class="custom-switch">
        <input type="checkbox" id="post_status_{{$object->id}}" name="" class="switch-input action-status" data-action-href="{{URL::signedRoute('exam.activedeactive',[$object->id])}}" @if($object->status) checked="" @endif    >
        <label for="post_status_{{$object->id}}" class="switch-label"></label>
    </div>

    
        <a href="{{URL::signedRoute('exam.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
            <span class="edit-icon"></span>
        </a>
        <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('exam.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
            <span class="delete-icon"></span>
        </a>
    
        <a href="{{URL::route('exam.getexamstudent',[$object->id])}}" class="view-action" title="@lang('common.label.exam_students')">
            <span class="view-icon"></span>
        </a>
    
    
    <span class="btn btn-custom font-size-sm <?php if($object->exam_mark_status == 1) { echo 'btn-secondary-1';}else{echo 'btn-warning';}?>" style="padding: 5px 10px;font-size: 1.2rem;font-weight: 600;cursor: none;">
        @if($object->exam_mark_status == 1)
            Submitted
        @else
            Pending
        @endif
    </span>
</div>