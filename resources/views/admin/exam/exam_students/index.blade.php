@extends('layouts.app')
@section('title') 
    @lang('common.label.exam')
@endsection
@section('content')

<div class="custom-card">
    <div class="row">
        <div class="col-lg-2 col-sm-2">
            <h3 class="title">@lang('common.label.exam')</h3>
        </div>
        <div class="col-lg-10 col-sm-10">
            <div class="table-header">
                <ul>
                    @if(count($results) > 0)
                        <li>
                            <h3 class="text-primary">{{$results[0]->batch_code}}</h3>
                        </li>
                        <li>
                            <h3 class="text-primary-2">{{$results[0]->subject_name}}</h3>
                        </li>
                        <li>
                            <h3 class="text-primary-2">({{date('d-M-Y',strtotime($results[0]->exam_date))}},  {{date('g:i a',strtotime($results[0]->exam_time))}} - {{date('g:i a',strtotime($results[0]->to_exam_time))}})</h3>
                        </li>
                    @endif
                    <li>
                        {{link_to_route('exam.list', __('common.label.back_btn'),['selected_tab'=>'bulk_import'], ['class'=>'btn btn-custom font-size-sm btn-secondary-2 with-arrow-left-20'])}}
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12">
            @include('admin.common.form_error_message')
            @include('admin.common.flash_message')
        </div>
    </div>
    <div class="">
        <div class="table-responsive">
            {!! Form::open(['method'=>'post','id'=>'formAddUpdateExamMarks','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => route('exam.submitMark',[Request::segment(3)])]) !!} 
            <table id="exam_student_table" class="table table-custom table-bordered">
                <thead>
                    <tr>
                        <th>
                            @lang('common.label.roll_number')
                        </th>
                        <th>
                            @lang('common.label.student_name')
                        </th>
                        <th>
                            @lang('common.label.total_obtain_marks')
                        </th>
                        <th>
                            @lang('common.label.percentage')
                        </th>
                        <th>
                            @lang('common.label.attendance_status')
                        </th>
                        <th>
                            @lang('common.label.enter_mark')
                        </th>
                    </tr>
                </thead>
                    @if(count($results) > 0)
                        @foreach ($results as $res)
                        <tr>
                            <td>{{$res->roll_number}}</td>
                            <td>{{$res->student_name}}</td>
                            <td>{{$res->total_marks}} / {{$res->obtain_marks}}</td>
                            <td>{{$res->percentage}}</td>
                            <td>
                                <input type="hidden" name="student_id[]" value="{{$res->student_id}}">
                                <input type="hidden" name="exam_students_id[]" value="{{$res->id}}">
                                <input type="hidden" name="total_marks[]" value="{{$res->total_marks}}">
                                <select name="attendance_status[]" class="attendance_status" id="attendance_status_<?= $res->id ?>" data-id="<?= $res->id ?>">
                                    <option @if(!empty($res) && ($res->status == 2))  selected  @endif value="2">Present</option>
                                    <option  @if(!empty($res) && ($res->status == 1))  selected  @endif   value="1">Absent</option>
                                    <option @if(!empty($res) && ($res->status == 3))  selected  @endif value="3">N/A</option>
                                </select>
                            </td>
                            <td>
                                <?php 
                                    $marks = '';
                                    if(!empty($res) && $res->status == 2){
                                        if($res->obtain_marks)
                                        {
                                            $marks = $res->obtain_marks;
                                        }
                                    }if(!empty($res) && ($res->status == 1 || $res->status == 3)){
                                        $marks = '0';
                                    }

                                ?>
                                <input type="text" required="" max="<?php echo $res->total_marks;?>" id="attendance_mark_<?= $res->id ?>"  onchange="enter_obtain_marks(<?= $res->id ?>,<?= $res->total_marks ?>,this.value);" name="obtain_mark[]" value="<?=$marks?>" style="width: 60px;">
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('common.label.no_record_found')</td>
                        </tr>
                    @endif
            </table>
            @if(count($results) > 0)
                <div class="col-lg-12 col-sm-12">
                    <div class="form-btn-group text-right">
                        {{Form::submit( __('common.label.update_notify') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateExam','title'=> __('common.label.update_notify')])}}
                    </div>
                </div>
            @endif
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_FOLDER_URL.'datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'exam/list.js')}}"></script>

<script type="text/javascript">
    <?php /*
    function enter_obtain_marks(id,totalMarks,marks)
    {
        if(id != '')
        {  
            var attendance_status = $('#attendance_status_'+id).val();
            $.ajax({
                type: "POST",
                url: BASE_URL +'exam/submitObtainMarks',
                dataType: 'json',
                data: {'id': id, 'total_marks': totalMarks,'marks': marks,'attendance_status': attendance_status},
                beforeSend: function ()
                {
                    $('.loader-outer').show();
                },
                success: function(data)
                {
                    $('.loader-outer').hide();
                    if(data.status == 1){
                        swal("Sucess",data.message);
                    }else if(data.status == 2){
                        swal("Warning",data.message);
                    }else{
                        swal("Error",data.message);
                    }
                }
            });
            return false;
        }
    }
    */ ?>

    
    $(".attendance_status").on('change',function(){
        var attendance_status = $(this).val();
        var id = $(this).attr('data-id');
        //alert(attendance_status)
        if(attendance_status == 3 || attendance_status == 1){
            $('#attendance_mark_'+id).val('0');
            $('#attendance_mark_'+id).attr("disabled", true);
            /*$.ajax({
                type: "POST",
                url: BASE_URL +'exam/submitObtainMarks',
                dataType: 'json',
                data: {'id': id, 'marks': 0,'attendance_status': attendance_status},
                beforeSend: function ()
                {
                    $('.loader-outer').show();
                },
                success: function(data)
                {
                    $('.loader-outer').hide();
                    if(data.status == 1){
                        swal("Sucess",data.message);
                    }else if(data.status == 2){
                        swal("Warning",data.message);
                    }else{
                        swal("Error",data.message);
                    }
                }
            });*/
        }else{
            $('#attendance_mark_'+id).val('');
            $('#attendance_mark_'+id).attr("disabled", false);
        }
      });
   
</script>
@endsection