<head>
    <div class="head-section">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-lg-6 col-3">
                    <a id="toggle_nav" href="javascript:void(0)" class="toggle-nav">
                        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/03_Menu_close.svg') }}" width="30" alt="Menu" class="close-nav" />
                        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/04_Menu_Expand.svg') }}" width="30" alt="Menu" class="expand-nav" />
                    </a>
                </div> -->

                <div class="col-lg-12 col-9">
                    <ul class="profile-nav">
                        <li class="dropdown">
                            <a href="JavaScript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="name">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- <li>
                                    <a href="javascript:void(0)" class="dropdown-item">My Profile</a>
                                </li> -->
                                <li><a href="{{ route('school.list') }}" title="@lang('common.label.school')">@lang('common.label.school')</a></li>
                                <li><a href="{{ route('notifications') }}" title="@lang('common.label.notification')">@lang('common.label.notification')</a></li>
                                <li><a href="{{ route('teacher.list') }}" title="@lang('common.label.teacher')">@lang('common.label.teacher')</a></li>
                                @if(Auth::user()->hasRole(SUPER_ADMIN_ROLE) == 1)
                                    <li><a href="{{ route('standards.list') }}" title="@lang('common.label.standards')">@lang('common.label.standards')</a></li>
                                    <li><a href="{{ route('subject.list') }}" title="@lang('common.label.subjects')">@lang('common.label.subjects')</a></li>
                                    <li><a href="{{ route('academic_year.list') }}" title="@lang('common.label.academic_year')">@lang('common.label.academic_year')</a></li>
                                    <li><a href="{{ route('branch.list') }}" title="@lang('common.label.branch')">@lang('common.label.branch')</a></li>
                                    <li><a href="{{ route('exam_type.list') }}" title="@lang('common.label.exam_type')">@lang('common.label.exam_type')</a></li>
                                    <li><a href="{{ route('medium.list') }}" title="@lang('common.label.medium')">@lang('common.label.medium')</a></li>
                                    <li><a href="{{ route('board.list') }}" title="@lang('common.label.board')">@lang('common.label.board')</a></li>
                                @endif
                                <li><a href="{{ route('change.password.get') }}" title="@lang('common.label.change_password')">@lang('common.label.change_password')</a></li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</head>