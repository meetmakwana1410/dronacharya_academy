<aside class="main-navigation">
    <a href="{{ route('students.list') }}" class="logo">
        <img src="{{ asset(IMAGES_FOLDER_URL.'logo.svg') }}" width="80" alt="logo" class="biglogo" />
        <img src="{{ asset(IMAGES_FOLDER_URL.'logo.svg') }}" width="50" alt="logo-mini" class="minilogo" />
    </a>
    <ul id="main_navigation" class="main-nav">
    	<li><a href="{{route('dashboard.list')}}" class="{{ ((in_array(Route::currentRouteName(),['dashboard.list'])) ? 'active' : '') }}" title="@lang('common.label.dashboard')"><span>@lang('common.label.dashboard')</span></a></li>

        <li><a href="{{route('students.list')}}" title="@lang('common.label.students')" class="{{ ((in_array(Route::currentRouteName(),['students.list','students.add','students.edit'])) ? 'active' : '') }}"><span>@lang('common.label.students')</span></a></li>

        <?php /*
        <li><a href="{{route('notifications')}}"  title="@lang('common.label.notification')" class="{{ ((in_array(Route::currentRouteName(),['notifications','notifications.add'])) ? 'active' : '') }}"><span>@lang('common.label.notification')</span></a></li>
        */ ?>

        <li><a href="{{route('exam.list')}}"  title="@lang('common.label.exam')" class="{{ ((in_array(Route::currentRouteName(),['exam.list','exam.add','exam.edit','exam.getexamstudent'])) ? 'active' : '') }}"><span>@lang('common.label.exam')</span></a></li>
        
        <li><a href="{{route('homework.list')}}"  title="@lang('common.label.homework')" class="{{ ((in_array(Route::currentRouteName(),['homework.list','homework.edit','homework.gethomework'])) ? 'active' : '') }}"><span>@lang('common.label.homework')</span></a></li>

        <li><a href="{{route('batch.list')}}"  title="@lang('common.label.batch')" class="{{ ((in_array(Route::currentRouteName(),['batch.list','batch.add','batch.edit'])) ? 'active' : '') }}"><span>@lang('common.label.batch')</span></a></li>
        
        <li><a href="{{route('inquiry.list')}}"  title="@lang('common.label.inquiry')" class="{{ ((in_array(Route::currentRouteName(),['inquiry.list','inquiry.edit','inquiry.show'])) ? 'active' : '') }}"><span>@lang('common.label.inquiry')</span></a></li>

        <li><a href="{{route('documents.list')}}"  title="@lang('common.label.documents')" class="{{ ((in_array(Route::currentRouteName(),['documents.list','documents.add','documents.edit'])) ? 'active' : '') }}"><span>@lang('common.label.documents')</span></a></li>

        <?php /*
        <li><a href="{{route('school.list')}}"  title="@lang('common.label.school')" class="{{ ((in_array(Route::currentRouteName(),['school.list','school.add','school.edit'])) ? 'active' : '') }}"><span>@lang('common.label.school')</span></a></li>
        */ ?>
        
        @if(Auth::user()->hasRole(SUPER_ADMIN_ROLE) == 1)
            <?php /*
            <li><a href="{{route('teacher.list')}}"  title="@lang('common.label.teacher')" class="{{ ((in_array(Route::currentRouteName(),['teacher.list','teacher.add','teacher.edit'])) ? 'active' : '') }}"><span>@lang('common.label.teacher')</span></a></li>
            
            <li><a href="{{route('medium.list')}}"  title="@lang('common.label.medium')" class="{{ ((in_array(Route::currentRouteName(),['medium.list','medium.add','medium.edit'])) ? 'active' : '') }}"><span>@lang('common.label.medium')</span></a></li>
            
            <li><a href="{{route('exam_type.list')}}"  title="@lang('common.label.exam_type')" class="{{ ((in_array(Route::currentRouteName(),['exam_type.list','exam_type.add','exam_type.edit'])) ? 'active' : '') }}"><span>@lang('common.label.exam_type')</span></a></li>

            <li><a href="{{route('board.list')}}"  title="@lang('common.label.board')" class="{{ ((in_array(Route::currentRouteName(),['board.list','board.add','board.edit'])) ? 'active' : '') }}"><span>@lang('common.label.board')</span></a></li>

            <li><a href="{{route('standards.list')}}"  title="@lang('common.label.standards')" class="{{ ((in_array(Route::currentRouteName(),['standards.list','standards.add','standards.edit'])) ? 'active' : '') }}"><span>@lang('common.label.standards')</span></a></li>

            <li><a href="{{route('subject.list')}}"  title="@lang('common.label.subjects')" class="{{ ((in_array(Route::currentRouteName(),['subject.list','subject.add','subject.edit'])) ? 'active' : '') }}"><span>@lang('common.label.subjects')</span></a></li>

            <li><a href="{{route('branch.list')}}"  title="@lang('common.label.branch')" class="{{ ((in_array(Route::currentRouteName(),['branch.list','branch.edit','branch.add'])) ? 'active' : '') }}"><span>@lang('common.label.branch')</span></a></li>
            
            <li><a href="{{route('academic_year.list')}}"  title="@lang('common.label.academic_year')" class="{{ ((in_array(Route::currentRouteName(),['academic_year.list','academic_year.add','academic_year.edit'])) ? 'active' : '') }}"><span>@lang('common.label.academic_year')</span></a></li>
            */ ?>
        @endif
      </ul>
</aside>