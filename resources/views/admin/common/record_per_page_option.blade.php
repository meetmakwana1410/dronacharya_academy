<span>@lang('common.label.records')</span>
<select class="record-perpage" id="record_perpage">
    <option value="10">10</option>
    <option value="25">25</option>
    <option value="50">50</option>
    <option value="100">100</option>
</select>