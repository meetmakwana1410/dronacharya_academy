<link rel="apple-touch-icon" sizes="57x57" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset(IMAGES_FOLDER_URL.'favicon/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset(IMAGES_FOLDER_URL.'favicon/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset(IMAGES_FOLDER_URL.'favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset(IMAGES_FOLDER_URL.'favicon/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset(IMAGES_FOLDER_URL.'favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset(IMAGES_FOLDER_URL.'favicon/manifest.json') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset(IMAGES_FOLDER_URL.'favicon/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">