<div class="action-column">
    <div class="custom-switch">
        <input type="checkbox" id="post_status_{{$object->id}}" name="" class="switch-input action-status" data-action-href="{{URL::signedRoute('board.activedeactive',[$object->id])}}" @if($object->status) checked="" @endif    >
        <label for="post_status_{{$object->id}}" class="switch-label"></label>
    </div>
    <a href="{{URL::signedRoute('board.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
        <span class="edit-icon"></span>
    </a>
    <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('board.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
        <span class="delete-icon"></span>
    </a>
</div>