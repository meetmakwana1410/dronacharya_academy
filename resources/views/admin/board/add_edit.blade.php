@extends('layouts.app')
@section('title') 
@if (isset($board) && !empty($board))
    @lang('common.label.edit_board')
@else
    @lang('common.label.add_board')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($board) && !empty($board))
                @lang('common.label.edit_board')
            @else
                @lang('common.label.add_board')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdateBoard','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($board) && !empty($board))?URL::signedRoute('board.edit',[$board->id]): route('board.add')]) !!}  
        @if (isset($board) && !empty($board))
            {{FORM::hidden('id',$board->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.board_name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('board_name', (isset($board) && !empty($board))?$board->board_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('board_name'))
                        <div class="parsley-errors-list">{{ $errors->first('board_name') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.board_full_name'),['class' => 'custom-label'])}}
                    {{Form::text('full_name', (isset($board) && !empty($board))?$board->full_name:'',['class' => 'form-control custom-input-style2'])}}
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdateBoard','title'=> __('common.label.save')])}}
                {{link_to_route('board.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'board/add_edit.js')}}"></script>
@endsection