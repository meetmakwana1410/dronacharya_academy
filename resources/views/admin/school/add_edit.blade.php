@extends('layouts.app')
@section('title') 
@if (isset($data) && !empty($data))
    @lang('common.label.edit_school')
@else
    @lang('common.label.add_school')
@endif
@endsection
@section('content')
    <div class="custom-card">
        <h3 class="title">
            @if (isset($data) && !empty($data))
                @lang('common.label.edit_school')
            @else
                @lang('common.label.add_school')
            @endif
        </h3>
        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($data) && !empty($data))?URL::signedRoute('school.edit',[$data->id]): route('school.add')]) !!}  
        @if (isset($data) && !empty($data))
            {{FORM::hidden('id',$data->id)}}
        @endif 
        {{csrf_field()}}
        <div class="row">
            
        </div>
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('school_name', (isset($data) && !empty($data))?$data->school_name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('school_name'))
                        <div class="parsley-errors-list">{{ $errors->first('school_name') }}</div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                {{link_to_route('school.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'school/add_edit.js')}}"></script>
@endsection