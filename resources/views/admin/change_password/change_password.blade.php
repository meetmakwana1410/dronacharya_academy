@extends('layouts.app')
@section('title') 
    @lang('common.label.change_password')
@endsection
@section('content')
<div class="custom-card">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="title">@lang('common.label.change_password')</h3>
            <form action="{{ route('change.password.post') }}" method="post" data-parsley-validate>
                {{csrf_field()}}
                <div class="form-block formfeild2">
                    <div class="form-group">
                        <label class="control-label">@lang('common.label.curr_password')*:</label>
                        <input type="password" name="old_password" data-parsley-trigger="change focusout" required="" data-parsley-required-message="@lang('common.message.curr_password_required')"data-parsley-minlength="6" data-parsley-minlength-message="@lang('common.message.password_minlength')">
                        @if ($errors->has('old_password'))
                            <div class="parsley-errors-list">{{ $errors->first('old_password') }}</div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label class="control-label">@lang('common.label.new_password')*:</label>
                            <input type="password" name="new_password" data-parsley-trigger="change focusout" required="" data-parsley-required-message="@lang('common.message.new_password_required')"data-parsley-minlength="6" data-parsley-minlength-message="@lang('common.message.password_minlength')">
                            @if ($errors->has('new_password'))
                                <div class="parsley-errors-list">{{ $errors->first('new_password') }}</div>
                            @endif
                        </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                        <div class="form-group">
                            <label class="control-label">@lang('common.label.confirm_password')*:</label>
                            <input type="password" name="new_confirm_password" data-parsley-trigger="change focusout" required="" data-parsley-required-message="@lang('common.message.confirm_password_required')"data-parsley-minlength="6" data-parsley-minlength-message="@lang('common.message.password_minlength')">
                            @if ($errors->has('new_confirm_password'))
                                <div class="parsley-errors-list">{{ $errors->first('new_confirm_password') }}</div>
                            @endif
                        </div>
                        </div>
                    </div>
                </div><!--/.form-block-->
                
                <div class="formsubmitbtn">
                    <input type="submit" name="save" value="@lang('common.label.save')" class="btn btn-custom font-size-sm btn-secondary-1">
                    <a href="home" class="btn btn-custom font-size-sm btn-secondary-2 canclebtn">@lang('common.label.cancel')</a>    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection