<div class="action-column">
   
	<div class="custom-switch">
        <input type="checkbox" id="post_status_{{$object->id}}" name="" class="switch-input action-status" data-action-href="{{URL::signedRoute('inquiry.activedeactive',[$object->id])}}" @if($object->status) checked="" @endif    >
        <label for="post_status_{{$object->id}}" class="switch-label"></label>
    </div>

    <a href="{{URL::signedRoute('inquiry.edit',[$object->id])}}" class="edit-action" title="@lang('common.label.edit')">
        <span class="edit-icon"></span>
    </a>

    <a href="javascript:void(0);" data-action-href="{{URL::signedRoute('inquiry.delete',[$object->id])}}" class="delete-action" title="{{ __('Delete') }}">
        <span class="delete-icon"></span>
    </a>
    <a href="{{route('inquiry.show',[$object->id])}}" class="view-action" title="@lang('common.label.show')">
        <span class="view-icon"></span>
    </a>
</div>