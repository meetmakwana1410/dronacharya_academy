@extends('layouts.app')
@section('title') 
    @lang('common.label.inquiry_details')
@endsection

@section('content')
<!-- Client Info -->
<div class="custom-card">
    <div class="row">        
        <div class="col-lg-4 col-sm-6 order-lg-8 order-sm-6 text-right">
            <a href="{{URL::route('inquiry.list')}}" class="btn btn-custom font-size-sm btn-secondary-2" id="reset_btn"title="{{ __('common.label.back_btn') }}">{{ __('common.label.back_btn') }}</a>
        </div>
        <div class="col-lg-8 col-sm-6">
            <h3 class="title">@lang('common.label.inquiry_details')
            <span class="btn btn-custom font-size-sm <?php if($inquiry[0]->status == 1) { echo 'btn-secondary-1';}else{echo 'btn-warning';}?>" style="padding: 5px 10px;font-size: 1.2rem;font-weight: 200;cursor: none;">
                @if($inquiry[0]->status == 1)
                    Confirm
                @else
                    Pending
                @endif
            </span>
            </h3>
        </div>
    </div>
    <div class="logo-detail">
        <div class="detail view-detail-font">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.name')</label>
                        <p>{{  !empty($inquiry)?$inquiry[0]->name:'-' }}</p>
                    </div>
                </div>
                
                <div class="col-lg-3 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.school')</label>
                        <p>{{  !empty($inquiry)?$inquiry[0]->school_name:'-' }}  / {{ !empty($inquiry)?$inquiry[0]->standard:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.school_timings')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->school_timings:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.created_on')</label>
                        <p>{{  !empty($inquiry)?date('j F Y, h:i A',strtotime($inquiry[0]->created_at)):'-' }}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.primary_mobile_number')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->primary_mobile_number:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.secondary_mobile_number')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->secondary_mobile_number:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.student_mobile_number')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->student_mobile_number:'-' }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.parents_email')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->parents_email:'-' }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.students_email')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->students_email:'-' }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.residence_address')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->residence_address:'-' }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-6">
                    <div class="form-group view-detail-font">
                        <label>@lang('common.label.remark')</label>
                        <p>{{ !empty($inquiry)?$inquiry[0]->remark:'-' }}</p>
                    </div>
                </div>
            </div>
        </div> 
    </div>

    <div class="logo-detail">
        <div class="detail view-detail-font">
            @include('admin.common.flash_message')
            {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => route('inquiry.inquiry-follow-up')]) !!}  
            @if (isset($inquiry) && !empty($inquiry))
                {{FORM::hidden('id',$inquiry[0]->id)}}
            @endif 
            {{csrf_field()}}
           
            <div class="row">
                <div class="col-lg-12 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('send_message', __('common.label.send_message'),['class' => 'custom-label asterisk'])}}
                        {{Form::textarea('send_message', '',['class' => 'form-control','cols'=>2,'rows'=>2,'placeholder'=>'Message (maximum 120 characters)','maxlength'=>'120','required'=>'', 'style'=>"height: 80px;"])}}
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12">
                <div class="form-btn-group text-right">
                    {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
   
    @if(count($inquiryFollowUp) > 0)
    <div class="logo-detail">
        <div class="table-responsive">
            <table id="inquiry_table1" class="table table-custom table-bordered">
                <thead>
                    <tr>
                        <th width="25%">@lang('common.label.created_on')</th>
                        <th>@lang('common.label.note')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($inquiryFollowUp as $followUp)
                    <tr>
                        <td>{{  !empty($followUp)?date('j F Y, h:i A',strtotime($followUp->created_at)):'-' }}</td>
                        <td>{{  !empty($followUp)?$followUp->note:'-' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
</div>

@endsection
