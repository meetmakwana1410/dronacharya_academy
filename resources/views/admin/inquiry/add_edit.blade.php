@extends('layouts.app')
@section('title') 
@if (isset($inquiry) && !empty($inquiry))
    @lang('common.label.edit_inquiry')
@else
    @lang('common.label.add_inquiry')
@endif
@endsection
@section('content')
    <link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .select2-results__option[aria-selected]{font-size: 12px}
    </style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($inquiry) && !empty($inquiry))
                @lang('common.label.edit_inquiry')
            @else
                @lang('common.label.add_inquiry')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($inquiry) && !empty($inquiry))?URL::signedRoute('inquiry.edit',[$inquiry->id]): route('inquiry.add')]) !!}  
        @if (isset($inquiry) && !empty($inquiry))
            {{FORM::hidden('id',$inquiry->id)}}
        @endif 
        {{csrf_field()}}
       
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('name', (isset($inquiry) && !empty($inquiry))?$inquiry->name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'2','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('name'))
                        <div class="parsley-errors-list">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                    {{ Form::label('primary_mobile_number', __('common.label.primary_mobile_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('primary_mobile_number', (isset($inquiry) && !empty($inquiry))?$inquiry->primary_mobile_number:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-minlength'=>'10','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('primary_mobile_number'))
                        <div class="parsley-errors-list">{{ $errors->first('primary_mobile_number') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                    {{ Form::label('secondary_mobile_number', __('common.label.secondary_mobile_number'),['class' => 'custom-label'])}}
                    {{Form::text('secondary_mobile_number', (isset($inquiry) && !empty($inquiry))?$inquiry->secondary_mobile_number:'',['class' => 'form-control custom-input-style2 parsley-validated','data-parsley-minlength'=>'10'])}}
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="form-group">
                    {{ Form::label('student_mobile_number', __('common.label.student_mobile_number'),['class' => 'custom-label'])}}
                    {{Form::text('student_mobile_number', (isset($inquiry) && !empty($inquiry))?$inquiry->student_mobile_number:'',['class' => 'form-control custom-input-style2 parsley-validated','data-parsley-minlength'=>'10'])}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('parents_email', __('common.label.parents_email'),['class' => 'custom-label'])}}
                    {{Form::email('parents_email', (isset($inquiry) && !empty($inquiry))?$inquiry->parents_email:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('students_email', __('common.label.students_email'),['class' => 'custom-label'])}}
                    {{Form::email('students_email', (isset($inquiry) && !empty($inquiry))?$inquiry->students_email:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('residence_address', __('common.label.residence_address'),['class' => 'custom-label'])}}
                    {{Form::text('residence_address', (isset($inquiry) && !empty($inquiry))?$inquiry->residence_address:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('school', __('common.label.school'),['class' => 'custom-label'])}}
                    <select name="school_id" id="school_id" class="form-control select2"  data-parsley-trigger="change focusout">
                    <option value="">Select School</option>
                        @if(isset($school))
                            @foreach($school as $scl)
                                <option value="{{$scl->id}}" {{(isset($inquiry->school_id)) && $inquiry->school_id == $scl->id ? 'selected':'' }}>{{$scl->school_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('school_timings', __('common.label.school_timings'),['class' => 'custom-label'])}}
                    {{Form::text('school_timings', (isset($inquiry) && !empty($inquiry))?$inquiry->school_timings:'',['class' => 'form-control custom-input-style2 parsley-validated'])}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('standard', __('common.label.standard'),['class' => 'custom-label asterisk'])}}
                    <select name="standard_id" id="standard_id" class="form-control select2"  data-parsley-trigger="change focusout">
                    <option value="">Select standard</option>
                        @if(isset($standards))
                            @foreach($standards as $std)
                                <option value="{{$std->id}}" {{(isset($inquiry->standard_id)) && $inquiry->standard_id == $std->id ? 'selected':'' }}>{{$std->standard_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.school_type'),['class' => 'custom-label asterisk'])}}
                    <div class="radio-answer">
                        <input type="radio" id="school_type_gseb" class="ans" name="school_type" value="1"required data-parsley-required-message="Please select at least one school type." checked="checked">
                        <label for="school_type_gseb">
                            <span class="opt-character">GSEB</span>
                        </label>
                       
                        <input type="radio" id="school_type_cbse" class="ans" name="school_type" value="2" required data-parsley-required-message="Please select at least one school type.">
                        <label for="school_type_cbse">
                            <span class="opt-character">CBSE</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('remark', __('common.label.remark'),['class' => 'custom-label'])}}
                    {{Form::textarea('remark', (isset($inquiry) && !empty($inquiry))?$inquiry->remark:'',['class' => 'form-control','cols'=>2,'rows'=>2,'placeholder'=>__('common.label.remark'),'style'=>"height: 80px;"])}}
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                {{link_to_route('inquiry.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <div class="modal fade in" id="image-crop-modal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Crop Image</strong></h4>
                </div>
                <div class="modal-body">
                    <canvas id="image-crop-canvas" width="100%"></canvas>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom btn-theme font-size-sm crop-image">Crop</button>
                    <button type="button" class="btn btn-custom btn-secondary-2 font-size-sm close-popup" >Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'inquiry/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection