@extends('layouts.app')
@section('title') 
@lang('common.label.notification')
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />
<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 14px}
    input.select2-search__field {font-size: 16px;}
</style>

    <div class="custom-card">
        <h3 class="title">
            @lang('common.label.notification')
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => route('notifications.add')]) !!}  
        
        {{csrf_field()}}
        <div class="row">
            <div class="col-lg-12 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.batch_code'),['class' => 'custom-label'])}}
                    <select name="batch_id" id="batch_id" class="form-control select2" data-parsley-trigger="change focusout">
                    <option value="">Select Batch Code</option>
                        @if(isset($batch))
                            @foreach($batch as $btc)
                                <option value="{{$btc->id}}">{{$btc->batch_name}} ({{$btc->branch_name}})</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>  
                    
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.students'),['class' => 'custom-label'])}}
                    <input type="checkbox" id="checkbox" >
                    <label for="checkbox">Select All Students</label>
                    <select name="student_id[]" id="student_id" class="form-control select2" multiple>
                        <option value="">Select Student</option>
                    </select>
                </div>
            </div>  
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.teacher'),['class' => 'custom-label'])}}
                    <select name="teacher_id[]" id="teacher_id" class="form-control select2"  data-parsley-trigger="change focusout" multiple>
                    <option value="">Select teacher</option>
                        @if(isset($teacher))
                            @foreach($teacher as $btc)
                                <option value="{{$btc->id}}" {{(isset($exam->teacher_id)) && $exam->teacher_id == $btc->id ? 'selected':'' }}>{{$btc->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>        
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    <input type="checkbox" id="push_message_type" class="fancy-checkbox" name="message_type[]" value="1" required data-parsley-required-message="{{__('common.message.select_one_operation')}}">
                    <label for="push_message_type"> Push Notification</label>

                    <input type="checkbox" required id="sms_message_type" class="fancy-checkbox" name="message_type[]" value="2" required data-parsley-required-message="{{__('common.message.select_one_operation')}}">
                    <label for="sms_message_type"> SMS Notification</label><br>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{Form::textarea('message', '',['class' => 'form-control','rows'=>3,'placeholder'=>'Message (maximum 250 characters)','maxlength'=>'250','required'=>''])}}
                </div>
            </div>
        </div>

        <div class="row">
            
            <input type="hidden" id="url" name="url" value="{{asset('/')}}">
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'notifications/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection