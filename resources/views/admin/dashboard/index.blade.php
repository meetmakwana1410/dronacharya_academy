@extends('layouts.app')

@section('content')
    <div class="custom-card">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="title">Dashboard</h3>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <ul class="dashboardbox">
                    <li>
                        <div class="dashboardboxcontent">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <h2 class="card-title">Total Teacher</h2>
                            <p class="card-text">{{$getAllUsersCount}}</p>
                        </div><!--/.dashboardboxcontent-->
                    </li>
                    <li>
                        <div class="dashboardboxcontent">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <h2 class="card-title">Confirm/Total Inquiry</h2>
                            <p class="card-text">{{$getConfirmInquiry}} / {{$getAllInquiry}}</p>                       	
                        </div><!--/.dashboardboxcontent-->
                    </li>
                    
                    <li>
                        <div class="dashboardboxcontent">
                            <i class="fa fa-building-o" aria-hidden="true"></i>
                            <h2 class="card-title">Total Batch</h2>
                            <p class="card-text">{{$getAllBatch}}</p>
                        </div><!--/.dashboardboxcontent-->
                    </li>
                                    
                </ul><!--/.dashboardbox-->
                <ul class="dashboardbox">
                    <li>
                        <div class="dashboardboxcontent">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <h2 class="card-title">Total Students</h2>
                            <p class="card-text">{{$getAllStudents}}</p>
                        </div><!--/.dashboardboxcontent-->
                    </li> 
                </ul>
            </div><!--/.col-lg-4-->
    </div><!--/.row-->
</div><!--/.custom-card-->
@endsection
