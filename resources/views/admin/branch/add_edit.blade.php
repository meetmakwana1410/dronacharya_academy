@extends('layouts.app')
@section('title') 
@if (isset($branch) && !empty($branch))
    @lang('common.label.edit_branch')
@else
    @lang('common.label.add_branch')
@endif
@endsection
@section('content')
<link href="{{asset(CSS_FOLDER_URL.'select2.min.css')}}" rel="stylesheet" />

<style type="text/css">
    .select2-results__option[aria-selected]{font-size: 12px}
</style>
    <div class="custom-card">
        <h3 class="title">
            @if (isset($branch) && !empty($branch))
                @lang('common.label.edit_branch')
            @else
                @lang('common.label.add_branch')
            @endif
        </h3>

        @include('admin.common.flash_message')
        {!! Form::open(['method'=>'post','id'=>'formAddUpdate','files'=>true,'data-parsley-validate'=>'','novalidate'=>'','url' => (isset($branch) && !empty($branch))?URL::signedRoute('branch.edit',[$branch->id]): route('branch.add')]) !!}  
        @if (isset($branch) && !empty($branch))
            {{FORM::hidden('id',$branch->id)}}
        @endif 
        {{csrf_field()}}
        
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                     {{ Form::label('name', __('common.label.name'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('name', (isset($branch) && !empty($branch))?$branch->name:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('name'))
                        <div class="parsley-errors-list">{{ $errors->first('name') }}</div>
                    @endif
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.email'),['class' => 'custom-label asterisk'])}}
                    {{Form::email('email', (isset($branch) && !empty($branch))?$branch->email:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required')])}}
                    @if ($errors->has('email'))
                        <div class="parsley-errors-list">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                    {{ Form::label('name', __('common.label.mobile_number'),['class' => 'custom-label asterisk'])}}
                    {{Form::text('contact_no', (isset($branch) && !empty($branch))?$branch->contact_no:'',['class' => 'form-control custom-input-style2 parsley-validated','required'=>'','data-parsley-required-message'=>__('common.message.field_required'),'onkeypress'=>'return isNumberKey(event);'])}}
                    @if ($errors->has('contact_no'))
                        <div class="parsley-errors-list">{{ $errors->first('contact_no') }}</div>
                    @endif
                </div>
            </div>
        </div>

        @if(!$branch)
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('password', __('common.label.password'),['class' =>(isset($branch) && !empty($branch))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password',  ['id'=>'password','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($branch) && !empty($branch))?false:true,'data-type'=>'passwordcheck','data-parsley-required-message'=>__('common.message.password_required_messages')])}}

                        @if ($errors->has('password'))
                            <div class="parsley-errors-list">{{ $errors->first('password') }}</div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('confirm_password', __('common.label.confirm_password'),['class' =>(isset($branch) && !empty($branch))?'custom-label': 'custom-label asterisk'])}}
                        {{ Form::password('password_confirmation',  ['id'=>'password_confirmation','class' => 'form-control custom-input-style2 parsley-validated','required'=>(isset($branch) && !empty($branch))?false:true,'data-parsley-equalto'=>'#password','data-parsley-required-message'=>__('common.message.confirm_password_required_messages')])}}
                        @if ($errors->has('password_confirmation'))
                            <div class="parsley-errors-list">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                </div>  
            </div>
        @endif


        <div class="col-lg-12 col-sm-12">
            <div class="form-btn-group text-right">
                {{Form::submit( __('common.label.save') ,['class'=>'btn btn-custom btn-theme font-size-sm','id'=>'btnUpdate','title'=> __('common.label.save')])}}
                {{link_to_route('branch.list', __('common.label.cancel'), [], ['class'=>'btn btn-custom font-size-sm btn-secondary-2','title'=> __('common.label.cancel')])}}
            </div>
        </div>

        
        {!! Form::close() !!}
    </div>
@endsection

@section('page_script')
<script type="text/javascript" src="{{asset(JS_ADMIN_MODULE_FOLDER_URL.'branch/add_edit.js')}}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".select2").select2();
</script>
@endsection