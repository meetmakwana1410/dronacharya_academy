<aside class="main-navigation">
    <a href="{{ route('category.list') }}" class="logo">
        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/logo_pinkwhite.svg') }}" width="170" alt="logo" class="biglogo" />
        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/logomini.svg') }}" width="50" alt="logo-mini" class="minilogo" />
    </a>

    <ul id="main_navigation" class="main-nav">
    	{{-- <li><a href="" title="@lang('common.label.dashboard')"><i class="theme-icon icon-client"></i><span>@lang('common.label.dashboard')</span></a></li> --}}
        <li><a href="{{route('category.list')}}"  title="@lang('common.label.categories')" class="{{ ((in_array(Route::currentRouteName(),['category.list','category.add','category.edit'])) ? 'active' : '') }}"><i class="theme-icon icon-survey"></i><span>@lang('common.label.categories')</span></a></li>

        <li><a href="{{route('post.list')}}"  title="@lang('common.label.categories')" class="{{ ((in_array(Route::currentRouteName(),['post.list','post.add','post.edit'])) ? 'active' : '') }}"><i class="theme-icon icon-survey"></i><span>@lang('common.label.post')</span></a></li>
      </ul>
</aside>