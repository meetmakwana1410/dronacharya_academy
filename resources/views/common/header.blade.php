<head>
    <div class="head-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-3">
                    <a id="toggle_nav" href="javascript:void(0)" class="toggle-nav">
                        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/03_Menu_close.svg') }}" width="30" alt="Menu" class="close-nav" />
                        <img src="{{ asset(IMAGES_FOLDER_URL.'icons/svg/04_Menu_Expand.svg') }}" width="30" alt="Menu" class="expand-nav" />
                    </a>
                </div>

                <div class="col-lg-6 col-9">
                    <ul class="profile-nav">
                        <li class="dropdown">
                            <a href="JavaScript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="name">{{Auth::user()->name}}</span>
                                <!-- <img src="{{ asset(IMAGES_FOLDER_URL.'user.png') }}" alt="user"> -->
                            </a>
                            <ul class="dropdown-menu">
                                <!-- <li>
                                    <a href="javascript:void(0)" class="dropdown-item">My Profile</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="dropdown-item">Change Password</a>
                                </li> -->
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</head>