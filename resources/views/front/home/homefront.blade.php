@extends('front.layouts.app')
@section('content')
   <div class="bg-img1 size1 flex-w flex-c-m p-t-55 p-b-55 p-l-15 p-r-15" style="background-image: url('{{asset(IMAGES_FOLDER_URL.'login-banner.jpg')}}');">
    <div class="wsize1 bor1 bg1 p-t-175 p-b-45 p-l-15 p-r-15 respon1">
      <button class="flex-c-m s1-txt3 size3 how-btn trans-04 where1">
          <a href="{{route('login')}}">Admin</a>
      </button>
      <div class="wrappic1">
        <img src="{{asset(IMAGES_FOLDER_URL.'logo.svg')}}" style="width: 15%"  alt="LOGO">
      </div>

      <p class="txt-center m1-txt1 p-t-33 p-b-68">
        Our website is under construction
      </p>

      <div class="wsize2 flex-w flex-c hsize1 cd100">
        <div class="flex-col-c-m size2 how-countdown">
          <span class="l1-txt1 p-b-9 days">35</span>
          <span class="s1-txt1">Days</span>
        </div>

        <div class="flex-col-c-m size2 how-countdown">
          <span class="l1-txt1 p-b-9 hours">17</span>
          <span class="s1-txt1">Hours</span>
        </div>

        <div class="flex-col-c-m size2 how-countdown">
          <span class="l1-txt1 p-b-9 minutes">50</span>
          <span class="s1-txt1">Minutes</span>
        </div>

        <div class="flex-col-c-m size2 how-countdown">
          <span class="l1-txt1 p-b-9 seconds">39</span>
          <span class="s1-txt1">Seconds</span>
        </div>
      </div>

      <form class="flex-w flex-c-m contact100-form validate-form p-t-55">
        <div class="wrap-input100 validate-input where1" data-validate = "Email is required: ex@abc.xyz">
          <input class="s1-txt2 placeholder0 input100" type="text" name="email" placeholder="Your Email">
          <span class="focus-input100"></span>
        </div>

        
        <button class="flex-c-m s1-txt3 size3 how-btn trans-04 where1">
          Get Notified
        </button>
        
      </form>

    </div>
  </div>

@endsection

@section('page_script')
    
@endsection