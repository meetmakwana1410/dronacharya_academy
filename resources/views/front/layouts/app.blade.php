<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    
    @include('front.common.meta_content')

    <!-- Bootstrap CSS -->
    @yield('page_css')
    <!-- Stylesheet -->

    <link rel="icon" type="icon" href="{{ asset(IMAGES_FOLDER_URL.'favicon.ico') }}">
</head>

<body class="">
    <div id="wrapper" class="clearfix">
   
        @include('front.common.loaders.fullscreen')
        
        <!-- Header Start -->
        @include('front.common.header')
        <!-- Header End -->
        
        <div class="main-content">
            <div class="client-information">
                @yield('content')
            </div>
        </div>
        <!-- end main-content -->

        @yield('footer')

        @include('front.common.footer')
        <script type="text/javascript">
            var BASE_URL ='<?php echo url('/').'/'; ?>';
        </script>
        
    </div>
        @include ('front.common.js.general')
        @yield('page_script')
</body>
</html>