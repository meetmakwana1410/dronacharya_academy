<!-- Required meta tags -->
<meta charset="utf-8">

  <!-- Meta Tags -->
  <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="description" content="<?php echo !empty($description)?$description:env('APP_NAME')?>" />
  <meta name="keywords" content="<?php echo !empty($keyword)?$keyword:env('APP_NAME')?>" />
  <meta name="classification" content="<?php echo !empty($classification)?$classification:env('APP_NAME')?>" />
  <meta name="author" content="<?php echo env('APP_NAME');?>" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title><?php echo !empty($title)?$title:env('APP_NAME')?></title>
