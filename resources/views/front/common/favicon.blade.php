<!--Favicons-->
<!-- Favicon and Touch Icons -->
<link href="images/favicon.png" rel="shortcut icon" type="image/png">
<link href="images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<link rel="icon" type="image/png" sizes="16x16" href="{{ asset(IMAGES_FOLDER_URL.'favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset(IMAGES_FOLDER_URL.'favicon/manifest.json') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset(IMAGES_FOLDER_URL.'favicon/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">