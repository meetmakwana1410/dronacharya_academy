@if (\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ \Session::get('success') }}
</div>
@endif

@if (\Session::has('error'))
<div class="alert alert-danger alert-dismissible fade show">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
    {{ \Session::get('error') }}
</div>
@endif