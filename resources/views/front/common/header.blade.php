<head>
    <title><?php echo env('APP_NAME') ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset(FRONT_CSS_FOLDER_URL.'util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset(FRONT_CSS_FOLDER_URL.'main.css')}}">
</head>
<script type="text/javascript">
    var csrfToken = '{{ csrf_token() }}';
</script>