<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label' => [

        /**************COMMON LABEL**************/
        'reset'                     => 'Reset',
        'name'                      => 'Name',
        'email'                     => 'Email',
        'subject'                   => 'Subject',
        'phone'                     => 'Phone Number',
        'mobile_number'             => 'Mobile Number',
        'message'                   => 'Message',
        'edit'                      => 'Edit',
        'exam_students'                      => 'Exam Students',
        'save'                      => 'Save',
        'confirm'                   => 'Confirm',
        'save_new'                  => 'Save & Add New',
        'save_close'                => 'Save & Close',
        'save_notifiy'              => 'Save & Notifiy',
        'note'                      => 'Note',
        'remark'                    => 'Remark',
        'show'                      => 'View',
        'delete'                    => 'Delete',
        'cancel'                    => 'Cancel',
        'no_record_found'           => 'No Record Found',
        'records'                   => 'Records',
        'password'                  => 'Password',
        'confirm_password'          => 'Confirm Password',
        'created_on'                => 'Created On',
        'edit_information'          => 'Edit Information',
        'select_column'             => 'Select Column',
        'back_btn'                  => 'Back',
        'Logout'                    => 'LogOut',
        'clone'                     => 'Clone',
        'download'                  => 'Download',
        'dashboard'                 => 'Dashboard',
        'update'                    => 'Update',
        'next'                      => 'Next',
        'previous'                  => 'Previous',
        'more'                      => 'More',
        'year'                      => 'Year',
        'action'                    => 'Action',
        'date_of_birth'             => 'Date of birth',
        'gender'                    => 'Gender',
        'occupation'                => 'Occupation',
        'address'                   => 'Address',
        'landline_number'           => 'Landline Number',
        'status'                    => 'Status',
        'notification'              => 'Notification',
        'created_by'                => 'Create by',

        /************** Change Password **************/
        'change_password'           => 'Change Password',
        'curr_password'             => 'Old Password',
        'new_password'              => 'New Plassword',

        /************* ACADEMIC YEAR LABEL**************/
        'academic_year'             => 'Academic Year',
        'add_academic_year'         => 'Add Academic Year',
        'edit_academic_year'        => 'Edit Academic Year',
        

        /************* MEDIUM LABEL**************/
        'medium'                    => 'Medium',
        'add_medium'                => 'Add Medium',
        'edit_medium'               => 'Edit Medium',


        /************* SCHOOL LABEL**************/
        'school'                    => 'School',
        'add_school'                => 'Add School',
        'edit_school'               => 'Edit School',

        /************* EXAM TYPE LABEL**************/
        'exam_type'                    => 'Exam type',
        'add_exam_type'                => 'Add Exam type',
        'edit_exam_type'               => 'Edit Exam type',

        /************* BOARD LABEL**************/
        'board'                     => 'Board',
        'add_board'                 => 'Add Board',
        'edit_board'                => 'Edit Board',
        'board_name'                => 'Board Name',
        'board_full_name'           => 'Board Full Name',

        /************* SUBJECTS LABEL**************/
        'subjects'                 => 'Subjects',
        'add_subjects'             => 'Add Subjects',
        'edit_subjects'            => 'Edit Subjects',
        'subjects_name'            => 'Subjects Name',
        'subject_image'            => 'Subjects Image',
        'select_standard'          => 'Select Standard (Assign Multiple)',

        /************* BOARD LABEL**************/
        'standards'                 => 'Standards',
        'add_standards'             => 'Add Standards',
        'edit_standards'            => 'Edit Standards',
        'standards_name'            => 'Standards Name',


        /************* BATCH LABEL**************/
        'batch'                     => 'Batch',
        'add_batch'                 => 'Add Batch',
        'edit_batch'                => 'Edit Batch',
        'batch_name'                => 'Batch Name',
        'select_baord'              => 'Select Baord',
        'batch_select_standard'     => 'Select Standard',
        'batch_code'                => 'Batch Code',
        'batch_image'               => 'Batch Image',
        'timetable_image'           => 'Timetable Image',

        /************* STUDENT LABEL**************/
        'students'                  => 'Students',
        'add_student'               => 'Add Student',
        'edit_student'              => 'Edit Student',
        'student_name'              => 'Student Name',
        'select_baord'              => 'Select Baord',
        'student_select_standard'   => 'Select Standard',
        'student_code'              => 'Student Code',
        'roll_number'               => 'Roll No',
        'identify_details'          => 'Identify Details',
        'parents_details'           => 'Parents Details',
        'student_details'           => 'Student Details',
        'batch_details'             => 'Batch Details',
        'admission_date'            => 'Admission Date',
        'father_name'               => 'Father Name',
        'father_mobile_number'      => 'Father Mobile',
        'mother_mobile_number'      => 'Mother Mobile',
        'studnet_mobile_number'      => 'Studnet Mobile',

        /************* TEACHER LABEL**************/
        'teacher'                   => 'Teacher',
        'add_teacher'               => 'Add Teacher',
        'edit_teacher'              => 'Edit Teacher',

        /************* EXAM LABEL**************/
        'exam'                      => 'Exam',
        'add_exam'                  => 'Add Exam',
        'edit_exam'                 => 'Edit Exam',
        'exam_date'                 => 'Exam Date',
        'exam_time'                 => 'From Exam Time',
        'to_exam_date'              => 'To Exam Date',
        'to_exam_time'              => 'To Exam Time',
        'exam_duration'             => 'Exam Duration',
        'exam_category'             => 'Exam Category',
        'total_marks'               => 'Total Marks',
        'obtain_marks'              => 'Obtain Marks',
        'total_obtain_marks'        => 'Total / Obtain Marks',
        'percentage'                => 'Percentage',
        'chapter'                   => 'Chapter',
        'enter_mark'                => 'Enter Mark',
        'attendance_status'         => 'Attendance Status',
        'exam_paper_prepared_by'    => 'Exam paper prepared by',
        'update_notify'             => 'Update and notify',

        /************* DOCUMENTS LABEL**************/
        'documents'                   => 'Documents',
        'upload_document'             => 'Uploads Documents',
        'add_documents'               => 'Add Documents',
        'edit_documents'              => 'Edit Documents',
        'uploaded'                    => 'Uploaded',

        /************* HOMEWORK LABEL**************/
        'homework'                   => 'Homework',
        'add_homework'               => 'Add Homework',
        'edit_homework'              => 'Edit Homework',
        'submission_date'            => 'Submission Date',
        'homework_details'           => 'Homework Details',

        /************* BRANCH LABEL**************/
        'branch'                     => 'Branch',
        'add_branch'                 => 'Add Branch',
        'edit_branch'                => 'Edit Branch',
        'branch_name'                => 'Branch Name',

        /************* Inquiry LABEL**************/
        'inquiry'                   => 'Inquiry',
        'add_inquiry'               => 'Add Inquiry',
        'edit_inquiry'              => 'Edit Inquiry',
        'primary_mobile_number'     => 'Primary mobile number',
        'secondary_mobile_number'   => 'Secondary mobile number',
        'student_mobile_number'     => 'Student mobile number',
        'parents_email'             => 'Parents Email',
        'students_email'            => 'Students Email',
        'residence_address'         => 'Residence Address',
        'school_name'               => 'School Name',
        'school_timings'            => 'School Timing',
        'standard'                  => 'Standard',
        'school_type'               => 'School Type',
        'inquiry_details'           => 'Inquiry Details',
        'send_message'              => 'Send Message',
    ],

    'admin_label' => [
        'menu'                          => 'Menu',
    ],
    'message' => [
        'update_success_msg'                        => 'Record has been updated successfully.',
        'create_success_msg'                        => 'Record has been added successfully.',
        'deactivated_sucessfully'                   => 'Record has been deactivated successfully.',
        'activated_sucessfully'                     => 'Record has been activated successfully.',
        'field_required'                            => 'Please enter input field value.',
        'password_minlength_message'                => 'Password should have minimum 8 characters which contain 1 upper case, 1 lower case & 1 number',
        'password_required_messages'                => 'Please enter password.',
        'confirm_password_required_messages'        => 'Please enter confirm password.',
        'password_confirm_password_equal_to_message' => 'Password and confirm password should be same.',
        'something_went_wrong'                      => 'Something went wrong please try again.',
        'bad_request_error'                         => 'Bad Request Error',
        'enquiry_submit_successfully'               => 'Your enquiry has been submitted successfully.',
        'deleted_sucessfully'                       => 'Record has been deleted successfully.',
        'password_pattern_message'=>'Password should have minimum 8 characters which contain 1 upper case, 1 lower case & 1 number',
        'mark_update_success'                       => 'Mark update successfully',
        'obtained_small_total_marks'                => 'Obtained marks must be smaller than total marks',
        'teacher_not_added'                         => 'Please enter Batch,Subjects,Branch before add Teachher',
        'enter_batch'                               => 'Please enter Batch',
        'select_one_operation'                      => 'Please select at least one operation',
        'notification_sent'                         => 'Notification has been sent',
        'follow_up_added_successfully'              => 'Follow up has been added successfully',
    ],
    'api_response_message' => [
        'login_successfully'                        => 'You have successfully login',
        'incorrect_mobile_password'                 => 'You have entered incorrect mobile number or password',
        'incorrect_mobile'                          => 'You have entered incorrect mobile number',
        'incorrect_password'                        => 'You have entered incorrect password',
        'success_msg'                               => 'Success',
        'profile_updated'                           => 'Your profile has been updated successfully',
        'no_record_found'                           => 'No Record Found',
        'password_changed_successfully'             => 'Password changed successfully',
        'old_password_not_match'                    => 'Old password does not match',
        'teacher_not_exist'                         => 'Teacher does not exist',
        'student_not_exist'                         => 'Student does not exist',
        'homework_updated_success'                  => 'Homework updated successfully',
        'image_deleted_success'                     => 'Image deleted successfully',
        'homework_delete_success'                  => 'Homework deleted success',
        'successfully_logged_out'                  => 'You have successfully logged out',
    ]
];