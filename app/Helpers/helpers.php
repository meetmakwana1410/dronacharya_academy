<?php 
use app\Models\posts;

    /**
        * Get file extension
        *
        * @return string
    */
    function getFileExtension($file)
    {
        if(!empty($file))
        {
            $fileNames  =   explode('/', $file);

            if(count($fileNames) > 0)
            {
                $fileext    =   explode('.', $fileNames[count($fileNames) - 1]);    
                if(count($fileext) > 0)
                {
                    return $fileext[count($fileext) - 1];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /**
        * Get file name
        *
        * @return string
    */
    function getfileName($file)
    {
        if(!empty($file))
        {
            $fileNames  =   explode('/', $file);
            if(count($fileNames) > 0)
            {
                return $fileNames[count($fileNames) - 1];    
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

  
    function check_date_format($date = '',$dateFormat='')
    {
        $response = '';
        if(!empty($date) && $date != '0000-00-00' && $date != '00-00-0000' && $date != '1970-01-01')
        {
        $response = date(($dateFormat?$dateFormat:'d-m-Y'),strtotime($date));
        }
        return $response;
    }

    /**
        * Craate Slug
        *
        * @return string
    */
    function slugify($text)
    {

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    function batch_code($req){
        return $batchCode = $req[0]->year.'_'.$req[0]->board_name.'_'.$req[0]->medium_name.'_'.$req[0]->standard_name.'_'.$req[0]->batch_name;
    }

    function check_file_exists($imgName = '',$path)
    {
      $response = '';
      if(!empty($imgName) && file_exists(public_path($path.$imgName)))
      {
        $response = url('/').$path.$imgName;
      }
      return $response;
    }

    function saveLog($method,$request,$message)
    {
        Log::info($message.' | '.$method, array('body' => $request));
    }

    function calculateAge($dateOfBirth)
    {
        $today  = date("Y-m-d");
        $diff   = date_diff(date_create($dateOfBirth), date_create($today));
        return $diff->format('%y');
    }

    /**
     * Convert null string into empty string ( null => '')
     * @param data in object or array
     * @return object or array with empty value 
     * @author Meet Makwana
     */
    function nullReplaceWithEmpty($data='')
    {
        $isObejct = 0;
        if(is_object($data))
        {
          $data     = $data->toArray();
          $isObejct = 1;
        }
        
        foreach ($data as $key => $value) {
            if(is_array($value))
            {
              foreach ($value as $innerKey => $innerValue) {
                  if (is_null($innerValue)) {
                       $data[$key][$innerKey] = "";
                  }
              }
            }else{
              $myArray = json_decode(json_encode($value), true);
              if(!empty($myArray) && is_array($myArray)){
                foreach ($myArray as $objInnerKey => $objInnerValue) {
                  //echo $objInnerKey.'='.$objInnerValue."\n";
                  if (is_null($objInnerValue)) {
                    $data[$key]->$objInnerKey = "";
                  }
                }
              }
            }

            if (is_null($value)) {
              $data[$key] = "";
            }
        }
        // print_r($data);die;
        if($isObejct == 1)
        {
          $data = (object)$data;
        }
       
        return $data;
    }

    function date_difference_instagram($tCreatedDate)
    {   
        //date_default_timezone_set("Asia/Kolkata");
        $date = date('Y-m-d H:i:s');
        
        $delta = strtotime($date) - strtotime($tCreatedDate);
        // return $delta;
        if ($delta < 1 * MINUTE)
        {
            return $delta == 1 ? "1s" : $delta . "s";
        }
        if ($delta < 2 * MINUTE)
        {
          return "1m";
        }
        if ($delta < 45 * MINUTE)
        {
            return floor($delta / MINUTE) . "m";
        }
        if ($delta < 90 * MINUTE)
        {
          return "1h";
        }
        if ($delta < 24 * HOUR)
        {
          return floor($delta / HOUR) . "h";
        }
        if ($delta < 30 * DAY)
        {
            if($delta <= 7){
                return floor($delta / DAY) . "d";
            }else{
                $week = floor($delta / WEEK);
                return $week <= 1 ? "1w" : $week . "w";
            }
        }
        //echo $delta.' | '.WEEK.' | '.(7 * WEEK)."\n";
        if($delta >= 7 * DAY){
            $week = floor($delta / WEEK);
            return $week <= 1 ? "1w" : $week . "w";
        }
        
        return date('F d',strtotime($tCreatedDate));
    }

    function uploadMultipleImages($request,$userDocumnetPath){
            
        //$cnt  =  1;
        //if(is_array($request->file('image'))){
            $cnt  = count($request->file('image'));
        //}
        //die($cnt);
        if($request->hasFile('image') && !empty($request->file('image'))){
          
            for($j=0;$j<$cnt;$j++){
                $docs       =   $request->file('image');
                $docName    =   [];
                $image      =   $request->file('image')[$j];
                $passportDocPath    =   public_path($userDocumnetPath);
                File::isDirectory($passportDocPath) or File::makeDirectory($passportDocPath, 0777, true, true);
                $fileOrgname        =  $image->getClientOriginalName();
                $fileOrgExt         =  $image->getClientOriginalExtension();
                $filename       =  str_replace([' ','-'], '_', rand(0,10000).'_'.time().'_'.$fileOrgname);
                $image->move($passportDocPath, $filename);
                $filenamedata[$j]['name']    =  $filename;
            }
            return $filenamedata;
        }
    }

    function uploadImage($request,$userDocumnetPath,$docName='image'){
        $filename = '';
        if(!empty($docName)){
            $image          =   $request->file($docName);
        }else{
            $image          =   $request->file('image');
        }
        
        
        if($image){
            $docStorepath       =   public_path($userDocumnetPath);
            File::isDirectory($docStorepath) or File::makeDirectory($docStorepath, 0777, true, true);
            $fileOrgname        =  $image->getClientOriginalName();
            $fileOrgExt         =  $image->getClientOriginalExtension();
            $filename           =  str_replace([' ','-'], '_', rand(0,10000).'_'.time().'_'.$fileOrgname);
            $image->move($docStorepath, $filename);

            return $filename;
        }
    }

    function send_notification_iphone($deviceToken,$message,$json_data,$type='')
    {
        /*echo "Device Token : ".$deviceToken.'<br>';
        echo "Message : ".$message;
        pr($json_data);
        die;*/
        
        $pemFile = public_path('uploads/').'apns-dev.pem';
        // echo phpinfo(); 
        if(!empty($deviceToken) && $deviceToken != '(null)' && is_readable($pemFile))
        {
            // Construct the notification payload
            $body = array();
            $body['aps']                = array('alert' => $message);
            $body['aps']['alert']       = $message;
            $body['aps']['sound']        = 'default';
            $body['aps']['data']         = $json_data;
    
            /* End of Configurable Items */
            $gateway = 'ssl://gateway.sandbox.push.apple.com:2195';

            $ctx = stream_context_create();
            // Define the certificate to use 

            //echo $CI->config->item('pem_path').'apns-dev.pem';die;
            
            stream_context_set_option($ctx, 'ssl', 'local_cert', $CI->config->item('pem_path').'apns-dev.pem');
            $fp = stream_socket_client($gateway, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
            //prd($fp);
            if (!$fp) {
                $data['msgs'] = "Failed to connect $err $errstr \n";
            } else {
                $payload = json_encode($body);
                $msg = chr(0) . pack("n", 32) . pack("H*", str_replace(" ", "", $deviceToken)) . pack("n", strlen($payload)) . $payload;
                $result = fwrite($fp, $msg);
                if (!$result)
                    $data['msgs'] = 'Message not delivered'; //. PHP_EOL;
                else
                    $data['msgs'] = 'Success'; //. PHP_EOL;
                fclose($fp);
            }
            //return $data;
            
        }    
    }

    function send_notification_android($deviceToken,$message,$json_data,$type='',$color='#93A83B')
    {
        //echo "deviceToken : ".$deviceToken.'<br>';
        //echo "message : ".$message;
        
        $url            = 'https://fcm.googleapis.com/fcm/send';
        //$server_key     = 'AIzaSyBAC82PblbrTUWdfIFP6miCgoavkEQs_EA'; 
        $server_key     = 'AAAAlUqiqh4:APA91bGLxZ9p_W4K_VNXZUummA7a4bLUBhgFwmvuCLohip--Jy9VOr7wKKIroGNLyQ15uXzLFV91k0SRuHwlny754RrZkpowDBFr5gy4wfITaeDM98y3h8WYFJzYPN19PDCb8sWfWmSX'; 
        $headers        = array('Content-Type:application/json', 'Authorization:key='.$server_key);
        $notification   = array(
                                'title'         => 'Dronacharya Academy '.$type,
                                'text'          => $message,
                                'icon'          => '@mipmap/ic_launcher_trns',
                                'sound'         => 'notification',
                                'color'         => $color,
                            );
        $fields = array('data'          => $json_data,
                       // 'notification'  => $notification,
                        'to'            => $deviceToken);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        //dd($result);  
        return $result;
    }

    function multi_unique($src){
        $output = array_map("unserialize",
        array_unique(array_map("serialize", $src)));
        return $output;
    }

    function sendSms($mobie='',$msg='')
    {
        $fields = [
                    'mobile'    => '9227403045',
                    'pass'      => '9227403045',
                    'senderid'  => 'DRONAC',
                    'to'        => $mobie,
                    'msg'       => $msg,
                ];
        $curl = curl_init();
        //$result = '';
        $url = "http://trans.jaldisms.com/smsstatuswithid.aspx?mobile=9227403045&pass=9227403045&senderid=DRONAC&to=".$mobie."&msg=".urlencode($msg);

        $result = curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
        ));

        
        $result = curl_exec($curl);
        if ($result === FALSE) {
            die('Send SMS Error: ' . curl_error($curl));
        }
        curl_close($curl);
        //dd($result);  
        return $result;
    }
