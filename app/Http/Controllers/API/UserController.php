<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use App\User;
use app\Models\Students;
use app\Models\Subjects;
use app\Models\Documents;
use App\RoleUser;
use App\Mail\ForgetPassword;
use app\Models\StudentAttendance;
use app\Models\Holidays;
use Validator;
use Hash;
use DB;
use Mail;

class UserController extends Controller
{
    /**
     * User Login
     * @param mobile_number,password,device_type,device_token
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function login(Request $request)
    {
    	try 
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'mobile_number' => 'required|max:10',
                'password' 		=> 'required',
                //'device_type' 	=> 'required|in:1,2',
                //'device_token' 	=> 'required'
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');
	    	
            // Check Login by email
	    	$checkLogin = Students::where('father_mobile_number',$input['mobile_number'])->where('status',1)->first();
            //dd($checkLogin);
	    	
            if(!empty($checkLogin))
	    	{ 
                /*echo $input['password'];
                var_dump(expression) Hash::check($checkLogin->password, $input['password']);
                dd($checkLogin->password);*/
	    		// Check login password
	    		if (Hash::check($input['password'], $checkLogin->password))
				{
					$data['id'] = $checkLogin->id;

                    if(!empty($input['device_token'])){
					   Students::where('id', $checkLogin->id)->update(['device_type' => $input['device_type'],'device_token' => $input['device_token']]);
                    }

		    		// Success Response
			    	$response 	=	[
			    		'status' 	=>	1,
			    		'message' 	=>	trans('common.api_response_message.login_successfully'),
			    		'data'		=>	nullReplaceWithEmpty($data)
			    	];
			    	return response()->json($response);
		    	}
	            // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	trans('common.api_response_message.incorrect_password'),
		    	];
		    	return response()->json($response);
	    	}
	    	else
	    	{
				// Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	trans('common.api_response_message.incorrect_mobile'),
		    	];
		    	return response()->json($response);
	    	}
    	} 
    	catch (Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }

    /**
     * Change Password
     * @param user_id,old_password,new_password
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function studentLogout(Request $request)
    {
        try 
        {
            $input      = $request->all();
            $id         = $request->user_id;

            //token key is null.
            $validator = Validator::make($input, [
                'user_id'          => 'required',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            // Check token key match or not
            $userData = Students::find($id);
            if(!empty($userData))
            {
                if($id != '')
                {
                    $userData->device_type              = '';
                    $userData->device_token             = '';
                    $userData->save();

                    // Success Response
                    $response   =   [
                        'status'    =>  1,
                        'message'   =>  trans('common.api_response_message.successfully_logged_out')
                    ];
                    return response()->json($response);
                        
                }
            }
            else
            {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  trans('common.api_response_message.no_record_found'),
                ]);
            }   
            
        } catch (Exception $e) {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }
    
    /**
     * Change Password
     * @param user_id,old_password,new_password
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function changePassword(Request $request)
    {
    	try 
    	{
    		$input 		= $request->all();
    		$id 		= $request->user_id;

        	//token key is null.
	       	$validator = Validator::make($input, [
                'user_id'  		   => 'required',
                //'old_password' 	=> 'required|string|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                'old_password'      => 'required|string|min:6',
                'new_password' 	    => 'required|string|min:6',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
            // Check token key match or not
            $userData = Students::find($id);
            if(!empty($userData))
            {
            	if($id != '')
	    		{
	    			$oldPassword = $request->old_password;
	    			$newPassword = $request->new_password;
					if(!empty($userData) && $oldPassword != '')
		    		{
		    			if (Hash::check($oldPassword, $userData->password))
		    			{
		    				// Update password
		    				$userData->password = Hash::make($newPassword);
		    				$userData->save();

		    				// Success Response
				    		$response 	=	[
					    		'status' 	=>	1,
					    		'message' 	=>	trans('common.api_response_message.password_changed_successfully')
					    	];
					    	return response()->json($response);
		    			}
		    			else
		    			{
		    				// Error Response
				    		$response 	=	[
					    		'status' 	=>	0,
					    		'message' 	=>	trans('common.api_response_message.old_password_not_match')
					    	];
					    	return response()->json($response);
		    			}
		    		}
		    		else
		    		{
		    			// Error Response
				    	return response()->json([
				    		'status' 	=>	0,
				    		'message' 	=>	trans('common.api_response_message.no_record_found')
				    	]);
		    		}
	    		}
            }
            else
            {
            	return response()->json([
					'status' 	=>	0,
					'message' 	=> 	trans('common.api_response_message.no_record_found'),
				]);
            }	
            
    	} catch (Exception $e) {
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }

    /**
     * Forget Password
     * @param email
     * @return json response - status, message
     * @author Meet Makwana
     */
    public function forgetPassword(Request $request)
    {
    	$input 		= $request->all();
    	$validator 	= Validator::make($input, [
            'email'	=> 'required|email',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
    		$response 	=	[
	    		'status' =>	0,
	    		'message'=>	$validator->errors()
	    	];
	    	return response()->json($response);
        }
        $userData = User::where('email', $input['email'])
        			->where('status',1)
	                ->first();
	    if(!empty($userData))    
	    {
	    	$resetKey = base64_encode(json_encode(['id'=>$userData->id,'time'=>time()]));
	    	$login = User::find($userData->id);
     		$login->remember_token = $resetKey;
      		$login->save();
      		Mail::to($input['email'])->send(new ForgetPassword($login));
	    	$response = [
	    		'status'  => 1,
	    		'message' => 'Reset password link is sent to your email. Please check.'
	    	];
	        return response()->json($response);
	    }   
	    else
	    {
	    	// Error Response
	    	return response()->json([
	    		'status' =>	0,
	    		'message'=>	'Email is not registerd yet'
	    	]);
	    }    
    }

    /**
     * Reset Password
     * @param 
     * @return json response - status, message
     * @author Meet Makwana
     */
    public function resetPassword(Request $request, $id)
    {
        $login  = User::where('remember_token', $id)->first();
        if(!empty($login))
        {
            return view('user.resetpassword', compact('id'));
        }
        else
        {
            $message = 'Sorry reset password link is expired!.';
            $success = false;
            return view('user.resetfailure', compact('message','success')); 
        }
        
    }

    /**
     * Update User Password
     * @param 
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function updatePassword(Request $request)
    {
        $input  = $request->all();
        
        $login  = User::where('remember_token', $input['reset_key'])->first();
        if(!empty($login))
        {
            $login->password  = Hash::make($input['password']);
            $login->remember_token = '';
            $login->save();
            $message = 'Password Updated successfully';
            $success = true;
            return view('user.resetfailure', compact('message','success'));   
        }
        else
        {
            $message = 'Sorry reset password link is expired!.';
            $success = false;
            return view('user.resetfailure', compact('message','success'));   
        }
    }

    /**
     * User Profile
     * @param user_id
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function userProfile(Request $request)
    {
        $input      = $request->all();
        $validator  = Validator::make($input, [
            'user_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        //$user = Students::find($input['user_id']);

        $user = Students::getAllStudents('', ['id'=>$input['user_id']]);
        if(!empty($user))
        {
            unset($user[0]->password);

            //DB::enableQueryLog();
            $subjects = Subjects::getStudentsSubjects(['student_id'=>$input['user_id']]);
            //dd(DB::getQueryLog());

            if(!empty($subjects)){
                $user[0]->student_subjects = $subjects;
            }else{
                $user[0]->student_subjects = [];
            }
            
            if(!empty($user[0]->profile_image)){
                $user[0]->profile_image = check_file_exists($user[0]->profile_image,$this->studnetImagePath);
            }

            $response =   [
                'status'    => 1,
                'message'   => trans('common.api_response_message.success_msg'),
                'data'      => nullReplaceWithEmpty($user[0])
            ];
            return response()->json($response);
        }
        else
        {
            return response()->json([
                'status' => 0,
                'message'=> 'User Not Found.'
            ]);
        }
    }

    /**
     * Remove ProfileImage (Delete)
     * @param id
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function removeProfileImage(Request $request)
    {
        $input      = $request->all();

        $validator  = Validator::make($input, [
            'image_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $user = UserImages::find($input['image_id']);
        if(!empty($user))
        {
            \File::delete(public_path($this->userImagePath) . '/' . $user->image); 
            UserImages::where('id',$input['image_id'])->delete();

            ///// IF REMOVE PRIMARY IMAGE THEN SET NEXT IMAGE AS A PRIMARY /////
            if($user->is_primary == 1){
                $images = UserImages::where('user_id',$user->user_id)->first();
                $images->is_primary = 1;
                $images->save();
            }

            return response()->json([
                'status' => 0,
                'message'=> 'User Image Deleted.'
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> 'User Image Not Found.'
            ]);
        }
    }

     /**
     * Edit Profile
     * @param name,email,password,date_of_birth,gender,preference_geneder,about_us,testebuds
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function editProfile(Request $request)
    {
        try
        {
            $input = $request->all();
            $validator  = Validator::make($input, [
                'user_id'               =>  'required',
                'student_name'          =>  'required',
                'student_email'         =>  'unique:students,student_email,'.$input['user_id'].',id',
                'student_mobile_number' =>  'max:10|unique:students,student_mobile_number,'.$input['user_id'].',id',
                'student_date_of_birth' =>  'required|date_format:d-m-Y',
                'gender'                =>  'required|in:1,2',
            ]);
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status' => 0,
                    'message'=> $validator->errors()
                ];
                return response()->json($response);
            }

            saveLog(__FUNCTION__,$input,'Request body API');
            DB::beginTransaction();
            $students = Students::findOrNew($input['user_id']);

            if(!empty($students)){

                $students->student_name             = $input['student_name'];
                $students->student_mobile_number    = $input['student_mobile_number'];
                $students->student_email            = $input['student_email'];
                $students->student_date_of_birth    = date('Y-m-d',strtotime($input['student_date_of_birth']));
                $students->gender                   = $input['gender'];
                $students->father_name              = $input['father_name'];
                $students->father_occupation        = $input['father_occupation'];
                $students->address                  = $input['address'];
                $students->landline_number          = $input['landline_number'];
                $students->device_type              = (isset($input['device_type']) && !empty($input['device_type'])) ? $input['device_type']:null;
                $students->device_token             = (isset($input['device_token']) && !empty($input['device_token'])) ? $input['device_token']:null;

                //// UPLOAD MULTIPLE IMAGES ////
                if($request->hasFile('image')) 
                {
                    $students->profile_image         = uploadImage($request,$this->studnetImagePath);
                }

                $students->save();

                DB::commit();
                return response()->json([
                    'status'    =>  1,
                    'message'   =>  trans('common.api_response_message.profile_updated')
                ]);
            }else{
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  trans('common.api_response_message.no_record_found')
                ]);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }

    /**
     * User Profile
     * @param user_id
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function userDocumetns(Request $request)
    {
        $input      = $request->all();
        $validator  = Validator::make($input, [
            'user_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $user = Students::getAllStudents('', ['id'=>$input['user_id']]);
        if(!empty($user))
        {
            //DB::enableQueryLog();
            $documents = Documents::getAllDocuments(
                '',
                [
                    'batch_id'=>$user[0]->batch_id,
                    'is_status_check'=>'1'
                ]
            );
            //dd(DB::getQueryLog());
            //dd($documents);
            if(!$documents->isEmpty())
            {
                foreach ($documents as $key => $value) {
                    $documents[$key]->list_date = date('jS-M-Y',strtotime($value['created_at']));
                    $documents[$key]->detail_date = date('jS F,Y',strtotime($value['created_at']));

                    if(!empty($documents[$key]->document) && file_exists(public_path().$this->documentsImagePath.'/'.$value->document))
                    {   
                        $documents[$key]->document = asset($this->documentsImagePath.$value->document);
                    }else{
                        $documents[$key]->document = '';
                    }
                }

                $response =   [
                    'status'    => 1,
                    'message'   => trans('common.api_response_message.success_msg'),
                    'data'      => $documents
                ];
            }else{
                $response = [
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ];
            }
            return response()->json($response);
        }
        else
        {
            return response()->json([
                'status' => 0,
                'message'=> 'User Not Found.'
            ]);
        }
    }

    /**
     * User Attendance
     * @param user_id
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function userAttendance(Request $request)
    {
        $input      = $request->all();
        $validator  = Validator::make($input, [
            'user_id'   => 'required|integer',
            'month'     => 'required|integer',
        ]);

        // if validation fails
        if ($validator->fails())
        {

            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }
        $user = Students::getAllStudents('', ['id'=>$input['user_id']]);
        if(!empty($user))
        {

            //DB::enableQueryLog();
            $attendance = StudentAttendance::select('id','date','status')->where('student_id',$input['user_id'])->whereRaw('MONTH(date) = ?',[$input['month']])->get();
            //dd(DB::getQueryLog());
            //dd($attendance);
            if(!$attendance->isEmpty())
            {
                
                $holidays = Holidays::whereRaw('MONTH(holiday_date) = ?',[date('m')])->get();
                
                /*foreach ($documents as $key => $value) {
                    $documents[$key]->list_date = date('jS-M-Y',strtotime($value['created_at']));
                    $documents[$key]->detail_date = date('jS F,Y',strtotime($value['created_at']));
                }*/

                $response =   [
                    'status'    => 1,
                    'message'   => trans('common.api_response_message.success_msg'),
                    'holidays'  => $holidays,
                    'data'      => $attendance
                ];
            }else{
                
                $response = [
                    'status'  => 1,
                    'holidays'  => $holidays,
                    'message' => trans('common.api_response_message.no_record_found'),
                ];
            }
            return response()->json($response);
        }
        else
        {

            return response()->json([
                'status' => 0,
                'message'=> 'User Not Found.'
            ],401);
        }
    }
}
