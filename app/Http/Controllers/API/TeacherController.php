<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\User;
use app\Models\Students;
use app\Models\Subjects;
use app\Models\TeacherBatch;
use app\Models\TeacherSubjects;
use app\Models\TeacherBranchBatchSubject;
use App\RoleUser;
use App\Mail\ForgetPassword;
use Validator;
use Hash;
use DB;
use Mail;

class TeacherController extends Controller
{
    /**
     * Teacher Login
     * @param mobile_number,password,device_type,device_token
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function login(Request $request)
    {
    	try 
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'mobile_number' => 'required|max:10',
                'password' 		=> 'required',
                //'device_type' 	=> 'required|in:1,2',
                //'device_token' 	=> 'required'
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');
	    	
	    	$checkLogin = User::where('contact_no',$input['mobile_number'])->where('status',1)->first();
            //dd($checkLogin);
	    	
            if(!empty($checkLogin))
	    	{ 
	    		if (Hash::check($input['password'], $checkLogin->password))
				{
					$data['id'] = $checkLogin->id;
					User::where('id', $checkLogin->id)->update(['device_type' => (!empty($input['device_type'])?$input['device_type']:''),'device_token' => (!empty($input['device_token'])?$input['device_token']:'')]);

		    		// Success Response
			    	$response 	=	[
			    		'status' 	=>	1,
			    		'message' 	=>	trans('common.api_response_message.login_successfully'),
			    		'data'		=>	nullReplaceWithEmpty($data)
			    	];
			    	return response()->json($response);
		    	}
	            // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	trans('common.api_response_message.incorrect_password'),
		    	];
		    	return response()->json($response);
	    	}
	    	else
	    	{
				// Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	trans('common.api_response_message.incorrect_mobile'),
		    	];
		    	return response()->json($response);
	    	}
    	} 
    	catch (Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }

    
    /**
     * Change Password
     * @param user_id,old_password,new_password
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function teacherLogout(Request $request)
    {
        try 
        {
            $input      = $request->all();
            $id         = $request->user_id;

            //token key is null.
            $validator = Validator::make($input, [
                'user_id'          => 'required',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            // Check token key match or not
            $userData = User::find($id);
            if(!empty($userData))
            {
                if($id != '')
                {
                    $userData->device_type              = '';
                    $userData->device_token             = '';
                    $userData->save();

                    // Success Response
                    $response   =   [
                        'status'    =>  1,
                        'message'   =>  trans('common.api_response_message.successfully_logged_out')
                    ];
                    return response()->json($response);
                        
                }
            }
            else
            {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  trans('common.api_response_message.no_record_found'),
                ]);
            }   
            
        } catch (Exception $e) {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }

    /**
     * Change Password
     * @param user_id,old_password,new_password
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function teacherChangePassword(Request $request)
    {
    	try 
    	{
    		$input 		= $request->all();
    		$id 		= $request->teacher_id;

        	//token key is null.
	       	$validator = Validator::make($input, [
                'teacher_id'  		   => 'required',
                //'old_password' 	=> 'required|string|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                'old_password'      => 'required|string|min:6',
                'new_password' 	    => 'required|string|min:6',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
            // Check token key match or not
            $userData = User::find($id);
            
            if(!empty($userData))
            {
            	if($id != '')
	    		{
	    			$oldPassword = $request->old_password;
	    			$newPassword = $request->new_password;
					if(!empty($userData) && $oldPassword != '')
		    		{
		    			if (Hash::check($oldPassword, $userData->password))
		    			{
		    				// Update password
		    				$userData->password = Hash::make($newPassword);
		    				$userData->save();

		    				// Success Response
				    		$response 	=	[
					    		'status' 	=>	1,
					    		'message' 	=>	trans('common.api_response_message.password_changed_successfully')
					    	];
					    	return response()->json($response);
		    			}
		    			else
		    			{
		    				// Error Response
				    		$response 	=	[
					    		'status' 	=>	0,
					    		'message' 	=>	trans('common.api_response_message.old_password_not_match')
					    	];
					    	return response()->json($response);
		    			}
		    		}
		    		else
		    		{
		    			// Error Response
				    	return response()->json([
				    		'status' 	=>	0,
				    		'message' 	=>	trans('common.api_response_message.no_record_found')
				    	]);
		    		}
	    		}
            }
            else
            {
            	return response()->json([
					'status' 	=>	0,
					'message' 	=> 	trans('common.api_response_message.no_record_found'),
				]);
            }	
            
    	} catch (Exception $e) {
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }

    /**
     * User Profile
     * @param user_id
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function teacherProfile(Request $request)
    {
        $input      = $request->all();
        $validator  = Validator::make($input, [
            'teacher_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $user = User::getAllUser('',['id'=>$input['teacher_id'],'status'=>'1']);

        if(!empty($user) && !$user->isEmpty($user))
        {
            unset($user->password);
            if(!empty($user[0]->image)){
                $user[0]->profile_image = check_file_exists($user[0]->image,$this->usersImagePath);
            }else{
                $user[0]->profile_image = '';
            }

            $teacherBtc    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(['teacher_id'=>$input['teacher_id']]);
            //dd($teacherBtc);
            $teacher_batch      = [];
            $student_subjects   = [];
            if(!empty($teacherBtc)){
                foreach ($teacherBtc as $key => $value) {
                    $teacher_batch[$key]['id']          = $value->batch_id;
                    $teacher_batch[$key]['batch_code']  = $value->batch_code .' ('.$value->branch_name.')';

                    $student_subjects[$key]['id']           = $value->subject_id;
                    $student_subjects[$key]['subject_name'] = $value->subject_name;
                }
                $user[0]->teacher_batch     = array_values(multi_unique($teacher_batch));
                $user[0]->student_subjects  = array_values(multi_unique($student_subjects));
            }
            else{
                $user[0]->teacher_batch     = [];
                $user[0]->student_subjects  = [];
            }
            

            $response =   [
                'status'    => 1,
                'message'   => trans('common.api_response_message.success_msg'),
                'data'      => nullReplaceWithEmpty($user[0])
            ];
            return response()->json($response);
        }
        else
        {
            return response()->json([
                'status' => 0,
                'message'=> 'User Not Found.'
            ],401);
        }
    }


    /**
     * Teacher Batch
     * @param user_id
     * @return json response - status, message
     * @author Meet Makwana
    */
    public function teacherBatch(Request $request)
    {
        $input      = $request->all();
        $validator  = Validator::make($input, [
            'teacher_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $user = User::getAllUser('',['id'=>$input['teacher_id'],'status'=>'1']);

        if(!empty($user) && !$user->isEmpty($user))
        {
            $teacherBtc    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(['teacher_id'=>$input['teacher_id']]);
            $teacher_batch      = [];
            $teacherBatch       = [];
            if(!empty($teacherBtc)){
                foreach ($teacherBtc as $key => $value) {
                    $teacher_batch[$key]['id']          = $value->batch_id;
                    $teacher_batch[$key]['batch_code']  = $value->batch_code .' ('.$value->branch_name.')';
                }
                $teacherBatch     = array_values(array_map("unserialize", array_unique(array_map("serialize", $teacher_batch))));
            }

            if(!empty($teacherBatch)){
                foreach ($teacherBatch as $key => $value) {
                    //DB::enableQueryLog();
                    $getSubjectList    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(
                        [
                            'teacher_id'    => $input['teacher_id'],
                            'batch_id'     =>  $value['id']
                        ]);
                    foreach ($getSubjectList as $subKey => $row) {
                        $teacherBatch[$key]['student_subjects'][$subKey]['id']           = $row->subject_id;
                        $teacherBatch[$key]['student_subjects'][$subKey]['subject_name'] = $row->subject_name;
                        
                    }
                    //dd(DB::getQueryLog());
                }
            }

            $response =   [
                'status'    => 1,
                'message'   => trans('common.api_response_message.success_msg'),
                'data'      => $teacherBatch
            ];
            return response()->json($response);
        }
        else
        {
            return response()->json([
                'status' => 0,
                'message'=> 'User Not Found.'
            ],401);
        }
    }

     /**
     * Edit Profile
     * @param name,email,password,date_of_birth,gender,preference_geneder,about_us,testebuds
     * @return json response - status, message,data
     * @author Meet Makwana
     */
    public function teacherEditProfile(Request $request)
    {
        try
        {
            $input = $request->all();
            $validator  = Validator::make($input, [
                'teacher_id'    =>  'required',
                'name'          =>  'required',
            ]);
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status' => 0,
                    'message'=> $validator->errors()
                ];
                return response()->json($response);
            }

            saveLog(__FUNCTION__,$input,'Request body API');
            DB::beginTransaction();
            $teacher = User::findOrNew($input['teacher_id']);

            if(!empty($teacher)){

                $teacher->name             = $input['name'];
               
                $teacher->device_type              = (isset($input['device_type']) && !empty($input['device_type'])) ? $input['device_type']:null;
                $teacher->device_token             = (isset($input['device_token']) && !empty($input['device_token'])) ? $input['device_token']:null;

                //// UPLOAD MULTIPLE IMAGES ////
                if($request->hasFile('image')) 
                {
                    $teacher->image         = uploadImage($request,$this->usersImagePath);
                }

                $teacher->save();

                DB::commit();
                return response()->json([
                    'status'    =>  1,
                    'message'   =>  trans('common.api_response_message.profile_updated')
                ]);
            }else{
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  trans('common.api_response_message.no_record_found')
                ]);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }
}
