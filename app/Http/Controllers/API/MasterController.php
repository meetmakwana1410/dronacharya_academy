<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\Notifications;
use app\Models\Students;
use app\Models\Subjects;
use app\Models\TeacherBatch;
use app\Models\TeacherSubjects;
use app\Models\Batch;
use app\Models\TeacherBranchBatchSubject;
use app\Models\ExamType;
use app\User;
use Validator;
use DB;

class MasterController extends Controller
{
    /**
     * Notification List
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function notificationList(Request $request)
    {
    	
    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'user_id' => 'required|integer',
                'page_number' => 'required|integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

    		$input 		= $request->all();
            $offset     = ($input['page_number'] - 1) * PAGINATION_LIMIT;
            
            //DB::enableQueryLog();
    		$notificationList = Notifications::getAllNotifications($offset,['user_id'=>$input['user_id']]);
            //dd(DB::getQueryLog());
    		
            $totalNotiCount = Notifications::getAllNotifications('',['user_id'=>$input['user_id']],'1');
            //dd($totalNotiCount);
	    	if(!empty($notificationList))
	        {
	        	foreach ($notificationList as $key => $value) {
	        		$notificationList[$key]->stripe_color = $this->notificationColor[$value->type];
	        		$notificationList[$key]->ago = date_difference_instagram($value->created_at);
	        	}
	        	
	            $response = [
	                'status'      => 1,
	                'message'     => trans('common.api_response_message.success_msg'),
	                'total_rows'  => !empty($totalNotiCount)?ceil($totalNotiCount/PAGINATION_LIMIT):0,
                    'data'        => $notificationList
	            ];
	            return response()->json($response);
	        }
	        else
	        {
	            $response = [
	                'status'  => 0,
	                'message' => trans('common.api_response_message.no_record_found'),
	            ];
	            return response()->json($response);
	        }
    	}
    	catch(Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }


    /**
     * Time Table List
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function timTable(Request $request)
    {
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'user_id'   => 'required|integer'
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');
            $students = Students::findOrNew($input['user_id']);
            if(!empty($students)){
                $timetable_image = '';
                $batch = Batch::findOrNew($students->batch_id);
                if(!empty($batch))
                {
                    if(!empty($batch->batch_image)){
                        $timetable_image =  check_file_exists($batch->batch_image,$this->batchImagePath);
                    }
                    
                    $response = [
                        'status'  => 1,
                        'message' => trans('common.api_response_message.success_msg'),
                        'data'    => $timetable_image
                    ];
                    return response()->json($response);
                }
                else
                {
                    $response = [
                        'status'  => 0,
                        'message' => trans('common.api_response_message.no_record_found'),
                    ];
                    return response()->json($response);
                }
            }
            else
            {
                $response = [
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ];
                return response()->json($response);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }

    /**
     * Teacher List
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getTeacherList(Request $request)
    {
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'teacher_id'   => 'integer',
                'branch_id'    => 'integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $input      = $request->all();
            $param = ['user_role'     => ROLE_TEACHER];

            if(isset($input['teacher_id']) && !empty($input['teacher_id'])){
                $param['id']        = $input['teacher_id'];
                $param['user_role'] = ROLE_TEACHER;
            }

            /*if(isset($input['branch_id']) && !empty($input['branch_id'])){
                $param['id'] = $input['branch_id']
            }*/

            $results = User::getAllUser('',$param);
           
            if(!empty($results))
            {
                return response()->json([
                    'status'      => 1,
                    'message'     => trans('common.api_response_message.success_msg'),
                    'data'        => $results
                ]);
            }
            else
            {
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ]);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }


    /**
     * Subjects List
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getSubjectsList(Request $request)
    {   
        try
        {
            $input      = $request->all();

            // Server side validations
            $validator = Validator::make($input, [
                'subject_id'   => 'integer',
            ]);

            if ($validator->fails())
            {
                return response()->json([
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ]);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $input      = $request->all();
            $param = [];
            if(isset($input['subject_id']) && !empty($input['subject_id'])){
                $param['subject_id'] = $input['subject_id'];
            }

            
            $results    = Subjects::getAllSubjects('',$param);
            //dd($results);
            if(!empty($results))
            {
                foreach ($results as $key => $value) {
                    //dd($value->subject_image);
                    //dd(public_path().$this->subjectImagePath.'/'.$value[$key]->subject_image);
                    if(!empty($value->subject_image) && file_exists(public_path().$this->subjectImagePath.'/'.$value->subject_image))
                    {   
                        $results[$key]->subject_image = asset($this->subjectImagePath.'/'.$value->subject_image);
                    }else{
                        $results[$key]->subject_image = '';
                    }
                }
                $response = [
                    'status'      => 1,
                    'message'     => trans('common.api_response_message.success_msg'),
                    'data'        => $results
                ];
                return response()->json($response);
            }
            else
            {
                $response = [
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ];
                return response()->json($response);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }

    /**
     * Get teacher assigned batch list
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getTeacherMasterList(Request $request)
    {
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'teacher_id'   => 'required|integer',
                'subject_id'   => 'integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $input      = $request->all();

            /*$param = [];

            if(isset($input['teacher_id']) && !empty($input['teacher_id'])){
                $param['id'] = $input['teacher_id'];
            }

            $teacherEtc    = TeacherBatch::getAllTeacherBatch(['teacher_id'=>$input['teacher_id']]);
            
            $teacherSub    = TeacherSubjects::getAllTeacherSubject(['teacher_id'=>$input['teacher_id']]);*/

            $teacherBtc    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(['teacher_id'=>$input['teacher_id']]);
            
            $teacherEtc      = [];
            $teacherSub   = [];
            if(!empty($teacherBtc)){
                foreach ($teacherBtc as $key => $value) {
                    $teacherEtc[$key]['id']          = $value->batch_id;
                    $teacherEtc[$key]['batch_code']  = $value->batch_code .' ('.$value->branch_name.') ['.$value->subject_name.']';

                    $teacherSub[$key]['id']           = $value->subject_id;
                    $teacherSub[$key]['subject_name'] = $value->subject_name;
                }
            }

            if(!empty($teacherEtc) || !empty($teacherSub))
            {
                return response()->json([
                    'status'      => 1,
                    'message'     => trans('common.api_response_message.success_msg'),
                    'data'        => [
                                        'batch'=>(!empty($teacherEtc)?$teacherEtc:[]),
                                        'subjects'=>(!empty($teacherSub)?multi_unique($teacherSub):[]),
                                    ]
                ]);
            }
            else
            {
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ]);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }


    /**
     * Get teacher notification list
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getTeacherNotification(Request $request)
    {
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'teacher_id'   => 'required|integer',
                'page_number'   => 'required|integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $input      = $request->all();

            $offset     = ($input['page_number'] - 1) * PAGINATION_LIMIT;
            //DB::enableQueryLog();
            $notificationList = Notifications::getAllNotifications($offset,['user_type'=>'2','user_id'=>$input['teacher_id']]);
            //dd(DB::getQueryLog());

            
            $totalNotiCount = Notifications::getAllNotifications('',['user_type'=>'2','user_id'=>$input['teacher_id']],'1');
            if(!empty($notificationList) && !$notificationList->isEmpty())
            {
                //dd($notificationList);
                foreach ($notificationList as $key => $value) {
                    $notificationList[$key]->stripe_color   = $this->notificationColor[$value->type];
                    $notificationList[$key]->ago            = date_difference_instagram($value->created_at);
                }
                
                $response = [
                    'status'      => 1,
                    'message'     => trans('common.api_response_message.success_msg'),
                    'total_rows'  => !empty($totalNotiCount)?ceil($totalNotiCount/PAGINATION_LIMIT):0,
                    'data'        => $notificationList
                ];
                return response()->json($response);
            }
            else
            {
                $response = [
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ];
                return response()->json($response);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }

    /**
     * Get batch wise students list
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getBatchStudentsList(Request $request)
    {
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'teacher_id' => 'required|integer',
                'batch_id'   => 'integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $input      = $request->all();
            if(isset($input['teacher_id']) && !empty($input['teacher_id'])){
                $param['id']        = $input['teacher_id'];
                $param['user_role'] = ROLE_TEACHER;
                $param['status']    = '1';
            }

            $results = User::getAllUser('',$param);
            //dd($results);
           
            if(!empty($results) && !$results->isEmpty())
            {
                $students = Students::select('id','batch_id','roll_number','student_name','profile_image')->where('batch_id',$input['batch_id'])->get(); 

                if(!empty($students)){

                    foreach ($students as $key => $value) {
                        if(!empty($value->profile_image)){
                            $students[$key]->profile_image = check_file_exists($value->profile_image,$this->studnetImagePath);
                        }else{
                            $students[$key]->profile_image = '';
                        }
                    }

                    //dd($students);
                    return response()->json([
                        'status'      => 1,
                        'message'     => trans('common.api_response_message.success_msg'),
                        'data'        => !empty($students)?$students:[]
                    ]);
                }else{
                    return response()->json([
                        'status'    =>  0,
                        'message' => trans('common.api_response_message.no_record_found'),
                    ]);  
                }
            }else{
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.teacher_not_exist'),
                ],401);
            }
            
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }

    /**
     * Get version details
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getParentsVersion()
    {
        try
        {
            return response()->json([
                'status'      => 1,
                'message'     => trans('common.api_response_message.success_msg'),
                'data'        => [
                    'android' => '3',
                    'ios' => '3',
                ]
            ]);
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }

    /**
     * Get version details
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getTeacherVersion()
    {
        try
        {
            return response()->json([
                'status'      => 1,
                'message'     => trans('common.api_response_message.success_msg'),
                'data'        => [
                    'android' => '1',
                    'ios' => '1',
                ]
            ]);
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }

    /**
     * Get teacher assigned batch list
     * @return json response - status, message,data
     * @author Mit Makwana
     */
    public function getExamTypeList(Request $request)
    {
        try
        {
            $input      = $request->all();
            
            saveLog(__FUNCTION__,$input,'Request body API');

            $examType    = ExamType::select('id','exam_type_name')->get();
            if(!empty($examType))
            {
                return response()->json([
                    'status'      => 1,
                    'message'     => trans('common.api_response_message.success_msg'),
                    'data'        => $examType
                ]);
            }
            else
            {
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ]);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
        }
    }
}
