<?php

namespace App\Http\Controllers\API\teacher;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\User;
use app\Models\Notifications;
use app\Models\Students;
use app\Models\Subjects;
use app\Models\StudentAttendance;
use Validator;
use DB; 

class TeacherAttendanceController extends Controller
{
    /**
     * Add Edit Home Work
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function submitAttendance(Request $request)
    {
        //dd($request->all());
        /*$data = json_decode($input['data'],true);
        foreach ($data as $key => $value) {
            print_r($value['student_id']);
            print_r($value['status']);
        }
        die;*/
        
    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'teacher_id'        => 'required|integer',
                'batch_id'          => 'required|integer',
                'data'              => 'required',
                'date'              => 'required|date|date_format:Y-m-d',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

            $teachers = User::getAllUser(
                '',
                [
                    'id'=>$input['teacher_id'],
                    'user_role'=>ROLE_TEACHER,
                    'status'=>'1',
                ]);
    
            if(!empty($teachers)){
                $data = json_decode($input['data'],true);

                $subject = [];
                if(isset($input['subject_id']) && !empty($input['subject_id'])){
                    $subject = Subjects::findOrNew($input['subject_id']);
                }

                foreach ($data as $key => $value) {
                    $studentAtt = new StudentAttendance();

                    $studentAtt->created_by     = $input['teacher_id'];
                    $studentAtt->subject_id     = $input['subject_id'];
                    $studentAtt->batch_id       = $input['batch_id'];
                    $studentAtt->date           = $input['date'];
                    $studentAtt->created_at     = date('Y-m-d H:i:s');
                    $studentAtt->student_id     = $value['student_id'];
                    $studentAtt->status         = $value['status'];
                    $studentAtt->save();

                    if(($input['is_send_notification'] == 1 || $input['is_send_sms'] == 1 ) && $value['status'] == 0){
                        
                        $students = Students::select('id','batch_id','student_name','device_type','device_token','father_mobile_number')->where('id',$value['student_id'])->get();  
                        
                        if(!empty($students) && !$students->isEmpty()){
                            foreach ($students as $key => $rows) {

                                if(!empty($subject)){
                                    $msg =  ucwords(strtolower($rows->student_name))." is absent in ".$subject->subject_name." Class on ".date('j F Y',strtotime($input['date']));
                                }else{
                                    $msg =  ucwords(strtolower($rows->student_name))." is absent in Class on ".date('j F Y',strtotime($input['date']));
                                }
                                
                                /*********** PUSH NOTIFICATION  *********/
                                if($input['is_send_notification'] == 1){
                                    if(!empty($rows->device_token)){
                                        if(!empty($rows->device_type == 1)){
                                            $json_data_android = array(
                                                'badge'         => 0,
                                                'flag'          => 'student_absent',
                                                'master_id'     => $value['student_id'],
                                                'msg'           => $msg,
                                            );
                                            send_notification_android($rows->device_token,$msg,$json_data_android,' - Homework','#2d949c');
                                        }
                                        elseif(!empty($rows->device_type == 2)){
                                            $json_data_iphone = array(
                                                'badge'         => 0,
                                                'flag'          => 'student_absent',
                                                'master_id'     => $value['student_id'],
                                                'msg'           => $msg,
                                            );
                                            send_notification_iphone($rows->device_token,$msg,$json_data_iphone,' - Homework');
                                        }
                                    }
                                    $noti = new Notifications();
                                    $noti->user_type    = '1'; //1 - Student
                                    $noti->type         = '5'; //Attendance
                                    $noti->master_id    = $value['student_id'];
                                    $noti->user_id      = $value['student_id'];
                                    $noti->message      = $msg;
                                    $noti->save();
                                }

                                /*********** TEXT SMS  *********/
                                if($input['is_send_sms'] == 1){
                                    $sendsms = sendSms($rows->father_mobile_number,$msg);
                                    //dd($sendsms);
                                }
                            }
                        }
                    }

                    
                    


                      
                }


                return response()->json([
                    'status'  => 1,
                    'message' => trans('common.api_response_message.success_msg'),
                ]);
                
            }else{
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.teacher_not_exist'),
                ],401);
            }
    	}
    	catch(Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }
}
