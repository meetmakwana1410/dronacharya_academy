<?php

namespace App\Http\Controllers\API\teacher;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\User;
use app\Models\Notifications;
use app\Models\Students;
use app\Models\Homework;
use app\Models\HomeworkImages;
use Validator;
use DB; 

class TeacherHoweworkController extends Controller
{
    /**
     * Add Edit Home Work
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function addEditHomeWork(Request $request)
    {
        //dd($request->all());
    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'teacher_id'        => 'required|integer',
                'batch_id'          => 'required|integer',
                'subject_id'        => 'required|integer',
                'submission_date'   => 'required|date|date_format:Y-m-d|',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

            $teachers = User::find($input['teacher_id']);
    
            if(!is_null($teachers)){
                
                if(isset($input['id'])){
                    $homeWork = Homework::findOrNew($input['id']);
                }else{
                    $homeWork = new Homework();
                }             

                $homeWork->created_by       = $input['teacher_id'];
                $homeWork->batch_id         = $input['batch_id'];
                $homeWork->subject_id       = $input['subject_id'];
                $homeWork->submission_date  = $input['submission_date'];
                $homeWork->note             = !empty($input['note'])?$input['note']:'';
                $homeWork->created_at       = date('Y-m-d H:i:s');

                if($request->hasFile('document')) 
                {
                    $homeWork->document         = uploadImage($request,$this->homeWorkImagePath,'document');
                }

                $homeWork->save();
                
                //// UPLOAD MULTIPLE IMAGES ////
                if($request->hasFile('image')) 
                {
                    $image         = uploadMultipleImages($request,$this->homeWorkImagePath);
                    foreach ($image as $key => $value) {
                        $homeworkImages = new HomeworkImages();
                        $homeworkImages->home_work_id   = $homeWork->id;
                        $homeworkImages->image          = $value['name'];
                        $homeworkImages->save();
                    }
                }

                
                
                if(isset($input['id'])){
                    return response()->json([
                        'status'  => 1,
                        'message' => trans('common.api_response_message.homework_updated_success'),
                    ]);
                }else{

                    if(isset($homeWork->id) && !empty($homeWork->id)){
                        $homeWorkData = Homework::getAllHomework(
                        '',
                        [
                            'home_work_id' => $homeWork->id,
                        ]);
                        

                        //Point 1 : Hindi homework has been assigned to the students and needs to be submitted on 28 July 2021
                        $msg =  $homeWorkData[0]['subject_name']." homework has been assigned to the students and needs to be submitted on ".date('j F Y',strtotime($input['submission_date']));
                    
                    
                        $students = Students::select('id','batch_id','student_name','device_type','device_token')->where('device_token','!=','')->where('batch_id',$input['batch_id'])->get();  
                    
                        if(!empty($students) && !$students->isEmpty()){
                            foreach ($students as $key => $value) {
                                if(!empty($value->device_token)){
                                    if(!empty($value->device_type == 1)){
                                        $json_data_android = array(
                                            'badge'         => 0,
                                            'flag'          => 'homework_added_by_teacher',
                                            'master_id'     => $homeWork->id,
                                            'msg'           => $msg,
                                        );
                                        send_notification_android($value->device_token,$msg,$json_data_android,' - Homework','#2d949c');
                                    }
                                    elseif(!empty($value->device_type == 2)){
                                        $json_data_iphone = array(
                                            'badge'         => 0,
                                            'flag'          => 'homework_added_by_teacher',
                                            'master_id'     => $homeWork->id,
                                            'msg'           => $msg,
                                        );
                                        send_notification_iphone($value->device_token,$msg,$json_data_iphone,' - Homework');
                                    }
                                }

                                $noti = new Notifications();

                                $noti->user_type    = '1'; //1 - Student
                                $noti->type         = '3'; //HomeWork
                                $noti->master_id    = $homeWork->id;
                                $noti->user_id      = $value->id;
                                $noti->message      = $msg;
                                $noti->save();
                            }
                        }
                    }
                    
                    return response()->json([
                        'status'  => 1,
                        'message' => trans('common.api_response_message.success_msg'),
                    ]);
                }
            }else{
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.teacher_not_exist'),
                ],401);
            }
    	}
    	catch(Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }

    /**
     * Remove Howmwork Image (Delete)
     * @param id
     * @return json response - status, message
     * @author Meet Makwana
     */
    public function removeHomeWorkImage(Request $request)
    {
        $input      = $request->all();

        $validator  = Validator::make($input, [
            'image_id' => 'required',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $image = HomeworkImages::find($input['image_id']);
        if(!empty($image))
        {
            \File::delete(public_path($this->homeWorkImagePath) . '/' . $image->image); 
            
            HomeworkImages::where('id',$input['image_id'])->delete();

            return response()->json([
                'status' => 1,
                'message'=> trans('common.api_response_message.image_deleted_success')
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> trans('common.api_response_message.no_record_found')
            ]);
        }
    }

    /**
     * Home Work List with filter
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function homeWorkList(Request $request)
    {
        //dd($request->all());
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'teacher_id'        => 'required|integer',
                'batch_id'          => 'integer',
                'subject_id'        => 'integer',
                'submission_date'   => 'date|date_format:Y-m-d|',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $teachers = User::find($input['teacher_id']);
    
            if(!is_null($teachers)){

                //DB::enableQueryLog();
                $homeWork = Homework::getAllHomework(
                    '',
                    [
                        'teacher_id'        => $input['teacher_id'],
                        'batch_id'          => isset($input['batch_id'])?$input['batch_id']:'',
                        'subject_id'        => isset($input['subject_id'])?$input['subject_id']:'',
                        'submission_date'   => isset($input['submission_date'])?$input['submission_date']:'',
                    ]);
                //dd(DB::getQueryLog());
                //dd($homeWork);
                            
                
                if(!$homeWork->isEmpty()){

                    foreach ($homeWork as $key => $value) {

                        $homeWork[$key]->submission_date = date('d-m-Y',strtotime($value['submission_date']));
                        $homeWork[$key]->homework_list_date = date('jS-M-Y',strtotime($value['submission_date']));

                        $homeWork[$key]->issue_date = date('d-m-Y',strtotime($value['created_at']));
                        $homeWork[$key]->homework_list_issuedate = date('jS-M-Y',strtotime($value['created_at']));


                        if(!empty($value['document']) && file_exists(public_path().$this->homeWorkImagePath.'/'.$value['document']))
                        {   
                            $homeWork[$key]->document = asset($this->homeWorkImagePath.'/'.$value['document']);
                        }else{
                            $homeWork[$key]->document = '';
                        }

                        if(!empty($value['subject_image']) && file_exists(public_path().$this->subjectImagePath.'/'.$value['subject_image']))
                        {   
                            $homeWork[$key]->subject_image = asset($this->subjectImagePath.'/'.$value['subject_image']);
                        }else{
                            $homeWork[$key]->subject_image = '';
                        }
                        //dd($homeWork[$key]->images);
                        if(!empty($homeWork[$key]->images))
                        {   
                            $exp        = explode(',', $homeWork[$key]->images);
                            $expids     = explode(',', $homeWork[$key]->images_id);
                            
                            foreach ($exp as $imgkey => $hWValue) {
                                $image[$imgkey]['id'] = $expids[$imgkey];
                                if(file_exists(public_path().$this->homeWorkImagePath.$hWValue)){
                                    $image[$imgkey]['image'] = asset($this->homeWorkImagePath.$hWValue);
                                }
                            }
                            $homeWork[$key]->images = $image;
                        }else{
                            $homeWork[$key]->images = [];
                        }
                    }
                    //dd($homeWork);
                    return response()->json([
                        'status'  => 1,
                        'message' => trans('common.api_response_message.success_msg'),
                        'data' => $homeWork,
                    ]);
                }else{
                    return response()->json([
                        'status'  => 0,
                        'message' => trans('common.api_response_message.no_record_found'),
                    ]);
                }
            }else{
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.teacher_not_exist'),
                ],401);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }

    

    /**
     * Remove Howmwork Image (Delete)
     * @param id
     * @return json response - status, message
     * @author Meet Makwana
     */
    public function deleteHomeWork(Request $request)
    {
        $input      = $request->all();

        $validator  = Validator::make($input, [
            'home_work_id' => 'required|integer',
        ]);

        // if validation fails
        if ($validator->fails())
        {
            // Error Response
            $response   =   [
                'status' => 0,
                'message'=> $validator->errors()
            ];
            return response()->json($response);
        }

        $homeWork = Homework::findOrNew($input['home_work_id']);
       
        if(!empty($homeWork))
        {
            $homeworkImages = HomeworkImages::where('home_work_id',$input['home_work_id'])->get();
           
            if(!empty($homeworkImages)){
                foreach ($homeworkImages as $key => $value) {
                    \File::delete(public_path($this->homeWorkImagePath) . '/' . $value->image); 
                    HomeworkImages::where('id',$value['id'])->delete();
                }
            }
            $homeWork->delete();

            return response()->json([
                'status' => 1,
                'message'=> trans('common.api_response_message.homework_delete_success')
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message'=> trans('common.api_response_message.no_record_found')
            ]);
        }
    }
}
