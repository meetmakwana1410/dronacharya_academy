<?php

namespace App\Http\Controllers\API\teacher;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\Notifications;
use app\Models\Exam;
use app\Models\TeacherBranchBatchSubject;
use app\Models\ExamStudents;
use app\User;
use Validator;
use DB; 
class TeacherExamController extends Controller
{
    /**
     * Report List
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function teacherExamList(Request $request)
    {

    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'teacher_id' => 'required|integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

            //DB::enableQueryLog();
            $results = TeacherBranchBatchSubject::getTeacherExam([
                'teacher_id'    => !empty($input['teacher_id'])?$input['teacher_id']:'',
                'subject_id'    => !empty($input['subject_id'])?$input['subject_id']:'',
                'batch_id'      => !empty($input['batch_id'])?$input['batch_id']:'',
            ]);
            //dd(DB::getQueryLog());

	    	if(!$results->isEmpty())
	        {
                foreach ($results as $key => $value) {
                    $results[$key]->exam_date = date('d-m-Y',strtotime($value['exam_date']));
                    $results[$key]->exam_time = date('g:i a',strtotime($value['exam_time']));
                    $results[$key]->to_exam_time = date('g:i a',strtotime($value['to_exam_time']));
                    $results[$key]->percentage = (string)round($value['percentage']);
                    $results[$key]->exam_list_date = date('jS-M-Y',strtotime($value['exam_date']));
                    $results[$key]->exam_detail_date = date('jS F,Y',strtotime($value['exam_date']));

                    $highestMark = ExamStudents::where('exam_id',$value['id'])->select(DB::raw('MAX(CAST(obtain_marks as UNSIGNED)) as highest_exam_mark'))->get();
                    //dd($highestMark);

                    if(!$highestMark->isEmpty())
                    {
                        $results[$key]->highest_mark = (string)$highestMark[0]['highest_exam_mark'] ;
                    }else{
                        $results[$key]->highest_mark = '0';
                    }

                    if(!empty($results[$key]->subject_image) && file_exists(public_path().$this->subjectImagePath.'/'.$results[$key]->subject_image))
                    {   
                        $results[$key]->subject_image = asset($this->subjectImagePath.'/'.$results[$key]->subject_image);
                    }else{
                        $results[$key]->subject_image = '';
                    }
                }
                //die;
	            $response = [
	                'status'  => 1,
	                'message' => trans('common.api_response_message.success_msg'),
	                'data'    => $results
	            ];
	            return response()->json($response);
	        }
	        else
	        {
	            $response = [
	                'status'  => 0,
	                'message' => trans('common.api_response_message.no_record_found'),
	            ];
	            return response()->json($response);
	        }
    	}
    	catch(Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }
}
