<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\Notifications;
use app\Models\Students;
use app\Models\Homework;
use Validator;
use DB; 
class HomeworkController extends Controller
{
    /**
     * Report List
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function list(Request $request)
    {
    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'student_id'           => 'required|integer',
                'teacher_id'        => 'integer',
                'subject_id'        => 'integer',
                'suggestion_date'   => 'date|date_format:Y-m-d',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

            $offset     = ($input['page_number'] - 1) * PAGINATION_LIMIT;

            //DB::enableQueryLog();
            $results = Students::getAllHomeworkStudents($offset,
                [
                    'student_id'        => $input['student_id'],
                    'teacher_id'        => !empty($input['teacher_id'])?$input['teacher_id']:'',
                    'subject_id'        => !empty($input['subject_id'])?$input['subject_id']:'',
                    'suggestion_date'   => !empty($input['suggestion_date'])?$input['suggestion_date']:'',
                    'search_text'       => !empty($input['search_text'])?$input['search_text']:'',
                    
                ]);


            $totalHomeworkCnt = Students::getAllHomeworkStudents('',['student_id'=>$input['student_id']],'1');
            //dd(DB::getQueryLog());

	    	if(!$results->isEmpty())
	        {
                foreach ($results as $key => $value) {
                    $results[$key]->submission_date = date('d-m-Y',strtotime($value['submission_date']));
                    $results[$key]->homework_list_date = date('jS-M-Y',strtotime($value['submission_date']));

                    $results[$key]->issue_date = date('d-m-Y',strtotime($value['created_at']));
                    $results[$key]->homework_list_issuedate = date('jS-M-Y',strtotime($value['created_at']));

                    if(!empty($results[$key]->images))
                    {   
                        $exp        = explode(',', $results[$key]->images);
                        $expids     = explode(',', $results[$key]->images_id);
                        
                        foreach ($exp as $imgkey => $hWValue) {
                            $image[$imgkey]['id'] = $expids[$imgkey];
                            if(file_exists(public_path().$this->homeWorkImagePath.$hWValue)){
                                $image[$imgkey]['image'] = asset($this->homeWorkImagePath.$hWValue);
                            }
                        }
                        $results[$key]->images = $image;
                    }else{
                        $results[$key]->images = [];
                    }

                    if(!empty($value['document']) && file_exists(public_path().$this->homeWorkImagePath.'/'.$value['document']))
                    {   
                        $results[$key]->document = asset($this->homeWorkImagePath.'/'.$value['document']);
                    }else{
                        $results[$key]->document = '';
                    }


                    //dd($value);

                    if(!empty($value['subject_image']) && file_exists(public_path().$this->subjectImagePath.'/'.$value['subject_image']))
                    {   
                        $results[$key]->subject_image = asset($this->subjectImagePath.'/'.$value['subject_image']);
                    }else{
                        $results[$key]->subject_image = '';
                    }
                }
                
	            return response()->json([
                    'status'        => 1,
                    'message'       => trans('common.api_response_message.success_msg'),
                    'total_rows'    => !empty($totalHomeworkCnt)?ceil($totalHomeworkCnt/PAGINATION_LIMIT):0,
                    'data'          => $results
                ]);
	        }
	        else
	        {
	            return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.no_record_found'),
                ]);
	        }
    	}
    	catch(Exception $e)
    	{
	    	return response()->json([
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ]);
    	}
    }

    /**
     * Home Work Details with filter
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function homeWorkDetails(Request $request)
    {
        //dd($request->all());
        try
        {
            $input      = $request->all();
            // Server side validations
            $validator = Validator::make($input, [
                'student_id'        => 'required|integer',
                'home_work_id'      => 'required|integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
                $response   =   [
                    'status'    =>  0,
                    'message'   =>  $validator->errors()
                ];
                return response()->json($response);
            }
            saveLog(__FUNCTION__,$input,'Request body API');

            $students = Students::find($input['student_id']);
    
            if(!is_null($students)){

                //DB::enableQueryLog();
                $homeWork = Homework::getAllHomework(
                    '',
                    [
                        'home_work_id'   => $input['home_work_id'],
                    ]);
                //dd(DB::getQueryLog());
                //dd($homeWork);
                            
                
                if(!$homeWork->isEmpty()){

                    foreach ($homeWork as $key => $value) {
                        $homeWork[$key]->submission_date = date('d-m-Y',strtotime($value['submission_date']));
                        $homeWork[$key]->homework_list_date = date('jS-M-Y',strtotime($value['submission_date']));

                        $homeWork[$key]->issue_date = date('d-m-Y',strtotime($value['created_at']));
                        $homeWork[$key]->homework_list_issuedate = date('jS-M-Y',strtotime($value['created_at']));


                        if(!empty($value['document']) && file_exists(public_path().$this->homeWorkImagePath.'/'.$value['document']))
                        {   
                            $homeWork[$key]->document = asset($this->homeWorkImagePath.'/'.$value['document']);
                        }else{
                            $homeWork[$key]->document = '';
                        }

                        if(!empty($value['subject_image']) && file_exists(public_path().$this->subjectImagePath.'/'.$value['subject_image']))
                        {   
                            $homeWork[$key]->subject_image = asset($this->subjectImagePath.'/'.$value['subject_image']);
                        }else{
                            $homeWork[$key]->subject_image = '';
                        }
                        //dd($homeWork[$key]->images);
                        if(!empty($homeWork[$key]->images))
                        {   
                            $exp        = explode(',', $homeWork[$key]->images);
                            $expids     = explode(',', $homeWork[$key]->images_id);
                            
                            foreach ($exp as $imgkey => $hWValue) {
                                $image[$imgkey]['id'] = $expids[$imgkey];
                                if(file_exists(public_path().$this->homeWorkImagePath.$hWValue)){
                                    $image[$imgkey]['image'] = asset($this->homeWorkImagePath.$hWValue);
                                }
                            }
                            $homeWork[$key]->images = $image;
                        }else{
                            $homeWork[$key]->images = [];
                        }
                    }
                    //dd($homeWork);
                    return response()->json([
                        'status'  => 1,
                        'message' => trans('common.api_response_message.success_msg'),
                        'data' => $homeWork,
                    ]);
                }else{
                    return response()->json([
                        'status'  => 0,
                        'message' => trans('common.api_response_message.no_record_found'),
                    ]);
                }
            }else{
                return response()->json([
                    'status'  => 0,
                    'message' => trans('common.api_response_message.student_not_exist'),
                ],401);
            }
        }
        catch(Exception $e)
        {
            // Error Response
            $response   =   [
                'status'    =>  0,
                'message'   =>  $e->getMessage()
            ];
            return response()->json($response);
        }
    }
}
