<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Models\Notifications;
use app\Models\Exam;
use app\Models\ExamStudents;
use app\Models\Students;
use Validator;
use DB; 
class DashboardController extends Controller
{
    /**
     * Report List
     * @return json response - status, message,data
     * @author Meet makwana
     */
    public function parenstsDashboard(Request $request)
    {
    	try
    	{
    		$input 		= $request->all();
    		// Server side validations
            $validator = Validator::make($input, [
                'user_id' => 'required|integer',
            ]);

            // if validation fails
            if ($validator->fails())
            {
                // Error Response
	    		$response 	=	[
		    		'status' 	=>	0,
		    		'message' 	=>	$validator->errors()
		    	];
		    	return response()->json($response);
            }
	    	saveLog(__FUNCTION__,$input,'Request body API');

            //DB::enableQueryLog();
            $results = ExamStudents::getExamStudents([
                                    'student_id'    => $input['user_id'],
                                    'tab_type'      => '2',
                                    'is_group_by'   => '1'
                                ]);
            //dd(DB::getQueryLog());

            //DB::enableQueryLog();
            $homework = Students::getAllHomeworkStudents('',
                        [
                            'student_id'        => $input['user_id'],
                            'date_between_week' => '1',
                        ]);
            //dd($hw);
            //dd(DB::getQueryLog());

	    	if(!$results->isEmpty() || !$homework->isEmpty())
	        {  
                if(!$results->isEmpty()){
                    foreach ($results as $key => $value) {
                        $results[$key]->exam_date = date('d-m-Y',strtotime($value['exam_date']));
                        $results[$key]->exam_time = date('g:i a',strtotime($value['exam_time']));
                        $results[$key]->to_exam_time = date('g:i a',strtotime($value['to_exam_time']));
                        $results[$key]->percentage = (string)round($value['percentage']);                        
                        $results[$key]->exam_list_date = date('jS-M-Y',strtotime($value['exam_date']));
                        $results[$key]->exam_detail_date = date('jS F,Y',strtotime($value['exam_date']));

                        $highestMark = ExamStudents::where('exam_id',$value['exam_id'])->select(DB::raw('MAX(CAST(obtain_marks as UNSIGNED)) as highest_exam_mark'))->get();
                        //dd($highestMark);

                        if(!$highestMark->isEmpty())
                        {
                            $results[$key]->highest_mark = (string)$highestMark[0]['highest_exam_mark'] ;
                        }else{
                            $results[$key]->highest_mark = '0';
                        }

                        if(!empty($results[$key]->subject_image) && file_exists(public_path().$this->subjectImagePath.'/'.$results[$key]->subject_image))
                        {   
                            $results[$key]->subject_image = asset($this->subjectImagePath.'/'.$results[$key]->subject_image);
                        }else{
                            $results[$key]->subject_image = '';
                        }
                    }
                }

                if(!$homework->isEmpty()){
                    foreach ($homework as $key => $value) {
                        if(!empty($homework[$key]->subject_image) && file_exists(public_path().$this->subjectImagePath.'/'.$homework[$key]->subject_image))
                        {   
                            $homework[$key]->subject_image = asset($this->subjectImagePath.'/'.$homework[$key]->subject_image);
                        }else{
                            $homework[$key]->subject_image = '';
                        }

                        $homework[$key]->submission_date = date('d-m-Y',strtotime($value['submission_date']));
                        $homework[$key]->homework_list_date = date('jS-M-Y',strtotime($value['submission_date']));
                        $homework[$key]->issue_date = date('d-m-Y',strtotime($value['created_at']));
                        $homework[$key]->homework_list_issuedate = date('jS-M-Y',strtotime($value['created_at']));
                


                        if(!empty($value->images))
                        {   
                            $exp        = explode(',', $value->images);
                           
                            $expids     = explode(',', $value->images_id);
                            
                            foreach ($exp as $imgkey => $hWValue) {
                                $image[$imgkey]['id'] = $expids[$imgkey];
                                if(file_exists(public_path().$this->homeWorkImagePath.$hWValue)){
                                    $image[$imgkey]['image'] = asset($this->homeWorkImagePath.$hWValue);
                                }
                            }
                            $homework[$key]->images = $image;
                        }else{
                            $homework[$key]->images = [];
                        }
                    }
                }
                
	            $response = [
	                'status'   => 1,
	                'message'  => trans('common.api_response_message.success_msg'),
	                'data'     => !empty($results)?$results:[],
                    'homework' => !empty($homework)?$homework:[]
	            ];
	            return response()->json($response);
	        }
	        else
	        {
	            $response = [
	                'status'  => 0,
	                'message' => trans('common.api_response_message.no_record_found'),
	            ];
	            return response()->json($response);
	        }
    	}
    	catch(Exception $e)
    	{
    		// Error Response
    		$response 	=	[
	    		'status' 	=>	0,
	    		'message' 	=>	$e->getMessage()
	    	];
	    	return response()->json($response);
    	}
    }
}
