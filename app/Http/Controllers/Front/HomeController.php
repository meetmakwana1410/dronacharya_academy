<?php

namespace app\Http\Controllers\Front;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
//use app\Http\Controllers\CommonController;
use DB;

class HomeController extends Controller
{
   /* public function __construct() {
        parent::__construct();
    }*/
	/**
     * Show the webshop front.
     *
     * @return array
     */
    public function index(Request $request)
    {
        try
        {   
            return view('front.home.homefront');
        }
        catch(Exception $e)
        {

        }
    	
    }
}
