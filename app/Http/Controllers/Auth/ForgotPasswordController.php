<?php

namespace app\Http\Controllers\Auth;

use app\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use DB;
use app\User;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $user_check = User::where('email', $request->email)->first();

        $parent_user_check = null;
        if(!empty($user_check->parent_id)){
            $parent_user_check = User::find( $user_check->parent_id);

        }
        

        if (empty($user_check)) {
            return back()->with('error', 'Email id does not exist with us.');
        } else if(isset($user_check) && $user_check->status == 0) {
            return back()->with('error', trans('common.message.you_are_deactivate_by_admin'));
            
        }

        if(!empty($parent_user_check) ){
            if( $parent_user_check->status == 0) {
                return back()->with('error', trans('common.message.you_are_deactivate_by_admin'));
            }
        }
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

         return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);
                   
    }

    //Password Broker for Seller Model
    public function broker()
    {
         return Password::broker();
    }
}
