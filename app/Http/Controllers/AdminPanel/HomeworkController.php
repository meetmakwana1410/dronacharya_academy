<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\DocumentsRequest;
use app\Models\Documents;
use app\Models\Subjects;
use app\Models\Batch;
use app\Models\Homework;
use app\Models\HomeworkImages;
use Auth;
use DB;
use Storage;
use Carbon;
use Illuminate\Support\Facades\View;

class HomeworkController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'uploads/documents';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'batch_code',
                1 => 'subject_name',
                2 => 'submission_date',
                3 => 'id',
            );
           
            $params = array(
                'batch_code'    => $request->columns[0]['search']['value'],
                'subject_name'  => $request->columns[1]['search']['value'],
                'submission_date'          => $request->columns[2]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
            );
            
            //DB::enableQueryLog();
            $results = Homework::getAllHomework($request->length);
            //dd(DB::getQueryLog());
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->batch_code;
                    $tempArray[] = $result->subject_name;
                    $tempArray[] = date('j F, Y',strtotime($result->submission_date));

                    $viewActionButton =  View::make('admin.homework.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.homework.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function getHomework($id = null) {
        $homework = [];
        $homework = Homework::getAllHomework('',['home_work_id'=>$id]);
        //dd($homework);

        if(!empty($homework)){
            //dd($homework[0]->created_at);
            $homework[0]->submission_date = date('j F, Y',strtotime($homework[0]->submission_date));
            //$homework[0]->created_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $homework[0]->created_at)->format('j F, Y');

           
        }
        return view('admin.homework.view', compact('homework'));
    }
}
