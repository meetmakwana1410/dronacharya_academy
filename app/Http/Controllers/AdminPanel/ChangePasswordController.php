<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use App\Http\Requests\BackChangePasswordRequest;
use App\Http\Controllers\Controller;

class ChangePasswordController extends AdminCommonController
{
    /**
     * Display change password form
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	return view('admin.change_password.change_password');
    }

    public function changePassword(Request $request)
    {

        try
        {
            $oldPswd = trim($request->old_password);
            $request->new_password;
        	$request->new_confirm_password;
            $loggedInId = Auth::user()->id;

           
            // Check current password match with db or not
            $user = User::where('id',$loggedInId)->select(['id','name','email','password'])->get();
            /*echo '<pre>';
            print_r($user[0]['email']);
            print_r($user[0]['password']);*/
           
            
            if (Hash::check($oldPswd, $user[0]['password']))
            {
              
                // Change with new password
                $password = Hash::make($request->new_password);
                $users = User::find($user[0]['id']);
                $users->id       = Auth::user()->id;
                $users->password = $password;
                $users->save();
               // dd($users);die;
                // Flash success
                $notification = array(
                    'message'    => __('common.message.change_password_success'),
                    'alert-type' => 'success'
                );
            }
            else
            {
                //die("else");
                // Flash error
                $notification = array(
                    'message'    => __('common.message.password_match'),
                    'alert-type' => 'error'
                );
            }
           //die("exit");
            \Session::flash('notification', $notification);
            return redirect()->route('change.password.get');
        }
        catch(\Exception $e)
        {
            print_r($e->getMessage());die;
            //die("Asd");
            // Flash error
            $notification = array(
                'message'    => $e->getMessage(), 
                'alert-type' => 'error'
            );
            \Session::flash('notification', $notification);
            return redirect()->route('change.password.get');
        }
    }
}
