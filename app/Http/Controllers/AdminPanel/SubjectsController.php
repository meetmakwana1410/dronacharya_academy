<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\SubjectsRequest;
use app\Models\Subjects,app\Models\Standards,app\Models\StandardSubjects;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\View;

class SubjectsController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'subjects';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'subject_image',
                1 => 'subject_name',
                2 => 'standards',
                3 => 'id',
            );
           
            $params = array(
                'subject_image' => $request->columns[0]['search']['value'],
                'subject_name'  => $request->columns[1]['search']['value'],
                'standards'     => $request->columns[2]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
            );
           
            $results = Subjects::getAllSubjects($request->length, $params);
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();

                    //echo public_path().'uploads'.'/'.$this->foldername.'/'.$result->subject_image;
                    if(!empty($result->subject_image) && file_exists(public_path().'/uploads'.'/'.$this->foldername.'/'.$result->subject_image))
                    { 
                        //dd(asset('/uploads'.'/'.$this->foldername.'/'.$result->subject_image));
                        $tempArray[] = '<img src="'.asset('/uploads'.'/'.$this->foldername.'/'.$result->subject_image).'" style="width: 70px;" id="default_logo">';
                    }else{
                        $tempArray[] = '';
                    }

                    $tempArray[] = $result->subject_name;
                    $tempArray[] = $result->standards;

                    $viewActionButton =  View::make('admin.subjects.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.subjects.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $subjects = [];
        $standardSubjects = [];
        $standards       = Standards::all();
        if (!empty($id)) {
            $subjects               = Subjects::findOrNew($id);
            $subjectsStandardData   = StandardSubjects::where('subject_id',$id)->get();
            $standardSubjects = [];
            if(!empty($subjectsStandardData)){
                foreach ($subjectsStandardData as $key => $value) {
                    $standardSubjects[] = $value->standard_id;
                }
            }
        }
        return view('admin.subjects.add_edit', compact('subjects','standards','standardSubjects'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectsRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $subjects   = null;
        $old_logo = ''; 
        if (!empty($id)) {
            StandardSubjects::where('subject_id', '=', $id)->delete();

            $subjects = Subjects::findOrNew($id);

            $old_logo = $subjects->subject_image;
        }
        if (empty($subjects)) {
            $subjects = new Subjects();
        }
       
        $subjects->subject_name   = $request->request->get('subject_name');


        if(!is_dir(public_path('uploads/').$this->foldername)){
            mkdir(public_path('uploads/').$this->foldername , 0777, true);
        }

        $logo = $request->input('image_name');
        //Upload logo
        if(!empty($logo))
        {
            if(!empty($logo) && !empty($old_logo))
            {   
                if(!empty($old_logo) && file_exists(public_path('uploads/').$this->foldername."/".$old_logo))
                { 
                    unlink(public_path('uploads/').$this->foldername."/".$old_logo);
                }
            }
            
            $images      = explode('#', substr($request->input('image_name'), 1));
            $data1       = explode(',',  $images[1]);

            $bgImgPath  = public_path('uploads/').$this->foldername."/" ;
            
            if(!empty($data1[0]))
            {   
                $exp = explode("/", $data1[0]);
                
                if(!empty($exp[1]))
                {
                    $extantion = explode(";", $exp[1]);
                }
            }
            
            if(!empty($extantion[0]))
            {
                $output_file    = $bgImgPath.time() . '.'.$extantion[0]; 
                $exp = explode("/",$output_file);
                $travalue_image = end($exp);

            }
            else
            {
                $output_file    = $bgImgPath.time() . '.jpeg';
                $exp = explode("/",$output_file);
                $travalue_image = end($exp);
            }
          
            $ifp    = fopen($output_file, "wb"); 
            if(!empty($data1[1]))
            {
                $fwrite = fwrite($ifp, base64_decode($data1[1])); 
                
                if ($fwrite === true)
                {
                    chmod($output_file, 644); 
                }
            }

            $subjects->subject_image   = $travalue_image;
        }

        $logo_remove_flag     = $request->input('logo_remove_flag');
        if(!empty($logo_remove_flag) && $logo_remove_flag == '1')
        {   
            if(!empty($old_logo) && file_exists(public_path('uploads/').$this->foldername."/".$old_logo))
            { 
                unlink(public_path('uploads/').$this->foldername."/".$old_logo);
                $subjects->subject_image  = '';
            }
        }

        $subjects->save();

        if($subjects->id){
            $standard   = $request->request->get('standard');
            foreach ($standard as $key => $value) {
                DB::table('standard_subjects')->insert(
                    ['standard_id' => $value, 'subject_id' => $subjects->id]
                );
            }
        }

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('subject')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Subjects::findOrNew($id);
        if (!empty($data)) {

            if(!empty($data->subject_image) && file_exists(public_path('uploads/').$this->foldername."/".$data->subject_image))
            { 
                unlink(public_path('uploads/').$this->foldername."/".$data->subject_image);
            }

            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
