<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\ExamTypeRequest;
use app\Models\ExamType;
use Auth;
use Illuminate\Support\Facades\View;

class ExamTypeController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'exam_type';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {
            $columns = array(
                0 => 'exam_type_name',
                1 => 'id',
            );
            $params = array(
                'exam_type_name'=> $request->columns[0]['search']['value'],
                'order_column' => $columns[$request->order[0]['column']],
                'order_dir' => $request->order[0]['dir'],
            );

            $results = ExamType::getAllExamType($request->length, $params);
            //dd($results);
            $data = array();

            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->exam_type_name;

                    $viewActionButton =  View::make('admin.exam_type.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.exam_type.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $examtype = [];
        if (!empty($id)) {
            $params = array(
                'id'=> $id,
            );
            $examtype   = ExamType::findOrNew($id);
            //dd($examtype);
        }
        return view('admin.exam_type.add_edit', compact('examtype'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(ExamTypeRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated      = $request->validated();
        $examType   = null;

        if (!empty($id)) {
            $examType = ExamType::findOrNew($id);
        }
        if (empty($examType)) {
            $examType = new ExamType();
        }
       
        $examType->exam_type_name                 = $request->request->get('exam_type_name');
        $examType->save();

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('exam_type')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = ExamType::findOrNew($id);
        if (!empty($data)) {
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    
}
