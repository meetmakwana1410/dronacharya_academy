<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\SchoolRequest;
use app\Models\School;
use Auth;
use Illuminate\Support\Facades\View;

class SchoolController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'school';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {
            $columns = array(
                0 => 'school_name',
                1 => 'id',
            );
            $params = array(
                'school_name'=> $request->columns[0]['search']['value'],
                'order_column' => $columns[$request->order[0]['column']],
                'order_dir' => $request->order[0]['dir'],
            );

            $results = School::getAllSchool($request->length, $params);
            //dd($results);
            $data = array();

            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->school_name;

                    $viewActionButton =  View::make('admin.school.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.school.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $data = [];
        if (!empty($id)) {
            $params = array(
                'id'=> $id,
            );
            $data   = School::findOrNew($id);
            //dd($data);
        }
        return view('admin.school.add_edit', compact('data'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated      = $request->validated();
        $iuData   = null;

        $loginUser  = Auth::user();
      
        if (!empty($id)) {
            $iuData = School::findOrNew($id);
            $iuData->updated_by = $loginUser->id;
            $iuData->updated_at = date('Y-m-d H:i:s');
        }
        if (empty($iuData)) {
            $iuData = new School();
            $iuData->status = 1;
            $iuData->created_by = $loginUser->id;
            $iuData->created_at = date('Y-m-d H:i:s');
        }
       
        $iuData->school_name     = trim($request->request->get('school_name'));
        $iuData->save();

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('school')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = School::findOrNew($id);
        if (!empty($data)) {
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $data = School::findOrNew($id);
        if (!empty($data)) {
            if($data->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $data->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $data->status = 1;
            }
            $data->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
