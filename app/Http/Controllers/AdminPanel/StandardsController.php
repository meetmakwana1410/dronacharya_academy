<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\StandardsRequest;
use app\Models\Standards,app\Models\Subjects,app\Models\StandardSubjects;
use Auth;
use DB;
use Illuminate\Support\Facades\View;

class StandardsController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'standards';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {
            $columns = array(
                0 => 'standard_name',
                1 => 'subject_name',
                2 => 'id',
            );
            $params = array(
                'standard_name' => $request->columns[0]['search']['value'],
                'subject_name'  => $request->columns[1]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
            );
            //dd($request->length);
            $results = Standards::getAllStandards($request->length, $params);
            //dd($results);
            $data = array();

            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->standard_name;
                    $tempArray[] = $result->subject_name;

                    $viewActionButton =  View::make('admin.standard.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.standard.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $standards = [];
        //$subjects       = Subjects::all();
        if (!empty($id)) {
            $params = array(
                'id'=> $id,
            );
            $standards              = Standards::findOrNew($id);
            /*$standardSubjectsData   = StandardSubjects::where('standard_id',$id)->get();

            $standardSubjects = [];
            if(!empty($standardSubjectsData)){
                foreach ($standardSubjectsData as $key => $value) {
                    $standardSubjects[] = $value->subject_id;
                }
            }*/
        }
        return view('admin.standard.add_edit', compact('standards'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(StandardsRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated      = $request->validated();
        $standard   = null;

        if (!empty($id)) {
            StandardSubjects::where('standard_id', '=', $id)->delete();

            $standard = Standards::findOrNew($id);
        }
        if (empty($standard)) {
            $standard = new Standards();
        }
       
        $standard->standard_name   = $request->request->get('standard_name');
        $standard->save();

        /*if($standard->id){
            $subjects   = $request->request->get('subjects');
            foreach ($subjects as $key => $value) {
                DB::table('standard_subjects')->insert(
                    ['standard_id' => $standard->id, 'subject_id' => $value]
                );
            }
        }*/

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('standards')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Standards::findOrNew($id);
        if (!empty($data)) {
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
