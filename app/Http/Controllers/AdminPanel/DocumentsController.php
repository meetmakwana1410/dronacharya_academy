<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\DocumentsRequest;
use app\Models\Documents;
use app\Models\Subjects;
use app\Models\Batch;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\View;

class DocumentsController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'uploads/documents';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'batch_code',
                1 => 'subject_name',
                2 => 'name',
                3 => 'id',
            );
           
            $params = array(
                'batch_code'    => $request->columns[0]['search']['value'],
                'subject_name'  => $request->columns[1]['search']['value'],
                'name'          => $request->columns[2]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
            );
           
            $results = Documents::getAllDocuments($request->length, $params);
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->batch_code;
                    $tempArray[] = $result->subject_name;
                    $tempArray[] = $result->name;

                    //echo (public_path().'/'.$this->foldername.'/'.$result->document);
                    if(!empty($result->document) && file_exists(public_path().'/'.$this->foldername.'/'.$result->document))
                    { 
                        if (in_array($result->extension, ["jpg", "png", "jpeg"])){
                            $exp = '<a href="'.asset($this->foldername.'/'.$result->document).'" target="_blank"><img src="'.asset('/img'.'/doc_img.png').'" style="width: 70px;" id="default_logo"></a>';
                        }elseif($result->extension == 'pdf'){
                            $exp = '<a href="'.asset($this->foldername.'/'.$result->document).'" target="_blank" ><img src="'.asset('/img'.'/doc_pdf.png').'" style="width: 70px;" id="default_logo"></a>';
                        }
                        $tempArray[] = $exp;
                    }else{
                        $tempArray[] = '';
                    }

                    $tempArray[] = date('j F, Y',strtotime($result->created_at));

                    $viewActionButton =  View::make('admin.documents.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            $batch    = Batch::where('status','1')->get();

            $params['order_dir'] = 'DESC';
            $results = Documents::getAllDocuments('', $params);
            $view = view('admin.documents.ajaxview', compact('results'))->render();
            return view('admin.documents.index',compact('batch','view'));
        }
    }

    public function allDocAjax(Request $request){
        $batch_id   = $request->request->get('batch_id');
        $results    = Documents::getAllDocuments('', ['batch_id'=> $batch_id]);
        
        $view       = view('admin.documents.ajaxview', compact('results'))->render();
        return response()->json(['html' => $view]);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $documents = [];
        
        $branch_user_id = '';
        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $branch_user_id     = Auth::user()->id;
        }
        $batch  = Batch::getAllBatch(
            '',
            ['batch_created_by' => $branch_user_id]);
        //dd($batch);
        $subjects       = Subjects::all();
        if (!empty($id)) {
            $documents               = Documents::findOrNew($id);
            //$subjects               = Subjects::findOrNew($id);
        }
        return view('admin.documents.add_edit', compact('subjects','documents','batch'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentsRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $subjects   = null;

        if (!empty($id)) {
            $documents = Documents::findOrNew($id);
        }
        else{
            $documents = new Documents();
        }
       
        $documents->batch_id     = $request->request->get('batch_id');
        $documents->subject_id   = $request->request->get('subject_id');
        $documents->name         = $request->request->get('name');
        $documents->note         = $request->request->get('note');


        if(!is_dir(public_path('uploads/').$this->foldername)){
            mkdir(public_path('uploads/').$this->foldername , 0777, true);
        }

        //// UPLOAD MULTIPLE IMAGES ////
        if($request->hasFile('image')) 
        {
            $documents->document    = uploadImage($request,$this->foldername);
            $documents->extension   =  $request->file('image')->getClientOriginalExtension();
        }

       
        $documents->save();


        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('documents')->with('success', $successMessage);
    }

     /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $data = Documents::findOrNew($id);
        if (!empty($data)) {
            if($data->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $data->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $data->status = 1;
            }
            $data->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Documents::findOrNew($id);
        if (!empty($data)) {
            if(!empty($data->document) && file_exists(public_path().'/'.$this->foldername."/".$data->document))
            { 
                unlink(public_path().'/'.$this->foldername."/".$data->document);
            }
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
