<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Models\Batch;
use app\Models\Students;
use app\Models\Notifications;
use app\User;
use Auth;
use DB;
use Storage;
use Hash;
use Illuminate\Support\Facades\View;

class NotificationsController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $batch      = Batch::getAllBatch('',['is_status_check'=>'1']);
        $students   = Students::getAllStudents('',['is_status_check'=>'1']);
        if($batch->isEmpty()){
            return redirect('notifications')->with('error', trans('common.message.enter_batch'));
        }

        $teacher    = User::getAllUser('', ['user_role'     => ROLE_TEACHER,'device_token_check'=>1]);

        return view('admin.notifications.add_edit', compact('batch','teacher','students'));
    }


    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = null) {
        //dd($request->all());

        $input = $request->all();
        // Retrieve the validated input data...
        //$validated  = $request->validated();
        $loginUser  = Auth::user();
        //echo '<pre>';
        if(!empty($input['student_id']) || !empty($input['batch_id'])){

            $msg = strip_tags(trim($input['message']));
            //dd($input['student_id']);
            /*********** IF STUDENT SELECT : START *********/
            if(isset($input['student_id']) && !empty($input['student_id'])){
                foreach ($input['student_id'] as $key => $value) {
                    
                    $students   = Students::select('id','student_name','father_mobile_number','device_type','device_token')->where('id',$value)->where('device_token','!=','')->first();
                   
                    /*********** PUSH NOTIFICATION *********/
                    if(!empty($students)){
                        if(in_array('1', $input['message_type'])){
                            if(!empty($students['device_type'] == 1)){
                                $json_data_android = array(
                                    'badge'         => 0,
                                    'flag'          => 'system_general_notification',
                                    'msg'           => $msg,
                                );
                                send_notification_android($students['device_token'],$msg,$json_data_android,' - General','#ad7c1f');
                            }
                            elseif(!empty($students['device_type'] == 2)){
                                $json_data_iphone = array(
                                    'badge'         => 0,
                                    'flag'          => 'system_general_notification',
                                    'msg'           => $msg,
                                );
                                send_notification_iphone($students['device_token'],$msg,$json_data_iphone,' - General');
                            }

                            $noti = new Notifications();
                            $noti->type         = '4';
                            $noti->master_id    = '0';
                            $noti->user_id      = $students['id'];
                            $noti->message      = $msg;
                            $noti->created_by   = $loginUser->id;
                            $noti->save();
                        }

                        /*********** TEXT SMS  *********/
                        if(in_array('2', $input['message_type'])){
                            $sendsms = sendSms($students['father_mobile_number'],$msg);
                            //dd($sendsms);
                        }

                    }
                }
            }
            /*********** IF STUDENT SELECT : END *********/


            /*********** IF TEACHER SELECT : START *********/ 
            if(isset($input['teacher_id']) && !empty($input['teacher_id'])){
                foreach ($input['teacher_id'] as $key => $value) {
                    $teacher    = User::getAllUser('', ['user_role'     => ROLE_TEACHER,'id'=>$value]);
                    //dd($teacher);

                    /*********** PUSH NOTIFICATION *********/
                    if(in_array('1', $input['message_type'])){
                        if(!empty($teacher[0]['device_token'])){
                            if(!empty($teacher[0]['device_type'] == 1)){
                                $json_data_android = array(
                                    'badge'         => 0,
                                    'flag'          => 'system_general_notification',
                                    'msg'           => $msg,
                                );
                                send_notification_android($teacher[0]['device_token'],$msg,$json_data_android,' - General','#97a84c');
                            }
                            elseif(!empty($teacher[0]['device_type'] == 2)){
                                $json_data_iphone = array(
                                    'badge'         => 0,
                                    'flag'          => 'system_general_notification',
                                    'msg'           => $msg,
                                );
                                send_notification_iphone($teacher[0]['device_token'],$msg,$json_data_iphone,' - General');
                            }

                            $noti = new Notifications();
                            $noti->user_type    = '2';
                            $noti->type         = '4';
                            $noti->master_id    = '0';
                            $noti->user_id      = $teacher[0]['id'];
                            $noti->message      = $msg;
                            $noti->created_by   = $loginUser->id;
                            $noti->save();
                        }
                    }

                    /*********** TEXT SMS  *********/
                    if(in_array('2', $input['message_type'])){
                        $sendsms = sendSms($students['contact_no'],$msg);
                    }
                }
            }
            /*********** IF TEACHER SELECT : END *********/


            /*********** IF BRANCH SELECT : START *********/
            /*
            if(isset($input['batch_id']) && !empty($input['batch_id'])){
                foreach ($input['batch_id'] as $key => $value) {
                    
                    $students   = Students::select('id','student_name','father_mobile_number','device_type','device_token')->where('batch_id',$value)->where('device_token','!=','')->get();
                   
                    if(!empty($students)){
                        
                        foreach ($students as $stdKey => $stud) {
                   
                            
                            if(in_array('1', $input['message_type'])){
                                if(!empty($stud['device_token'])){
                                    if(!empty($stud['device_type'] == 1)){
                                        $json_data_android = array(
                                            'badge'         => 0,
                                            'flag'          => '3',
                                            'msg'           => $msg,
                                        );
                                        send_notification_android($stud['device_token'],$msg,$json_data_android,' - General','#97a84c');
                                    }
                                    elseif(!empty($stud['device_type'] == 2)){
                                        $json_data_iphone = array(
                                            'badge'         => 0,
                                            'flag'          => '3',
                                            'msg'           => $msg,
                                        );
                                        send_notification_iphone($stud['device_token'],$msg,$json_data_iphone,' - General');
                                    }

                                    $noti = new Notifications();
                                    $noti->type         = '4';
                                    $noti->master_id    = '0';
                                    $noti->user_id      = $stud['id'];
                                    $noti->message      = $msg;
                                    $noti->created_by   = $loginUser->id;
                                    $noti->save();
                                }
                            }

                            
                            if(in_array('2', $input['message_type'])){

                            }
                        }
                    }
                }
            }*/
            /*********** IF BRANCH SELECT : END *********/
        }

        return redirect('notifications')->with('success', trans('common.message.notification_sent'));
    }

    /**
     * Retrieve all students list as per batch selection
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
    */
    public function getBatchStudent(Request $request){
        $batch_code = request('batch_code');
        if(!empty($batch_code)){
            $students = Students::where('batch_id',$batch_code)->get();
            if(!empty($students)){
                $dropHtml = '';
                //$dropHtml = '<option value="">Select Students</option>';
                foreach ($students as $key => $value) {
                    $dropHtml .= '<option value="'.$value->id.'" >'.$value->student_name.'</option>';
                }
                return ['flag'=>1,'html'=>$dropHtml];
            }
            else{
                return ['flag'=>0,'html'=>''];
            }
        }else{
                return ['flag'=>0,'html'=>''];
            }
    }
}
