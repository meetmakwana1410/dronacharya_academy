<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\StudentsRequest;
use app\Models\Students;
use app\Models\ExamStudents;
use app\Models\StudentSubjects;
use app\Models\Batch;
use app\Models\TeacherBranchBatchSubject;
use app\Models\School;
use Auth;
use DB;
use Storage;
use Hash;
use Illuminate\Support\Facades\View;

class StudentsController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'students';
        $this->loginUser  = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {
            $columns = array(
                0 => 'roll_number',
                1 => 'student_name',
                2 => 'student_mobile_number',
                3 => 'father_name',
                4 => 'father_mobile_number',
                5 => 'batch_code',
                6 => 'id',
            );
                
            $student_branch_id = '';
            //dd(Auth::user());
            if(Auth::user()->hasRole(ROLE_BRANCH)){
                $student_branch_id     = Auth::user()->id;
            }

            
            $params = array(
                'roll_number'           => $request->columns[0]['search']['value'],
                'student_name'          => $request->columns[1]['search']['value'],
                'student_mobile_number' => $request->columns[2]['search']['value'],
                'father_name'           => $request->columns[3]['search']['value'],
                'father_mobile_number'  => $request->columns[4]['search']['value'],
                'batch_code'            => $request->columns[5]['search']['value'],
                'order_column'          => $columns[$request->order[0]['column']],
                'order_dir'             => $request->order[0]['dir'],
                'student_branch_id'     => $student_branch_id,
            );

            //DB::enableQueryLog();
            $results = Students::getAllStudents($request->length, $params);
            //dd(DB::getQueryLog());
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->roll_number;
                    $tempArray[] = $result->student_name;
                    $tempArray[] = $result->student_mobile_number;
                    $tempArray[] = $result->father_name;
                    $tempArray[] = $result->father_mobile_number;
                    $tempArray[] = $result->batch_code.' '.$result->branch_name;

                    $viewActionButton =  View::make('admin.students.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.students.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $students           = [];
        $studentsSubjects   = [];
        $school     = School::getAllSchool('',['is_status_check'=>'1']);

        $branch_user_id = '';
        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $branch_user_id     = Auth::user()->id;
        }
        $batch  = Batch::getAllBatch(
            '',
            ['batch_created_by' => $branch_user_id,'is_status_check' => '1']);

        if($batch->isEmpty()){
            return redirect('students')->with('error', trans('common.message.enter_batch'));
        }
        $batchSubject           = [];
        if (!empty($id)) {
            $students      = Students::findOrNew($id);
            $studSubjects  = StudentSubjects::where('students_id',$id)->get();
            if(!empty($studSubjects)){
                foreach ($studSubjects as $key => $value) {
                    $studentsSubjects[] = $value->subject_id;
                }
            }
            $batchSubject           = Batch::getBatchSubjects($students->batch_id);
        }
        return view('admin.students.add_edit', compact('students','batch','studentsSubjects','batchSubject','school'));
    }

    /**
     * Retrieve all subject list as per batch selection
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
    */
    public function getBatchSubject(Request $request){
        $batch_code = request('batch_code');

        if(!empty($batch_code)){
            $batchSubject = Batch::getBatchSubjects($batch_code);

            if(!empty($batchSubject)){

                //DB::enableQueryLog();
                $teachers    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(['batch_id'      => $batch_code],'teacher_branch_batch_subject.teacher_id');
                //dd(DB::getQueryLog());
                //dd($teachers);  

                $dropHtml = '<option value="">Select Subject</option>';
                foreach ($batchSubject as $key => $value) {
                    $dropHtml .= '<option value="'.$value->subject_id.'" >'.$value->subject_name.'</option>';
                }

                $teacherDropHtml = '<option value="">Select teacher</option>';
                foreach ($teachers as $key => $value) {
                    $teacherDropHtml .= '<option value="'.$value->teacher_id.'" >'.$value->teacher_name.'</option>';
                }
                return ['flag'=>1,'html'=>$dropHtml,'teacherDropHtml'=>$teacherDropHtml];
            }
            else{
                return ['flag'=>0,'html'=>'','teacherDropHtml'=>''];
            }
        }else{
                return ['flag'=>0,'html'=>'','teacherDropHtml'=>''];
            }
    }
    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(StudentsRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $students   = null;
        $loginUser  = Auth::user();
        if (!empty($id)) {
            StudentSubjects::where('students_id', '=', $id)->delete();

            $students = Students::findOrNew($id);
            $students->updated_by = $loginUser->id;
        }
        if (empty($students)) {
            $students = new Students();
            $students->created_by = $loginUser->id;
        }

        $batch = Batch::where('id',$request->request->get('batch_id'))->first();
        //dd($batch->branch_id);
        //dd($request->all());
        /*if(Auth::user()->hasRole(ROLE_BRANCH)){
            $students->batch_id           = $loginUser->id;
        }else{*/
            $students->batch_id           = $request->request->get('batch_id');
            $students->branch_id          = $batch->branch_id;
        //}

        $students->roll_number              = $request->request->get('roll_number');
        $students->student_name             = $request->request->get('student_name');
        $students->student_date_of_birth    = date('Y-m-d',strtotime($request->request->get('student_date_of_birth')));
        $students->gender                   = $request->request->get('gender');
        $students->student_mobile_number    = $request->request->get('student_mobile_number');
        $students->student_email            = $request->request->get('student_email');
        $students->father_name              = $request->request->get('father_name');
        $students->father_mobile_number     = $request->request->get('father_mobile_number');
        $students->mother_mobile_number     = $request->request->get('mother_mobile_number');
        $students->landline_number          = $request->request->get('landline_number');
        $students->father_email             = $request->request->get('father_email');
        $students->father_occupation        = $request->request->get('father_occupation');

        $password                           = $request->request->get('password');
        if(!empty($password)){
            $students->password             = Hash::make($password);
        }
        $students->address                  = $request->request->get('address');
        //$students->batch_id                 = $request->request->get('batch_id');
        $students->admission_date           = date('Y-m-d',strtotime($request->request->get('admission_date')));
        if(!empty($request->request->get('school_id'))){
            $students->school_id                = $request->request->get('school_id');
        }
        //dd($students);
        $students->save();

        if($students->id){
            $subjects   = $request->request->get('subjects');
            foreach ($subjects as $key => $value) {
                DB::table('student_subjects')->insert(
                    ['students_id' => $students->id, 'subject_id' => $value]
                );
            }
        }

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $msg = 'Congratulations! You are register  as a student in DRONACHARYA ACADEMY. Your username and password for application is </br> Username:'.$request->father_mobile_number.' , Password: '.$request->request->get('password').' </br> From : DRONACHARYA ACADEMY';
            $sendsms = sendSms($request->father_mobile_number,$msg);
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('students')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Students::findOrNew($id);
        if (!empty($data)) {
            StudentSubjects::where('students_id', '=', $id)->delete();
            ExamStudents::where('student_id', '=', $id)->delete();
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $product = Students::findOrNew($id);
        if (!empty($product)) {
            if($product->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $product->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $product->status = 1;
            }
            $product->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
