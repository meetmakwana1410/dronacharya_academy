<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\BranchRequest;
use app\User;
use app\RoleUser;
use Auth;
use DB;
use Storage;
use Hash;
use Illuminate\Support\Facades\View;

class BranchController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'name',
                1 => 'email',
                2 => 'contact_no',
                3 => 'id',
            );
            $params = array(
                'name'          => $request->columns[0]['search']['value'],
                'email'         => $request->columns[1]['search']['value'],
                'contact_no'    => $request->columns[2]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
                'user_role'     => ROLE_BRANCH,
            );

            $results = User::getAllUser($request->length, $params);
            
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->name;
                    $tempArray[] = $result->email;
                    $tempArray[] = $result->contact_no;

                    $viewActionButton =  View::make('admin.branch.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.branch.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $branch           = [];

        if (!empty($id)) {
            $branch         = User::findOrNew($id);
        }
        return view('admin.branch.add_edit', compact('branch'));
    }

    
    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $branch   = null;
        $loginUser  = Auth::user();
        if (!empty($id)) {
            $branch = User::findOrNew($id);
            $branch->updated_by = $loginUser->id;
        }
        if (empty($branch)) {
            $branch = new User();
            $branch->created_by = $loginUser->id;
            $branch->created_at = date('Y-m-d H:i:s');
        }

        $branch->name              = $request->request->get('name');
        $branch->email             = strtolower(trim($request->request->get('email')));
        $branch->contact_no        = $request->request->get('contact_no');
        $branch->password          = Hash::make($request->request->get('password'));
        $branch->save();

        if (empty($id)) {
            DB::table('role_users')->insert(
                    ['role_id' => '2', 'user_id' =>$branch->id]
                );
        }

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('branch')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = User::findOrNew($id);
        if (!empty($data)) {
            RoleUser::where('user_id', '=', $id)->delete();
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $user = User::findOrNew($id);
        if (!empty($user)) {
            if($user->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $user->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $user->status = 1;
            }
            $user->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
