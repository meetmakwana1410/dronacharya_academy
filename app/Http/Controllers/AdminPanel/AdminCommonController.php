<?php

namespace app\Http\Controllers\AdminPanel;
use app\Http\Controllers\CommonController;
use Illuminate\Http\Request;
use Auth;

class AdminCommonController extends CommonController
{
    public function __construct() {
        parent::__construct();
    }
}
