<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\ExamRequest;
use app\Models\Exam;
use app\Models\Batch;
use app\Models\Students;
use app\Models\Notifications;
use app\Models\Subjects;
use app\Models\ExamStudents;
use app\Models\ExamType;
use app\Models\TeacherBranchBatchSubject;
use app\User;
use Auth;
use DB;
use Storage;
use Hash;
use Illuminate\Support\Facades\View;

class ExamController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'exam';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'batch_code',
                1 => 'exam_date',
                2 => 'subject_name',
                3 => 'id',
            );
            
            $brach_id = '';
            if(Auth::user()->hasRole(ROLE_BRANCH)){
                $brach_id = Auth::user()->id;
            }

            $params = array(
                'batch_code'            => $request->columns[0]['search']['value'],
                'exam_date'             => $request->columns[1]['search']['value'],
                'subject_name'          => $request->columns[2]['search']['value'],
                'brach_id'              => $brach_id,
                'order_column'          => $columns[$request->order[0]['column']],
                'order_dir'             => $request->order[0]['dir'],
            );
            
            //DB::enableQueryLog();
            $results = Exam::getAllExam($request->length, $params);
            //dd(DB::getQueryLog());
            //dd($results);

            $data = array();
            if (!empty($results)) {
                foreach ($results as $key => $value) {

                    $marksStatus = ExamStudents::where('exam_id',$value->id)->whereIn('status', [1,2,3])->get();
                    
                    if(!empty($marksStatus) && !$marksStatus->isEmpty()){
                        $results[$key]['exam_mark_status'] = '1';
                    }else{
                        $results[$key]['exam_mark_status'] = '0';
                    }
                }

                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->batch_code;
                    $tempArray[] = date('d-m-Y',strtotime($result->exam_date)).' ('.date('g:i a',strtotime($result->exam_time)).' To '.date('g:i a',strtotime($result->to_exam_time)).')';
                    $tempArray[] = $result->subject_name.' ('.$result->total_marks.')';
                    //$tempArray[] = $result->chapter;
                    $viewActionButton =  View::make('admin.exam.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.exam.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $exam     = [];
        $batch    = Batch::getAllBatch('',['is_status_check'=>'1']);

        if($batch->isEmpty()){
            return redirect('exam')->with('error', trans('common.message.enter_batch'));
        }
        
        $examType   = ExamType::all();
        $teacher    = User::getAllUser('', ['user_role'     => ROLE_TEACHER]);
        $batchSubject = [];
        if (!empty($id)) {
            $exam            = Exam::findOrNew($id);
            $batchSubject    = Batch::getBatchSubjects($exam->batch_id);
        }
        return view('admin.exam.add_edit', compact('exam','batch','batchSubject','teacher','examType'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(ExamRequest $request, $id = null) {
        /*dd($_REQUEST);
        dd($request->all());*/
        $input = $request->all();
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $exam   = null;
        $loginUser  = Auth::user();
        if (!empty($id)) {
            $exam = Exam::findOrNew($id);
            $exam->updated_by = $loginUser->id;
        }
        if (empty($exam)) {
            $exam = new Exam();
            if(Auth::user()->hasRole(ROLE_BRANCH)){
                $exam->branch_id     = $loginUser->id;
            }elseif(Auth::user()->hasRole(SUPER_ADMIN_ROLE)){
                $getBrachId = Batch::findOrNew($request->request->get('batch_id'));
                $exam->branch_id     = $getBrachId->branch_id;
            }
            $exam->created_by       = $loginUser->id;
        }

        $exam->batch_id             = $request->request->get('batch_id');
        $exam->exam_category_id     = $request->request->get('exam_category_id');
        $exam->subject_id           = $request->request->get('subject_id');
        $exam->total_marks          = $request->request->get('total_marks');
        $exam->chapter              = isset($input['chapter'])?$input['chapter']:'';
        $exam->teacher_id           = isset($input['teacher_id'])?$input['teacher_id']:'';
        $exam->exam_date            = date('Y-m-d',strtotime($request->request->get('exam_date')));
        $exam->exam_time            = date('H:i',strtotime($request->request->get('exam_time')));
        $exam->to_exam_time         = date('H:i',strtotime($request->request->get('to_exam_time')));

        $to_time    = strtotime($exam->exam_time);
        $from_time  = strtotime($exam->to_exam_time);

        $exam->exam_duration        =  round(abs($to_time - $from_time) / 60,2). " minute";
        
        $exam->save();

        $this->changeStatus($exam->id);

        //////// SEND NOTIFICATION TO TEACHER ////////
        $teacher    = [];
        if(isset($input['teacher_id'])){
            $teacher    = User::findOrNew($request->request->get('teacher_id'));
        }
        $subjects   = Subjects::findOrNew($request->request->get('subject_id'));
        
        
        if(!empty($teacher)){

            if($request->request->get('exam_category_id') == 1){
                $cat = 'Weekly';
            }else{
                $cat = 'Midterm';
            }

            //Point 3 Midterm Exam of Hindi will be held on July 19,2021 from 6 pm to 7 pm
            $msg = $cat." Exam of ".$subjects->subject_name.", chapter ".$request->request->get('chapter')." will be held on ".date('F j, Y',strtotime($request->request->get('exam_date')))." (".date('g:i a',strtotime($request->request->get('exam_time'))).')';
            
            
            if(!empty($teacher->device_token)){
                if(!empty($teacher->device_type == 1)){
                    $json_data_android = array(
                        'badge'         => 0,
                        'flag'          => 'exam_assigned_to_teacher',
                        'master_id'       => $exam->id,
                        'msg'           => $msg,                       
                    );
                    send_notification_android($teacher->device_token,$msg,$json_data_android,' - Exam','#97a84c');
                }
                elseif(!empty($teacher->device_type == 2)){
                    $json_data_iphone = array(
                        'badge'         => 0,
                        'flag'          => 'exam_assigned_to_teacher',
                        'master_id'       => $exam->id,
                        'msg'           => $msg,
                    );
                    send_notification_iphone($teacher->device_token,$msg,$json_data_iphone,' - Exam');
                }

                $noti = new Notifications();

                $noti->user_type    = '2';
                $noti->type         = '2';
                $noti->master_id    = $exam->id;
                $noti->user_id      = $teacher->id;
                $noti->message      = $msg;
                $noti->save();
            }
        }

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('exam')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Exam::findOrNew($id);
        if (!empty($data)) {
            ExamStudents::where('exam_id',$id)->delete();
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $data = Exam::findOrNew($id);
        if (!empty($data)) {
            if($data->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $data->status = 0;
            }else{
                //////// SEND NOTIFICATION TO STUDENTS ////////
                /*if($data->exam_date >= date('Y-m-d')) // If exam date is past then today date then don't send notification
                {*/
                    $students = Students::getAllExamStudents(
                                            [
                                                'batch_id'      => $data->batch_id,
                                                'subject_id'    => $data->subject_id,
                                            ]
                                        );
                    
                    if(!empty($students) && !$students->isEmpty()){

                        if($data->exam_category_id == 1){
                            $cat = 'Weekly';
                        }else{
                            $cat = 'Midterm';
                        }

                        //Point 3 Midterm Exam of Hindi will be held on July 19,2021 from 6 pm to 7 pm
                        $msg = $cat." Exam of ".$students[0]->subject_name.", (chapter ".$data->chapter.") will be held on ".date('F j, Y',strtotime($data->exam_date))." From ".date('g:i a',strtotime($data->exam_time)).' to '.date('g:i a',strtotime($data->to_exam_time)).' ( Duration : '.$data->exam_duration.')';

                        //dd($students);
                        foreach ($students as $key => $value) {
                            $noti = new Notifications();

                            $noti->type         = '2';
                            $noti->master_id    = $data->id;
                            $noti->user_id      = $value->id;
                            $noti->message      = $msg;
                            $noti->save();

                            $examStud = new ExamStudents();
                            $examStud->exam_id          = $data->id;
                            $examStud->student_id       = $value->id;
                            $examStud->save();

                            if(!empty($value->device_token)){
                                if(!empty($value->device_type == 1)){
                                    $json_data_android = array(
                                        'badge'         => 0,
                                        'flag'          => 'exam_assigned_to_student',
                                        'master_id'       => $data->id,
                                        'msg'           => $msg,
                                    );
                                    send_notification_android($value->device_token,$msg,$json_data_android,' - Exam','#97a84c');
                                }
                                elseif(!empty($value->device_type == 2)){
                                    $json_data_iphone = array(
                                        'badge'         => 0,
                                        'flag'          => 'exam_assigned_to_student',
                                        'master_id'       => $data->id,
                                        'msg'           => $msg,
                                    );
                                    send_notification_iphone($students[0]->device_token,$msg,$json_data_iphone,' - Exam');
                                }
                            }
                        }
                    }
                //}
                
                $responseMessage = trans('common.message.activated_sucessfully');
                $data->status = 1;
            }
            $data->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Display a listing exam students.
     *
     * @return \Illuminate\Http\Response
    */
    public function getExamStudent($id = null) {
        $results = [];
        if($id != ''){
 
            //DB::enableQueryLog();
            $results = ExamStudents::getExamStudents(['exam_id'=>$id,'is_group_by'=>'1','is_order_by'=>'1']);
            //dd(DB::getQueryLog());
            //dd($results);

            if($results->isNotEmpty()){
                return view('admin.exam.exam_students.index', compact('results'));
            }else{
                return view('admin.exam.exam_students.index', compact('results'));
            }
        }
        else{
            return redirect('exam')->with('success', trans('common.label.no_record_found'));
        }
    }

    /**
     * Display a listing exam students.
     *
     * @return \Illuminate\Http\Response
    */
    public function submitMark(Request $request,$id = null) {

        //dd($request->all());

        $input = $request->all();
        $results = [];
        $loginUser  = Auth::user();
        if($id != ''){
              
            foreach ($input['student_id'] as $key => $value) {
                //echo($input['exam_students_id']);
                $examStud   = ExamStudents::findOrNew($input['exam_students_id'][$key]);

                    /*echo '<pre>';
                    //print_r($examStud);
                    echo $examStud->status.' != '.$input['attendance_status'][$key].'<br>';
                    echo $examStud->obtain_marks.' != '.$input['obtain_mark'][$key].'<br>';*/

                if (!empty($examStud) && isset($input['attendance_status'][$key]) && (($examStud->status != $input['attendance_status'][$key] ) || ($examStud->obtain_marks != $input['obtain_mark'][$key]))) {

                    $examData   = Exam::findOrNew($examStud->exam_id);
                    //dd($examData);


                    if($examData->exam_category_id == 1){
                        $cat = 'Weekly';
                    }else{
                        $cat = 'Midterm';
                    }

                    if($input['attendance_status'][$key] == 3){
                        //echo "3<br>";
                        $examStud->obtain_marks = 0;
                        $examStud->percentage   = 0;
                    }elseif($input['attendance_status'][$key] == 1){
                        //echo "1<br>";
                        $examStud->obtain_marks = !empty($input['obtain_mark'][$key])?$input['obtain_mark'][$key]:0;
                        $examStud->percentage   = 0;

                        //////// SEND NOTIFICATION TO STUDENTS////////

                        //DB::enableQueryLog();
                        $students = Students::getAllExamStudents(
                                                [
                                                    'subject_id'    => $examData->subject_id,
                                                    'student_id'    => $examStud->student_id,
                                                ]
                                            );
                        //dd(DB::getQueryLog());
                        
                        if(!empty($students)){

                            //Point 3 Midterm Exam of Hindi will be held on July 19,2021 from 6 pm to 7 pm
                            $msg = $cat." Exam of ".$students[0]->subject_name.", (chapter ".$examData->chapter.") was be held on  ".date('F j, Y',strtotime($examData->exam_date))." (".date('g:i a',strtotime($examData->exam_time)).') was not attended.';

                            if(!empty($students[0]->device_token)){
                                if(!empty($students[0]->device_type == 1)){
                                    $json_data_android = array(
                                        'badge'         => 0,
                                        'flag'          => 'exam_mark_added',
                                        'master_id'       => $examData->id,
                                        'msg'           => $msg,
                                    );
                                    send_notification_android($students[0]->device_token,$msg,$json_data_android,' - Exam','#FF0000');
                                }
                                elseif(!empty($students[0]->device_type == 2)){
                                    $json_data_iphone = array(
                                        'badge'         => 0,
                                        'flag'          => 'exam_mark_added',
                                        'master_id'       => $examData->id,
                                        'msg'           => $msg,
                                    );
                                    send_notification_iphone($students[0]->device_token,$msg,$json_data_iphone,' - Exam');
                                }

                                $noti = new Notifications();

                                $noti->type         = '2';
                                $noti->master_id    = $examData->id;
                                $noti->user_id      = $examStud->student_id;
                                $noti->message      = $msg;
                                $noti->save();
                            }
                        }
                    }else{
                        //echo "2<br>";
                        if(isset($input['obtain_mark'][$key])){
                            $examStud->obtain_marks         = $input['obtain_mark'][$key];
                            if($input['obtain_mark'][$key] <= $input['total_marks'][$key]){
                                $examStud->percentage       = ($input['obtain_mark'][$key]*100)/$input['total_marks'][$key];

                                //////// SEND NOTIFICATION TO STUDENTS////////

                            //DB::enableQueryLog();
                            $students = Students::getAllExamStudents(
                                                    [
                                                        'subject_id'    => $examData->subject_id,
                                                        'student_id'    => $examStud->student_id,
                                                    ]
                                                );
                            //dd(DB::getQueryLog());
                            
                            if(!empty($students)){
                                //Point 2 Midterm Exam of Hindi was held on July 19, 2021 and the  student obtained 25 out of 30
                                $msg = $cat." Exam of ".$students[0]->subject_name.", (chapter ".$examData->chapter.") was held on ".date('F j, Y',strtotime($examData->exam_date))." (".date('g:i a',strtotime($examData->exam_time)).') and the student obtained '.$input['obtain_mark'][$key].' out of '.$examData->total_marks;

                                if(!empty($students[0]->device_token)){
                                    if(!empty($students[0]->device_type == 1)){
                                        $json_data_android = array(
                                            'badge'         => 0,
                                            'flag'          => 'exam_mark_added',
                                            'master_id'       => $examData->id,
                                            'msg'           => $msg,
                                        );
                                        send_notification_android($students[0]->device_token,$msg,$json_data_android,' - Exam','#97a84c');
                                    }
                                    elseif(!empty($students[0]->device_type == 2)){
                                        $json_data_iphone = array(
                                            'badge'         => 0,
                                            'flag'          => 'exam_mark_added',
                                            'master_id'       => $examData->id,
                                            'msg'           => $msg,
                                        );
                                        send_notification_iphone($students[0]->device_token,$msg,$json_data_iphone,' - Exam');
                                    }

                                    $noti = new Notifications();

                                    $noti->type         = '2';
                                    $noti->master_id    = $examData->id;
                                    $noti->user_id      = $examStud->student_id;
                                    $noti->message      = $msg;
                                    $noti->save();
                                }
                            }
                            }else{
                                return response()->json(['status'=>2,'message'=>trans('common.message.obtained_small_total_marks')]);
                            }
                        }
                    }

                    $examStud->mark_fill_by   =  $loginUser->id;
                    $examStud->status         =  $input['attendance_status'][$key];
                    //  print_r($examStud);
                    $examStud->save();
                    //print_r($examStud);
                }
            }
            //die("exit");
            return redirect('exam')->with('success', trans('common.message.mark_update_success'));
        }
        else{
            return redirect('exam')->with('success', trans('common.label.no_record_found'));
        }
    }
        
    /**
     * Store a student exam marks
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function submitObtainMarks(Request $request) {

        $input      = $request->all();
        $loginUser  = Auth::user();
        $examStud   = ExamStudents::findOrNew($input['id']);

        $responseData       = ['status'=>0,'message'=>trans('common.message.something_went_wrong')];
        $responseMessage    = trans('common.message.something_went_wrong');

        if (!empty($examStud)) {

            $examData   = Exam::findOrNew($examStud->exam_id);
            //dd($examData);
            if($input['attendance_status'] == 3){
                $examStud->obtain_marks = 0;
            }elseif($input['attendance_status'] == 1){
                $examStud->obtain_marks = !empty($input['marks'])?$input['marks']:0;
                $examStud->percentage   = 0;

                //////// SEND NOTIFICATION ////////

                //DB::enableQueryLog();
                $students = Students::getAllExamStudents(
                                        [
                                            'subject_id'    => $examData->subject_id,
                                            'student_id'    => $examStud->student_id,
                                        ]
                                    );
                //dd(DB::getQueryLog());
                //dd($students);
                if(!empty($students)){

                    if($examData->exam_category_id == 1){
                        $cat = 'Weekly';
                    }else{
                        $cat = 'Midterm';
                    }

                    $msg = $cat." Exam of ".$students[0]->subject_name.", (chapter ".$examData->chapter.") was held on ".date('F j, Y',strtotime($examData->exam_date))." (".date('g:i a',strtotime($examData->exam_time)).') was not attended.';

                    
                    $noti = new Notifications();

                    $noti->type         = '2';
                    $noti->master_id    = $examData->id;
                    $noti->user_id      = $examStud->student_id;
                    $noti->message      = $msg;
                    $noti->save();


                    if(!empty($students[0]->device_token)){
                        if(!empty($students[0]->device_type == 1)){
                            $json_data_android = array(
                                'badge'         => 0,
                                'flag'          => 'exam_mark_added',
                                'master_id'       => $examData->id,
                                'msg'           => $msg,
                            );
                            send_notification_android($students[0]->device_token,$msg,$json_data_android,' - Exam','#97a84c');
                        }
                        elseif(!empty($students[0]->device_type == 2)){
                            $json_data_iphone = array(
                                'badge'         => 0,
                                'flag'          => 'exam_mark_added',
                                'master_id'       => $examData->id,
                                'msg'           => $msg,
                            );
                            send_notification_iphone($students[0]->device_token,$msg,$json_data_iphone,' - Exam');
                        }
                    }
                }
            }else{
                $examStud->obtain_marks         = $input['marks'];
                if($input['marks'] <= $input['total_marks']){
                    $examStud->percentage       = ($input['marks']*100)/$input['total_marks'];
                }else{
                    return response()->json(['status'=>2,'message'=>trans('common.message.obtained_small_total_marks')]);
                }
            }

            $examStud->mark_fill_by   =  $loginUser->id;
            $examStud->status         =  $input['attendance_status'];
            $examStud->save();

            $responseData['message']        = trans('common.message.mark_update_success');
            $responseData['status'] = 1;
        }
        return response()->json($responseData);
    }

    /**
     * Retrieve all subject list as per batch selection
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
    */
    public function getSubjectTeacher(Request $request){
        $subject_id = request('subject_id');
        $batch_id   = request('batch_id');

        if(!empty($subject_id)){
            //DB::enableQueryLog();
            $teachers    = TeacherBranchBatchSubject::getTeacherBranchBatchSubject(['batch_id' => $batch_id,'subject_id' => $subject_id]);
            //dd(DB::getQueryLog());
            //dd($teachers);  

            $teacherDropHtml = '<option value="">Select teacher</option>';
            foreach ($teachers as $key => $value) {
                $teacherDropHtml .= '<option value="'.$value->teacher_id.'" >'.$value->teacher_name.'</option>';
            }
            return ['flag'=>1,'teacherDropHtml'=>$teacherDropHtml];
            
        }else{
                return ['flag'=>0,'teacherDropHtml'=>''];
            }
    }
}
