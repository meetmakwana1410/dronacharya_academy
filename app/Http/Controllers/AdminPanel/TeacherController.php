<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\TeacherRequest;
use app\User;
use app\RoleUser;
use app\Models\Batch;
use app\Models\TeacherBatch;
use app\Models\Subjects;
use app\Models\TeacherSubjects;
use app\Models\TeacherBranchBatchSubject;
use app\Models\Homework;
use app\Models\HomeworkImages;
use Auth;
use DB;
use Storage;
use Hash;
use Illuminate\Support\Facades\View;

class TeacherController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'students';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'name',
                1 => 'email',
                2 => 'contact_no',
                3 => 'id',
            );
            $params = array(
                'name'          => $request->columns[0]['search']['value'],
                'email'         => $request->columns[1]['search']['value'],
                'contact_no'    => $request->columns[2]['search']['value'],
                'order_column'  => $columns[$request->order[0]['column']],
                'order_dir'     => $request->order[0]['dir'],
                'user_role'     => ROLE_TEACHER,
            );

            $results = User::getAllUser($request->length, $params);
            
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->name;
                    $tempArray[] = $result->email;
                    $tempArray[] = $result->contact_no;

                    $viewActionButton =  View::make('admin.teacher.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "daw" => intval($request->daw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.teacher.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $teacher    = [];
        $batch      = Batch::getAllBatch();
        $subjects   = Subjects::getAllSubjects();
        $branch     = User::getAllUser('', ['user_role'=> ROLE_BRANCH]);

        if($batch->isEmpty() || $subjects->isEmpty() || $branch->isEmpty()){
            return redirect('teacher')->with('error', trans('common.message.teacher_not_added'));
        }
        
        $teacherBBS = [];
        if (!empty($id)) {
            $teacher    = User::findOrNew($id);
            $teacherBBS =  TeacherBranchBatchSubject::where('teacher_id','=',$id)->get();
        }
        return view('admin.teacher.add_edit', compact('teacher','batch','subjects','branch','teacherBBS'));
    }

    
    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request, $id = null) {
        //dd($request->all());
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $teacher    = null;
        $loginUser  = Auth::user();
        if (!empty($id)) {
            /*TeacherBatch::where('teacher_id', '=', $id)->delete();
            TeacherSubjects::where('teacher_id', '=', $id)->delete();*/
            TeacherBranchBatchSubject::where('teacher_id', '=', $id)->delete();
            $teacher = User::findOrNew($id);
            $teacher->updated_by = $loginUser->id;
        }
        if (empty($teacher)) {
            $teacher = new User();
            $teacher->created_by = $loginUser->id;
            $teacher->created_at = date('Y-m-d H:i:s');
        }

        $teacher->name              = $request->request->get('name');
        $teacher->email             = strtolower(trim($request->request->get('email')));
        $teacher->contact_no        = $request->request->get('contact_no');

        $password = $request->request->get('password');
        if(!empty($password)){
            $teacher->password          = Hash::make($request->request->get('password'));
        }

        //dd($teacher);
        $teacher->save();
        
        if (empty($id)) {
            DB::table('role_users')->insert(
                ['role_id' => '3', 'user_id' =>$teacher->id]
            );
        }

        if(!empty($teacher->id)){

            $branch     = $request->request->get('branch');
            $batchId    = $request->request->get('batch_id');
            $subjectIds = $request->request->get('subject_id');

            if(!empty($branch)){
                foreach($branch as $key => $value) {
                    DB::table('teacher_branch_batch_subject')->insert(
                        [
                            'teacher_id'    => isset($teacher->id)?$teacher->id:'0',
                            'branch_id'     => isset($value)?$value:'0',
                            'subject_id'    => isset($subjectIds[$key])?$subjectIds[$key]:'0',
                            'batch_id'      => isset($batchId[$key])?$batchId[$key]:'0',
                        ]
                    );
                }
            }

            /*$subjectIds   = $request->request->get('subject_id');
            foreach ($subjectIds as $key => $value) {
                DB::table('teacher_subjects')->insert(
                    ['teacher_id' => $teacher->id, 'subject_id' => $value]
                );
            }*/
        }

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('teacher')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = User::findOrNew($id);
        if (!empty($data)) {
            RoleUser::where('user_id', '=', $id)->delete();
            TeacherBranchBatchSubject::where('teacher_id', '=', $id)->delete();
            $homework = Homework::where('created_by', '=', $id)->get();
            if(!empty($homework)){
                foreach ($homework as $key => $value) {
                    HomeworkImages::where('home_work_id', '=', $value->id)->delete();
                    Homework::where('created_by', '=', $value->id)->delete();
                }
            }
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    public function deleteRecord(Request $request) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $id              = $request->request->get('recordsId');
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = TeacherBranchBatchSubject::findOrNew($id);
        if (!empty($data)) {
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $user = User::findOrNew($id);
        if (!empty($user)) {
            if($user->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $user->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $user->status = 1;
            }
            $user->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
