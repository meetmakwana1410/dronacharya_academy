<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\AcademicYearRequest;
use app\Models\AcademicYear;
use Auth;
use Illuminate\Support\Facades\View;

class AcademicYearController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'product';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {
            $columns = array(
                0 => 'year',
                1 => 'id',
            );
            $params = array(
                'year'=> $request->columns[0]['search']['value'],
                'order_column' => $columns[$request->order[0]['column']],
                'order_dir' => $request->order[0]['dir'],
            );

            $results = AcademicYear::getAllAcademicYears($request->length, $params);
            //dd($results);
            $data = array();

            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->year;

                    $viewActionButton =  View::make('admin.academic_year.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.academic_year.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $academic_year = [];
        if (!empty($id)) {
            $params = array(
                'id'=> $id,
            );
            $academic_year   = AcademicYear::findOrNew($id);
            //dd($academic_year);
        }
        return view('admin.academic_year.add_edit', compact('academic_year'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(AcademicYearRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated      = $request->validated();
        $academicYear   = null;

        if (!empty($id)) {
            $academicYear = AcademicYear::findOrNew($id);
        }
        if (empty($academicYear)) {
            $academicYear = new AcademicYear();
            $academicYear->status = 1;
        }
       
        $academicYear->year                 = $request->request->get('year');
        $academicYear->save();

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('academic_year')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $product = AcademicYear::findOrNew($id);
        if (!empty($product)) {
            $product->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $product = AcademicYear::findOrNew($id);
        if (!empty($product)) {
            if($product->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $product->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $product->status = 1;
            }
            $product->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }
}
