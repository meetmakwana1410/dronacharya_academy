<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\BoardRequest;
use app\Models\Batch;
use app\Models\Inquiry;
use app\Models\Students;
use app\User;
use Auth;
use Illuminate\Support\Facades\View;

class DashboardController extends AdminCommonController {

    public function __construct() {

        parent::__construct();
        $this->foldername = 'board';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $loginUser = Auth::user();

        /**
         * Get all Users count.
         */
        $getAllUsersCount = User::join('role_users','role_users.user_id','=','users.id')
        ->join('roles','role_users.role_id','=','roles.id')
        ->where('role_users.role_id','=',3)
        ->count();

        if(empty($getAllUsersCount) && $getAllUsersCount < 0){
            $getAllUsersCount = 0;
        }

        /**
         * Get all Inquiry count.
        */
        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $getAllInquiry      = Inquiry::where('created_by','=',$loginUser->id)->count();
            $getConfirmInquiry  = Inquiry::where('status','=',1)->where('created_by','=',$loginUser->id)->count();
        }else{
            $getAllInquiry      = Inquiry::count();
            $getConfirmInquiry  = Inquiry::where('status','=',1)->count();
        }

        if(empty($getAllInquiry) && $getAllInquiry < 0){
            $getAllInquiry = 0;
        }

        /**
         * Get all Batch count.
        */
        $getAllBatch = Batch::where('status','=',1)->count();

        if(empty($getAllBatch) && $getAllBatch < 0){
            $getAllBatch = 0;
        }

        /**
         * Get all Batch count.
         */
        //$getAllStudents = Students::count();

        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $getAllStudents         = Students::where('branch_id','=',$loginUser->id)->count();
        }else{
            $getAllStudents         = Students::count();
        }

        if(empty($getAllStudents) && $getAllStudents < 0){
            $getAllStudents = 0;
        }

        return view('admin.dashboard.index',compact('getAllUsersCount','getAllInquiry','getAllBatch','getAllStudents','getConfirmInquiry'));
    }

}
