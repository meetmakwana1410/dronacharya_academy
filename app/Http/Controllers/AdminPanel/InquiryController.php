<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\InquiryRequest;
use app\Models\Inquiry;
use app\Models\School;
use app\Models\Standards;
use app\Models\InquiryFollowUp;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\View;

class InquiryController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'inquiries';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'name',
                1 => 'primary_mobile_number',
                2 => 'school_name',
                3 => 'standard',
                4 => 'school_type',
                5 => 'created_at',
                6 => 'id',
            );

            $createdBy = '';
            $loginUser = Auth::user();
            if(Auth::user()->hasRole(ROLE_BRANCH)){
                $createdBy   = $loginUser->id;
            }
            //dd($createdBy);
            //dd($request->columns[0]);
            $params = array(
                'name'                  => $request->columns[0]['search']['value'],
                'primary_mobile_number' => $request->columns[1]['search']['value'],
                'standard_school'       => $request->columns[2]['search']['value'],
                'created_at'            => $request->columns[3]['search']['value'],
                'created_by'            => $request->columns[4]['search']['value'],
                'order_column'          => $columns[$request->order[0]['column']],
                'order_dir'             => $request->order[0]['dir'],
                'login_user_id'         => $createdBy,
            );
            //dd($params);
            //DB::enableQueryLog();
            $results = Inquiry::getAllInquiry($request->length, $params);
            //dd(DB::getQueryLog());
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->name;
                    $tempArray[] = $result->primary_mobile_number;
                    $tempArray[] = $result->school_name.' / '.(($result->school_type == 1)?"GSEB":'CBSE').' / '.$result->standard; 
                    $tempArray[] = date('j F, Y',strtotime($result->created_at));
                    $tempArray[] = $result->created_by;

                    $viewActionButton =  View::make('admin.inquiry.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.inquiry.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $inquiry    = [];
        $school     = School::getAllSchool('',['is_status_check'=>'1']);
        $standards  = Standards::all();
        if (!empty($id)) {
            $inquiry               = Inquiry::findOrNew($id);
        }
        return view('admin.inquiry.add_edit', compact('inquiry','school','standards'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(InquiryRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $inquiry      = null;
        $loginUser = Auth::user();
        

        if (!empty($id)) {
            $inquiry = Inquiry::findOrNew($id);
            $inquiry->updated_by = $loginUser->id;
        }
        if (empty($inquiry)) {
            $inquiry = new Inquiry();
            $inquiry->created_by = $loginUser->id;
            $inquiry->status     = 1;
        }

        $inquiry->name                          = $request->request->get('name');
        $inquiry->primary_mobile_number         = $request->request->get('primary_mobile_number');
        $inquiry->secondary_mobile_number       = $request->request->get('secondary_mobile_number');
        $inquiry->student_mobile_number         = $request->request->get('student_mobile_number');
        $inquiry->parents_email                 = $request->request->get('parents_email');
        $inquiry->students_email                = $request->request->get('students_email');
        $inquiry->residence_address             = $request->request->get('residence_address');
        $inquiry->school_id                     = !empty($request->request->get('school_id'))?$request->request->get('school_id'):'';
        $inquiry->school_timings                = $request->request->get('school_timings');
        $inquiry->standard_id                   = $request->request->get('standard_id');
        $inquiry->school_type                   = $request->request->get('school_type');
        $inquiry->remark                        = $request->request->get('remark');
        $inquiry->save();


        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $standards  = Standards::where('id',$request->request->get('standard_id'))->get();
            //dd($standards);
            $school_type = $request->request->get('school_type');
            if($school_type == 1){
                $schoolType = 'GSEB';
            }else{
                $schoolType = 'CBSE';
            }
            $msg = 'Thank you for your enquiry about the '.$standards[0]['standard_name'].' '.$schoolType.' Course. From : DRONACHARYA ACADEMY';
            
            $sendsms = sendSms($request->request->get('primary_mobile_number'),$msg);
            //dd($sendsms);
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('inquiry')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Inquiry::findOrNew($id);
        if (!empty($data)) {
            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $product = Inquiry::findOrNew($id);
        if (!empty($product)) {
            if($product->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $product->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $product->status = 1;
            }
            $product->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the inquiry details
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null) {
        $inquiry    = [];
        if (!empty($id)) {
            $inquiry               = Inquiry::getAllInquiry('',['id'=>$id]);
            $inquiryFollowUp = InquiryFollowUp::where('inquiry_id',$id)->get();
        }
        //dd($inquiry);

        if(!empty($inquiry) && !$inquiry->isEmpty()){
            return view('admin.inquiry.view', compact('inquiry','inquiryFollowUp'));
        }else{
            return redirect('inquiry')->with('error', trans('common.label.no_record_found'));
        }
    }

    /**
     * Store a newly created inquiry followUp.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function submitInquiryFollowUp(Request $request) {
        //dd($request->all());

        $loginUser = Auth::user();
        
        $inquiry = new InquiryFollowUp();
        $msg = strip_tags(trim($request->request->get('send_message')));
        $inquiry->note           = $msg;
        $inquiry->created_by     = $loginUser->id;
        $inquiry->inquiry_id     = $request->request->get('id');
        $inquiry->save();

        $inqData = Inquiry::findOrNew($inquiry->inquiry_id);
        $sendsms = sendSms($inqData->primary_mobile_number,$msg);

        return redirect('inquiry/show/'.$request->request->get('id'))->with('success', trans('common.message.follow_up_added_successfully'));
    }
}
