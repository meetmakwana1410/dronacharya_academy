<?php

namespace app\Http\Controllers\AdminPanel;

use Illuminate\Http\Request;
use app\Http\Requests\BatchRequest;
use app\User;
use app\Models\Batch;
use app\Models\Subjects;
use app\Models\Board;
use app\Models\Medium;
use app\Models\AcademicYear;
use app\Models\Standards;
use app\Models\Branch;
use app\Models\Students;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\View;

class BatchController extends AdminCommonController {

    public function __construct() {
        parent::__construct();
        $this->foldername = 'batch';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if ($request->ajax()) {

            $columns = array(
                0 => 'batch_code',
                1 => 'branch_name',
                2 => 'board_name',
                3 => 'id',
            );
           
            $params = array(
                'batch_code'        => $request->columns[0]['search']['value'],
                'branch_name'        => $request->columns[1]['search']['value'],
                'order_column'      => $columns[$request->order[0]['column']],
                'order_dir'         => $request->order[0]['dir'],
            );
           
            $results = Batch::getAllBatch($request->length, $params);
            //dd($results);

            $data = array();
            if (!empty($results)) {
                $data = $results->getCollection()->transform(function ($result) use ($data) {
                    $tempArray = array();
                    $tempArray[] = $result->batch_code;
                    $tempArray[] = $result->branch_name;

                    $viewActionButton =  View::make('admin.batch.action_buttons', ['object'=>$result]);
                    $tempArray[] = $viewActionButton->render();
                    return $tempArray;
                });
            }

            $jsonData = array(
                "draw" => intval($request->draw), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal" => $results->total(), // Total number of records
                "recordsFiltered" => $results->total(),
                "data" => $data // Total data array
            );
            return response()->json($jsonData);
        }else{
            return view('admin.batch.index');
        }
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id = null) {
        $batch = [];
        
        $academic_year  = AcademicYear::all();
        $board          = Board::all();
        $medium         = Medium::all();
        $standards      = Standards::all();

        if (!empty($id)) {
            $batch      = Batch::findOrNew($id);
        }
        $branch = User::getAllUser('', ['user_role'=>ROLE_BRANCH]);
        return view('admin.batch.add_edit', compact('batch','standards','board','medium','academic_year','branch'));
    }

    /**
     * Store a newly created|specified resource in storage.
     *
     * @param  app\Http\Requests\  $request
     * @param  int|null  $id
     * @return \Illuminate\Http\Response
     */
    public function store(BatchRequest $request, $id = null) {
        // Retrieve the validated input data...
        $validated  = $request->validated();
        $batch      = null;
        $loginUser = Auth::user();
        $old_logo = ''; 

        if (!empty($id)) {
            $batch = Batch::findOrNew($id);
            $batch->updated_by = $loginUser->id;
            $old_logo = $batch->batch_image;
        }
        if (empty($batch)) {
            $batch = new Batch();
            $batch->created_by = $loginUser->id;
        }

        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $batch->branch_id           = $loginUser->id;
        }else{
            $batch->branch_id           = $request->request->get('branch_id');
        }

        $batch->batch_name          = $request->request->get('batch_name');
        $batch->board_id            = $request->request->get('board');
        $batch->standard_id         = $request->request->get('standard');
        $batch->academic_year_id    = $request->request->get('academic_year');
        $batch->medium_id           = $request->request->get('medium_id');

        if(!is_dir(public_path('uploads/').$this->foldername)){
            mkdir(public_path('uploads/').$this->foldername , 0777, true);
        }

        $logo = $request->input('image_name');
        //Upload logo
        if(!empty($logo))
        {
            if(!empty($logo) && !empty($old_logo))
            {   
                if(!empty($old_logo) && file_exists(public_path('uploads/').$this->foldername."/".$old_logo))
                { 
                    unlink(public_path('uploads/').$this->foldername."/".$old_logo);
                }
            }
            
            $images      = explode('#', substr($request->input('image_name'), 1));
            $data1       = explode(',',  $images[1]);

            $bgImgPath  = public_path('uploads/').$this->foldername."/" ;
            
            if(!empty($data1[0]))
            {   
                $exp = explode("/", $data1[0]);
                
                if(!empty($exp[1]))
                {
                    $extantion = explode(";", $exp[1]);
                }
            }
            
            if(!empty($extantion[0]))
            {
                $output_file    = $bgImgPath.time() . '.'.$extantion[0]; 
                $exp = explode("/",$output_file);
                $travalue_image = end($exp);

            }
            else
            {
                $output_file    = $bgImgPath.time() . '.jpeg';
                $exp = explode("/",$output_file);
                $travalue_image = end($exp);
            }
          
            $ifp    = fopen($output_file, "wb"); 
            if(!empty($data1[1]))
            {
                $fwrite = fwrite($ifp, base64_decode($data1[1])); 
                
                if ($fwrite === true)
                {
                    chmod($output_file, 644); 
                }
            }

            $batch->batch_image   = $travalue_image;
        }

        $logo_remove_flag     = $request->input('logo_remove_flag');
        if(!empty($logo_remove_flag) && $logo_remove_flag == '1')
        {   
            if(!empty($old_logo) && file_exists(public_path('uploads/').$this->foldername."/".$old_logo))
            { 
                unlink(public_path('uploads/').$this->foldername."/".$old_logo);
                $batch->batch_image  = '';
            }
        }

        $batch->save();

        if(!empty($batch->id)){
            $results = Batch::getAllBatch('',['id'=>$batch->id]);
            $code = batch_code($results);
            $batch->batch_code = $code;
            $batch->save();
        }
        

        if (!empty($id)) {
            $successMessage = trans('common.message.update_success_msg');
        } else {
            $successMessage = trans('common.message.create_success_msg');
        }
        return redirect('batch')->with('success', $successMessage);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // IMAGE DELTE,ALSO DELETE TRANS DATA
        $responseData       = ['status'=>0,'message'=>''];
        $responseMessage    = trans('common.message.something_went_wrong');
        $data = Batch::findOrNew($id);
        if (!empty($data)) {

            if(!empty($data->batch_image) && file_exists(public_path('uploads/').$this->foldername."/".$data->batch_image))
            { 
                unlink(public_path('uploads/').$this->foldername."/".$data->batch_image);
            }

            $data->delete();
            $responseMessage        = trans('common.message.deleted_sucessfully');
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Show the form for creating|Edit a new|specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function changeStatus($id){
        $responseData = ['status'=>0,'message'=>''];
        $product = Batch::findOrNew($id);
        if (!empty($product)) {
            if($product->status){
                $responseMessage = trans('common.message.deactivated_sucessfully');
                $product->status = 0;
            }else{
                $responseMessage = trans('common.message.activated_sucessfully');
                $product->status = 1;
            }
            $product->save();
            $responseData['status'] = 1;
            $responseData['id']     = $id;
        }
        $responseData['message'] = $responseMessage;
        return response()->json($responseData);
    }

    /**
     * Display a listing batch wise students.
     *
     * @return \Illuminate\Http\Response
    */
    public function getStudent($id = null) {
        $results = [];
        if($id != ''){
 
            //DB::enableQueryLog();
            $results = Students::getAllStudents('',['batch_id'=>$id,'order_column'=>'roll_number','order_dir'=>'ASC']);
            //dd(DB::getQueryLog());
            //dd($results);

            if($results->isNotEmpty()){
                return view('admin.batch.batch_students', compact('results'));
            }else{
                return view('admin.batch.batch_students', compact('results'));
            }
        }
        else{
            return redirect('exam')->with('success', trans('common.label.no_record_found'));
        }
    }
}
