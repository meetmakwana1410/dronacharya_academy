<?php

namespace app\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;

use app\User;
use Auth;

class CommonController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
}