<?php

namespace app\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct(){
    	$this->notificationColor = ['1'=>'#567BB8','2'=>'#A7B85D','3'=>'#b85d83','4'=>'#ba6554'];

    	$this->studnetImagePath 	= '/uploads/students/';
    	$this->subjectImagePath 	= '/uploads/subjects/';
    	$this->batchImagePath 		= '/uploads/batch/';
    	$this->homeWorkImagePath 	= '/uploads/homework/';
    	$this->usersImagePath 		= '/uploads/users/';
    	$this->documentsImagePath 	= '/uploads/documents/';
    }
}
