<?php

namespace app\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use app\User;
class RolesMiddleware
{
     /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }
    
    public function handle($request, Closure $next,$role)
    {
        $loginUser = Auth::user();
        $parentId = $loginUser->parent_id;
        $parentuser = null;
        //Check parent company is active or inactive, If inactive redirect to login page
        if(!empty($parentId)){
            $parentuser = User::find($parentId);
            if($parentuser->status == 0){
                Auth::logout();
                return redirect('login')->with('error',trans('common.message.you_are_deactivate_by_admin'));
            }
        }
            
        //dd($loginUser);
        //Check user's status active or inactive, If inactive redirect to login page
        if($loginUser->status == 0){
            Auth::logout();
            return redirect('login')->with('error',trans('common.message.you_are_deactivate_by_admin'));
        }
        
        if (!$this->auth->user()->hasRole($role)) {
            if ($this->auth->user()->hasRole(SUPER_ADMIN_ROLE) || $this->auth->user()->hasRole(ROLE_BRANCH)){
                return redirect('dashboard');
            }
        }
        return $next($request);
    }
}