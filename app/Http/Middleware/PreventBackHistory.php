<?php
/**
 * This is to handle all request of admin users
 * @author Kuldeep Vadaliya <kuldeep@topsinfosolutions.com>
 */
namespace app\Http\Middleware;

use Closure;

class PreventBackHistory {
  /**
   * Handle an incoming request. and clear all previous data on browser back button
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @author  Kuldeep Vadaliya
   * @date    08-09-2018
   * @return mixed
   */
  public function handle($request, Closure $next) {
    $response = $next($request);
    $response->headers->set('Cache-Control' , 'nocache, no-store, max-age=0, must-revalidate');
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Expires', 'Sun, 02 Jan 1990 00:00:00 GMT');
    return $response;
  }
}