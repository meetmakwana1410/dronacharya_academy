<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\Model\Subjects;

class StudentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if($request->id){
            return [
                'batch_id'              => 'required',
                'roll_number'           => 'required',
                'student_name'          => 'required',
                'student_date_of_birth' => 'required',
                'gender'               => 'required|in:1,2',
                'father_name'           => 'required',
                'father_mobile_number'  => 'required|max:10|unique:students,father_mobile_number,'.$request->id,
                'mother_mobile_number'  => 'max:10',
                'landline_number'       => 'max:15',
                'address'               => 'required',
            ];
        }else{
            return [
                'batch_id'              => 'required',
                'roll_number'           => 'required',
                'student_name'          => 'required',
                'student_date_of_birth' => 'required',
                'gender'                => 'required|in:1,2',
                'father_name'           => 'required',
                'father_mobile_number'  => 'required|max:10|unique:students',
                'mother_mobile_number'  => 'max:10',
                'landline_number'       => 'max:15',
                'address'               => 'required',
            ];
        }
    }
}