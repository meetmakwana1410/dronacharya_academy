<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\User;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if($request->id){
            return [
                'name'                  => 'required',
                'email'                 => 'required|max:100|unique:users,email,'.$request->id,
                'contact_no'            => 'required|max:12|unique:users,contact_no,'.$request->id,
            ];
        }else{
            return [
                'name'                  => 'required',
                'email'                 => 'required|max:100|unique:users',
                'contact_no'            => 'required|max:12|unique:users',
                'password'              => 'required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'required'
            ];
        }
    }
}