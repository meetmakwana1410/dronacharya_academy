<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\Model\Exam;

class ExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
            return [
                'batch_id'              => 'required',
                'exam_category_id'      => 'required',
                'subject_id'            => 'required',
                'total_marks'           => 'required',
                'exam_date'             => 'required',
                'exam_time'             => 'required',
                'to_exam_time'          => 'required',
            ];
    }
}