<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\Model\Medium;

class MediumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->id){
            return [
                'medium_name'  => 'required|max:50|unique:medium,medium_name,'.$request->id,
            ];
        }else{
            return [
                'medium_name'  =>'required|max:50|unique:medium',
            ];
        }
    }
}