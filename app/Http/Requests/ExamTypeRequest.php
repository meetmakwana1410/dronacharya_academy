<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\Model\ExamType;

class ExamTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->id){
            return [
                'exam_type_name'  => 'required|max:50|unique:exam_type,exam_type_name,'.$request->id,
            ];
        }else{
            return [
                'exam_type_name'  =>'required|max:50|unique:exam_type',
            ];
        }
    }
}