<?php

namespace app\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use app\Model\Board;

class BoardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->id){
            return [
                'board_name'  => 'required|max:50|unique:board,board_name,'.$request->id,
            ];
        }else{
            return [
                'board_name'  =>'required|max:50|unique:board',
            ];
        }
    }
}