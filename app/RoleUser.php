<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $fillable = array('user_id','role_id');
    /**
     * Get assigned user
     * @return type
     */
    public function user(){
        return $this->belongsTo('app\User');
    }
    
    /**
     * Get assigned role
     * @return Role
     */
    public function role(){
        return $this->belongsTo('app\Role');
    }
    
    /**
     * Attach user role if role not avilable
     * @param \app\User $user
     * @param String $roleName
     * @return boolean
     */
    public static function attachUserRole(User $user,$roleName){
        $userRoleDB = $user->userRole->all();
        $compareuserRoleDB = [];
        foreach ($userRoleDB as $ur) {
            $compareuserRoleDB[] = $ur->role_id;
        }
        $role = Role::where('role',$roleName)->first();
        if(!empty($role)){
            if (!in_array($role->id, $compareuserRoleDB)) {
                return $user->userRole()->saveMany([new self(['role_id'=>$role->id])]);
            }
        }
        return false;
    }
    
    /**
     * 
     * @param \app\User $user
     * @param String $roleName
     * @return boolean
     */
    public static function removeUserRole(User $user,$roleName){
        $role = Role::where('role',$roleName)->first();
        if(!empty($role)){
            return self::where('role_id',$role->id)->where('user_id',$user->id)->delete();
        }
        return false;
    }
}
