<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notifications extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'notifications';

    protected $hidden = ['created_at','updated_at','created_by','updated_by'];


    /**
     * Display a listing of the notifications.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllNotifications($perPage = '',$params=array(),$isCount=0){
        $notiObj = self::select(['notifications.*']);

        if(!empty($params['id'])){
            $notiObj->where('id','=',$params['id']);
        }

        if(!empty($params['user_id'])){
            $notiObj->where('user_id','=',$params['user_id']);
        }

        if(!empty($params['user_type'])){
            $notiObj->where('user_type','=',$params['user_type']);
        }
      
        $notiObj->orderBy('id', 'DESC');
       
        if(!isset($perPage)){
            return $notiObj->get();
        }elseif($isCount == 1){
            return $notiObj->count();
        }
        else{
            $notiObj->skip($perPage)->take(PAGINATION_LIMIT);
            return $notiObj->get();
        }
    }

    
}