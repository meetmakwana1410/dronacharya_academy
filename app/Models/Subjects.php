<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Subjects extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'subjects';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllSubjects($perPage = '',$params=array()){

        $subjectsObj = self::select(['subjects.id','subjects.subject_name','subjects.subject_image',DB::RAW('GROUP_CONCAT(" ",standards.standard_name) as standards')]);

        if(!empty($params['subject_name'])){
            $subjectsObj->where('subjects.subject_name','like',"%".$params['subject_name']."%");
        }

        if(!empty($params['standards'])){
            $subjectsObj->where('standards.standard_name','like',"%".$params['subject_name']."%");
        }

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $subjectsObj->where('subjects.status','1');
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $subjectsObj->where('subjects.id','=',$params['subject_id']);
        }


        $subjectsObj->leftjoin('standard_subjects','subjects.id','standard_subjects.subject_id');
        $subjectsObj->leftjoin('standards','standards.id','standard_subjects.standard_id');

        $subjectsObj->groupBy('subjects.id');

        $subjectsObj->orderBy(!empty($params['order_column'])?$params['order_column']:'subjects.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $subjectsObj->get();
        }else{
           return $subjectsObj->paginate($perPage);
        }
    }

    /**
     * Display a listing of the students subjects.
     *
     * @param Int $student 
     * @return \Illuminate\Http\Response
     */
    public static function getStudentsSubjects($params=array()){
        $standardsObj = self::select(['subjects.id','subjects.subject_name']);
        if(!empty($params['student_id'])){
            $standardsObj->where('student_subjects.students_id','=',$params['student_id']);
        }
        $standardsObj->leftjoin('student_subjects','student_subjects.subject_id','subjects.id');
        return $standardsObj->get();
    }
}