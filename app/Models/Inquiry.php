<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Inquiry extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'inquiries';

    /**
     * Display a listing of the inquiries.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllInquiry($perPage = '',$params=array()){

        $queryObj = self::select(['inquiries.*','standards.standard_name as standard','school.school_name','users.name as created_by']);

        if(isset($params['batch_name']) && !empty($params['batch_name'])){
            $queryObj->where('batch.batch_name','like',"%".$params['batch_name']."%");
        }

        if(isset($params['school_name']) && !empty($params['school_name'])){
            $queryObj->where('school.school_name','like',"%".$params['school_name']."%");
        }

        if(isset($params['created_by']) && !empty($params['created_by'])){
            $queryObj->where('users.name','like',"%".$params['created_by']."%");
        }

        if(isset($params['primary_mobile_number']) && !empty($params['primary_mobile_number'])){
            $queryObj->where('inquiries.primary_mobile_number','like',"%".$params['primary_mobile_number']."%");
        }
        
        if(isset($params['name']) && !empty($params['name'])){
            $queryObj->where('inquiries.name','like',"%".$params['name']."%");
        }

        if(isset($params['standard']) && !empty($params['standard'])){
            $queryObj->where('standards.standard_name','like',"%".$params['standard']."%");
        }

        if(isset($params['standard_school']) && !empty($params['standard_school'])){
            $queryObj->where('standards.standard_name','like',"%".$params['standard_school']."%")->orWhere('school.school_name','like',"%".$params['standard_school']."%");
        }

        if(isset($params['batch_name']) && !empty($params['batch_name'])){
            $queryObj->where('batch.batch_name','like',"%".$params['batch_name']."%");
        }

        if(!empty($params['id'])){
            $queryObj->where('inquiries.id','=',$params['id']);
        }

        if(!empty($params['login_user_id'])){
            $queryObj->where('inquiries.created_by','=',$params['login_user_id']);
        }

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $queryObj->where('inquiries.status','1');
        }

        $queryObj->leftjoin('standards','inquiries.standard_id','standards.id');
        $queryObj->leftjoin('school','inquiries.school_id','school.id');
        $queryObj->leftjoin('users','users.id','inquiries.created_by');

        $queryObj->orderBy(!empty($params['order_column'])?$params['order_column']:'inquiries.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }
}