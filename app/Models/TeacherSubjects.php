<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TeacherSubjects extends Model
{   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'teacher_subjects';

    public static function getAllTeacherSubject($params=array()){
        $teacherSubjectsObj = self::select(['teacher_subjects.*','subjects.subject_name']);
        $teacherSubjectsObj->leftjoin('subjects','subjects.id','teacher_subjects.subject_id');
        $teacherSubjectsObj->where('teacher_subjects.teacher_id',$params['teacher_id']);
        $teacherSubjectsObj->orderBy('teacher_subjects.id','DESC');
        return $teacherSubjectsObj->get();
    }
}