<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class InquiryFollowUp extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'inquiry_follow_up';
}