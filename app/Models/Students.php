<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Students extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'students';
    
    protected $hidden = ['device_type','device_token','created_at','updated_at','created_by','updated_by','status'];

    /**
     * Display a listing of the students.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllStudents($perPage = '',$params=array()){
       
        $queryObj = self::select(['students.*','batch.batch_name as batch_code','standards.standard_name','academic_year.year','board.board_name','medium.medium_name','users.name as branch_name','school.school_name']);

        if(!empty($params['roll_number'])){
            $queryObj->where('roll_number','like',"%".$params['roll_number']."%");
        }

        if(!empty($params['student_name'])){
            $queryObj->where('student_name','like',"%".$params['student_name']."%");
        }

        if(!empty($params['student_mobile_number'])){
            $queryObj->where('student_mobile_number','like',"%".$params['student_mobile_number']."%");
        }

        if(!empty($params['father_name'])){
            $queryObj->where('father_name','like',"%".$params['father_name']."%");
        }
        
        if(!empty($params['father_mobile_number'])){
            $queryObj->where('father_mobile_number','like',"%".$params['father_mobile_number']."%");
        }

        if(!empty($params['batch_code'])){
            if(isset($params['student_branch_id']) && !empty($params['student_branch_id'])){
                $queryObj->where('batch.branch_id','=',$params['student_branch_id'])->where('batch.batch_name','like',"%".$params['batch_code']."%")->orWhere('users.name','like',"%".$params['batch_code']."%");
            }else{
                $queryObj->where('batch.batch_name','like',"%".$params['batch_code']."%")->orWhere('users.name','like',"%".$params['batch_code']."%");
            }
        }

        if(!empty($params['batch_id'])){
            $queryObj->where('students.batch_id','like',"%".$params['batch_id']."%");
        }

        if(!empty($params['id'])){
            $queryObj->where('students.id','=',$params['id']);
        }

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $queryObj->where('students.status','1');
        }
        
        if(!empty($params['branch_user_id'])){
            $queryObj->where('students.created_by','=',$params['branch_user_id']);
        }

        if(!empty($params['student_branch_id'])){
            $queryObj->where('students.branch_id','=',$params['student_branch_id']);
        }

        if(isset($params['device_token_check']) && $params['device_token_check'] == 1){
            $queryObj->where('students.device_token','!=','');
        }

        $queryObj->leftjoin('batch','batch.id','students.batch_id');
        $queryObj->leftjoin('standards','batch.standard_id','standards.id');
        $queryObj->leftjoin('academic_year','batch.academic_year_id','academic_year.id');
        $queryObj->leftjoin('board','batch.board_id','board.id');
        $queryObj->leftjoin('medium','batch.medium_id','medium.id');
        $queryObj->leftjoin('users','users.id','batch.branch_id');
        $queryObj->leftjoin('school','students.school_id','school.id');

        
        $queryObj->orderBy(!empty($params['order_column'])?$params['order_column']:'students.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }

    public static function getAllExamStudents($params=array()){
        $examStandardsObj = self::select(['students.id','students.student_name','students.device_type','students.device_token','student_subjects.students_id','subject_name']);

        if(isset($params['batch_id']) && !empty($params['batch_id'])){
            $examStandardsObj->where('students.batch_id','=',$params['batch_id']);
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $examStandardsObj->where('student_subjects.subject_id','=',$params['subject_id']);
        }

        if(isset($params['student_id']) && !empty($params['student_id'])){
            $examStandardsObj->where('students.id','=',$params['student_id']);
        }
        //$examStandardsObj->where('device_token','!=','');

        $examStandardsObj->leftjoin('student_subjects','students.id','student_subjects.students_id');
        $examStandardsObj->leftjoin('subjects','subjects.id','student_subjects.subject_id');

        return $examStandardsObj->get();
    }


    public static function getAllHomeworkStudents($perPage = '',$params=array(),$isCount=0){
        $howeworkStudentsObj = self::select(['students.id','students.batch_id','students.student_name','home_work.id as homework_id','home_work.subject_id','home_work.document','subjects.subject_name','subjects.subject_image','home_work.submission_date','home_work.created_at','home_work.note','home_work.created_by AS teacher_id','users.name AS teacher_name',DB::RAW('GROUP_CONCAT(home_work_images.image) as images'),DB::RAW('GROUP_CONCAT(home_work_images.id) as images_id')]);

        $howeworkStudentsObj->leftjoin('home_work','home_work.batch_id','students.batch_id');
        $howeworkStudentsObj->leftjoin('home_work_images','home_work.id','home_work_images.home_work_id');
        $howeworkStudentsObj->leftjoin('subjects','subjects.id','home_work.subject_id');
        $howeworkStudentsObj->leftjoin('users','home_work.created_by','users.id');

        if(isset($params['search_text']) && !empty($params['search_text'])){
            $howeworkStudentsObj->where('home_work.note','like',"%".$params['search_text']."%")
            ->orWhere('subjects.subject_name','like',"%".$params['search_text']."%")
            ->orWhere('users.name','like',"%".$params['search_text']."%");
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $howeworkStudentsObj->where('home_work.subject_id','=',$params['subject_id']);
        }

        if(isset($params['teacher_id']) && !empty($params['teacher_id'])){
            $howeworkStudentsObj->where('home_work.created_by','=',$params['teacher_id']);
        }

        if(isset($params['suggestion_date']) && !empty($params['suggestion_date'])){
            $howeworkStudentsObj->where('home_work.submission_date','=',"'".$params['suggestion_date']."'");
        }


        if(isset($params['date_between_week']) && !empty($params['date_between_week'])){
            $howeworkStudentsObj->whereBetween('home_work.submission_date',[date(
            'Y-m-d'), date( "Y-m-d", strtotime( date("Y-m-d")." +7 day" ))]);
        }
        $howeworkStudentsObj->where('students.id','=',$params['student_id']);
        $howeworkStudentsObj->whereNotNull('home_work.id');
        
        $howeworkStudentsObj->groupBy('home_work.id');

        $howeworkStudentsObj->orderBy('home_work.id','DESC');

        if(!isset($perPage)){
            return $howeworkStudentsObj->get();
        }elseif($isCount == 1){
            return $howeworkStudentsObj->count();
        }
        else{
            $howeworkStudentsObj->skip($perPage)->take(PAGINATION_LIMIT);
            return $howeworkStudentsObj->get();
        }
    }
    
}