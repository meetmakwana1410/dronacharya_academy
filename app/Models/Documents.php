<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Documents extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'documents';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllDocuments($perPage = '',$params=array()){
        $documentsObj = self::select(['documents.*','batch.batch_name as batch_code','subjects.subject_name']);
        
     
        if(!empty($params['id'])){
            $documentsObj->where('documents.id',$params['id']);
        }

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $documentsObj->where('documents.status','1');
        }

        if(isset($params['batch_code']) && !empty($params['batch_code'])){
            $documentsObj->where('batch.batch_code','1');
        }

        if(isset($params['batch_id']) && !empty($params['batch_id'])){
            $documentsObj->where('documents.batch_id',$params['batch_id']);
        }

        $documentsObj->leftjoin('batch','batch.id','documents.batch_id');
        $documentsObj->leftjoin('subjects','subjects.id','documents.subject_id');
      
        $documentsObj->orderBy(!empty($params['order_column'])?$params['order_column']:'documents.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $documentsObj->get();
        }else{
           return $documentsObj->paginate($perPage);
        }
    }
}