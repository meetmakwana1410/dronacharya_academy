<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Homework extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'home_work';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by','updated_by','status', 'updated_at',
    ];

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllHomework($perPage = '',$params=array()){
        //dd($params['teacher_id']);
        $HomeworkObj = self::select(['home_work.id','home_work.batch_id','home_work.subject_id','home_work.submission_date','home_work.document','home_work.note','subjects.id as subjects_id','subjects.subject_name','subjects.subject_image','batch.batch_name as batch_code',DB::RAW('GROUP_CONCAT(home_work_images.image) as images'),DB::RAW('GROUP_CONCAT(home_work_images.id) as images_id')]);

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $HomeworkObj->where('subjects.status','1');
        }

        if(isset($params['teacher_id']) && !empty($params['teacher_id'])){
            $HomeworkObj->where('home_work.created_by',$params['teacher_id']);
        }
        
        if(isset($params['home_work_id']) && !empty($params['home_work_id'])){
            $HomeworkObj->where('home_work.id',$params['home_work_id']);
        }

        if(isset($params['batch_id']) && !empty($params['batch_id'])){
            $HomeworkObj->where('home_work.batch_id',$params['batch_id']);
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $HomeworkObj->where('home_work.subject_id',$params['subject_id']);
        }

        if(isset($params['batch_code']) && !empty($params['batch_code'])){
            $HomeworkObj->where('home_work.batch_name',$params['batch_code']);
        }

        $HomeworkObj->leftjoin('subjects','home_work.subject_id','subjects.id');
        $HomeworkObj->leftjoin('batch','batch.id','home_work.batch_id');
        $HomeworkObj->leftjoin('home_work_images','home_work_images.home_work_id','home_work.id');
        $HomeworkObj->groupby('home_work.id');
        

        $HomeworkObj->orderBy(!empty($params['order_column'])?$params['order_column']:'home_work.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $HomeworkObj->get();
        }else{
           return $HomeworkObj->paginate($perPage);
        }
    }

    /**
     * Display a listing of the students subjects.
     *
     * @param Int $student 
     * @return \Illuminate\Http\Response
     */
    public static function getStudentsSubjects($params=array()){
        $standardsObj = self::select(['subjects.id','subjects.subject_name']);
        if(!empty($params['student_id'])){
            $standardsObj->where('student_subjects.students_id','=',$params['student_id']);
        }
        $standardsObj->leftjoin('student_subjects','student_subjects.subject_id','subjects.id');
        return $standardsObj->get();
    }
}