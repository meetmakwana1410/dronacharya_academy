<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Exam extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'exam';
    
    protected $hidden = ['created_at','updated_at','created_by','updated_by','status'];

    /**
     * Display a listing of the students.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllExam($perPage = '',$params=array()){
        $queryObj = self::select(['exam.*','batch.batch_name as batch_code','subjects.subject_name','batch.batch_name','subjects.subject_image','exam_type.exam_type_name']);

        
        if(!empty($params['subject_name'])){
            $queryObj->where('subjects.subject_name','like',"%".$params['subject_name']."%");
        }

        if(!empty($params['batch_code'])){
            $queryObj->where('batch.batch_code','like',"%".$params['batch_code']."%");
        }

        if(!empty($params['total_marks'])){
            $queryObj->where('exam.total_marks','like',"%".$params['total_marks']."%");
        }

        if(!empty($params['id'])){
            $queryObj->where('exam.id','=',$params['id']);
        }

        if(!empty($params['teacher_id'])){
            $queryObj->where('exam.teacher_id','=',$params['teacher_id']);
        }

        if(!empty($params['brach_id'])){
            $queryObj->where('exam.branch_id','=',$params['brach_id']);
        }

        if(!empty($params['created_by'])){
            $queryObj->where('exam.created_by','=',$params['created_by']);
        }

        if(!empty($params['subject_id'])){
            $queryObj->where('exam.subject_id','=',$params['subject_id']);
        }

        if(!empty($params['batch_id'])){
            $queryObj->where('exam.batch_id','=',$params['batch_id']);
        }

        $queryObj->leftjoin('batch','batch.id','exam.batch_id');
        $queryObj->leftjoin('subjects','subjects.id','exam.subject_id');
        $queryObj->leftjoin('users','users.id','exam.teacher_id');
        $queryObj->leftjoin('exam_type','exam_type.id','exam.exam_category_id');

        $queryObj->orderBy(!empty($params['order_column'])?$params['order_column']:'exam.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }

}