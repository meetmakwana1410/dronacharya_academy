<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Board extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'board';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllBoard($perPage = '',$params=array()){
        $boardObj = self::select(['board.id','board.board_name','board.full_name','board.status']);
        
        if(!empty($params['board_name'])){
            $boardObj->where('board_name','like',"%".$params['board_name']."%");
        }
        if(!empty($params['full_name'])){
            $boardObj->where('full_name','like',"%".$params['full_name']."%");
        }
       
        if(!empty($params['id'])){
            $boardObj->where('board.id',$params['id']);
        }


        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $boardObj->where('board.status','1');
        }
      
        $boardObj->orderBy(!empty($params['order_column'])?$params['order_column']:'board.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $boardObj->get();
        }else{
           return $boardObj->paginate($perPage);
        }
    }
}