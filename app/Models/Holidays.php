<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Holidays extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'holidays';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];
}