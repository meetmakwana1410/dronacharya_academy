<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Standards extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'standards';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllStandards($perPage = '',$params=array()){
        $standardsObj = self::select(['standards.id','standards.standard_name',DB::RAW('GROUP_CONCAT(" ",subjects.subject_name) as subject_name')]);

        if(!empty($params['standard_name'])){
            $standardsObj->where('standard_name','like',"%".$params['standard_name']."%");
        }

        if(!empty($params['subject_name'])){
            $standardsObj->where('subjects.subject_name','like',"%".$params['subject_name']."%");
        }

        $standardsObj->leftjoin('standard_subjects','standards.id','standard_subjects.standard_id');
        $standardsObj->leftjoin('subjects','subjects.id','standard_subjects.subject_id');

        $standardsObj->groupBy('standards.id');
        $standardsObj->orderBy(!empty($params['order_column'])?$params['order_column']:'standards.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $standardsObj->get();
        }else{
           return $standardsObj->paginate($perPage);
        }
    }
}