<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TeacherBranchBatchSubject extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'teacher_branch_batch_subject';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    

    public static function getTeacherBranchBatchSubject($params=array(),$groupBy = ''){
        $queryObj = self::select(['teacher_branch_batch_subject.*','batch.batch_code','users.name as branch_name','subjects.subject_name','teacherUser.name as teacher_name']);
        $queryObj->leftjoin('users','users.id','teacher_branch_batch_subject.branch_id');
        $queryObj->leftjoin('batch','batch.id','teacher_branch_batch_subject.batch_id');
        $queryObj->leftjoin('subjects','subjects.id','teacher_branch_batch_subject.subject_id');
        $queryObj->leftjoin('users as teacherUser','teacherUser.id','teacher_branch_batch_subject.teacher_id');

        if(isset($params['teacher_id']) && !empty($params['teacher_id'])){
            $queryObj->where('teacher_branch_batch_subject.teacher_id',$params['teacher_id']);
        }

        if(isset($params['batch_id']) && !empty($params['batch_id'])){
            $queryObj->where('teacher_branch_batch_subject.batch_id',$params['batch_id']);
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $queryObj->where('teacher_branch_batch_subject.subject_id',$params['subject_id']);
        }

        if(!empty($groupBy)){
            $queryObj->groupby($groupBy);
        }
        $queryObj->orderBy('teacher_branch_batch_subject.id','DESC');
        return $queryObj->get();
    }

    public static function getTeacherExam($params=array(),$groupBy = ''){
        $queryObj = self::select(['exam.*','subjects.subject_name','batch.batch_name','subjects.subject_image','batch.batch_code','users.name as branch_name','exam_type.exam_type_name']);
        
        $queryObj
            ->join('exam', function($join){
                $join->on('exam.batch_id','teacher_branch_batch_subject.batch_id');
                $join->on('exam.subject_id','teacher_branch_batch_subject.subject_id');
            });
        $queryObj->leftjoin('users','teacher_branch_batch_subject.branch_id','users.id');
        $queryObj->leftjoin('batch','batch.id','exam.batch_id');
        //$queryObj->leftjoin('users','users.id','exam.teacher_id');
        $queryObj->leftjoin('subjects','subjects.id','teacher_branch_batch_subject.subject_id');
        $queryObj->leftjoin('exam_type','exam_type.id','exam.exam_category_id');
        
        if(isset($params['teacher_id']) && !empty($params['teacher_id'])){
            $queryObj->where('teacher_branch_batch_subject.teacher_id',$params['teacher_id']);
        }

        if(isset($params['batch_id']) && !empty($params['batch_id'])){
            $queryObj->where('teacher_branch_batch_subject.batch_id',$params['batch_id']);
        }

        if(isset($params['subject_id']) && !empty($params['subject_id'])){
            $queryObj->where('teacher_branch_batch_subject.subject_id',$params['subject_id']);
        }

        if(!empty($groupBy)){
            $queryObj->groupby($groupBy);
        }
        $queryObj->orderBy('teacher_branch_batch_subject.id','DESC');
        return $queryObj->get();
    }
}