<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ExamType extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'exam_type';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllExamType($perPage = '',$params=array()){
        $examTypeObj = self::select(['exam_type.id','exam_type.exam_type_name']);
        if(!empty($params['exam_type_name'])){
            $examTypeObj->where('exam_type_name','like',"%".$params['exam_type_name']."%");
        }
       
        if(!empty($params['id'])){
            $examTypeObj->where('exam_type.id',$params['id']);
        }


      
        $examTypeObj->orderBy(!empty($params['order_column'])?$params['order_column']:'exam_type.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $examTypeObj->get();
        }else{
           return $examTypeObj->paginate($perPage);
        }
    }
}