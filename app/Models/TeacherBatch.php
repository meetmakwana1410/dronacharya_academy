<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TeacherBatch extends Model
{   
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'teacher_batch';

    public static function getAllTeacherBatch($params=array()){
        $teacherBatchObj = self::select(['teacher_batch.*','batch.batch_code']);
        $teacherBatchObj->leftjoin('batch','batch.id','teacher_batch.batch_id');

        $teacherBatchObj->where('teacher_batch.teacher_id',$params['teacher_id']);
        $teacherBatchObj->orderBy('teacher_batch.id','DESC');
        return $teacherBatchObj->get();
    }
}