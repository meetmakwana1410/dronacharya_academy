<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Medium extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'medium';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllmedium($perPage = '',$params=array()){
        $mediumObj = self::select(['medium.id','medium.medium_name','medium.status']);
        if(!empty($params['medium_name'])){
            $mediumObj->where('medium_name','like',"%".$params['medium_name']."%");
        }
       
        if(!empty($params['id'])){
            $mediumObj->where('medium.id',$params['id']);
        }


        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $mediumObj->where('medium.status','1');
        }
      
        $mediumObj->orderBy(!empty($params['order_column'])?$params['order_column']:'medium.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $mediumObj->get();
        }else{
           return $mediumObj->paginate($perPage);
        }
    }
}