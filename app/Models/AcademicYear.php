<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AcademicYear extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'academic_year';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllAcademicYears($perPage = '',$params=array()){
        $academicYearObj = self::select(['academic_year.id','academic_year.year','academic_year.status']);
        if(!empty($params['year'])){
            $academicYearObj->where('year','like',"%".$params['year']."%");
        }
       
        if(!empty($params['id'])){
            $academicYearObj->where('academic_year.id',$params['id']);
        }


        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $academicYearObj->where('academic_year.status','1');
        }
      
        $academicYearObj->orderBy(!empty($params['order_column'])?$params['order_column']:'academic_year.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $academicYearObj->get();
        }else{
           return $academicYearObj->paginate($perPage);
        }
    }
}