<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class School extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'school';

    /**
     * Display a listing of the products.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllSchool($perPage = '',$params=array()){
        $returnObj = self::select(['school.id','school.school_name','school.status']);
        if(!empty($params['school_name'])){
            $returnObj->where('school_name','like',"%".$params['school_name']."%");
        }
       
        if(!empty($params['id'])){
            $returnObj->where('school.id',$params['id']);
        }


        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $returnObj->where('school.status','1');
        }
      
        $returnObj->orderBy(!empty($params['order_column'])?$params['order_column']:'school.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $returnObj->get();
        }else{
           return $returnObj->paginate($perPage);
        }
    }
}