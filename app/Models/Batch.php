<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Batch extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'batch';

    /**
     * Display a listing of the batch.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllBatch($perPage = '',$params=array()){

        $queryObj = self::select(['batch.id','batch.batch_name','batch.board_id','batch.standard_id','batch.academic_year_id','batch.medium_id','batch.status','board.board_name','standards.standard_name','medium.medium_name','academic_year.year','batch.batch_code',DB::RAW('CONCAT_WS("_",academic_year.year,board.board_name,medium.medium_name,standards.standard_name,batch.batch_name,users.name) as batch_code_old'),'users.name as branch_name']);

        if(!empty($params['batch_name'])){
            $queryObj->where('batch.batch_name','like',"%".$params['batch_name']."%");
        }

        if(!empty($params['board_name'])){
            $queryObj->where('board.board_name','like',"%".$params['board_name']."%");
        }
        
        if(!empty($params['standard_name'])){
            $queryObj->where('standards.standard_name','like',"%".$params['standard_name']."%");
        }
        if(!empty($params['year'])){
            $queryObj->where('academic_year.year','like',"%".$params['year']."%");
        }

        if(!empty($params['medium_name'])){
            $queryObj->where('medium.medium_name','like',"%".$params['medium_name']."%");
        }

        if(!empty($params['id'])){
            $queryObj->where('batch.id','=',$params['id']);
        }

        if(!empty($params['batch_created_by'])){
            $queryObj->where('batch.branch_id','=',$params['batch_created_by']);
        }

        if(!empty($params['branch_name'])){
            $queryObj->where('users.name','like',"%".$params['branch_name']."%");
        }

        if(!empty($params['batch_code'])){
            $queryObj->where('batch.batch_code','like',"%".$params['batch_code']."%");
        }

        if(isset($params['is_status_check']) && $params['is_status_check']  == 1){
            $queryObj->where('batch.status','1');
        }
        
        if(Auth::user()->hasRole(ROLE_BRANCH)){
            $queryObj->where('batch.branch_id','=',Auth::user()->id);
        }

        $queryObj->leftjoin('board','board.id','batch.board_id');
        $queryObj->leftjoin('standards','standards.id','batch.standard_id');
        $queryObj->leftjoin('academic_year','academic_year.id','batch.academic_year_id');
        $queryObj->leftjoin('medium','medium.id','batch.medium_id');
        $queryObj->leftjoin('users','users.id','batch.branch_id');
      
        $queryObj->orderBy(!empty($params['order_column'])?$params['order_column']:'batch.id', !empty($params['order_dir'])?$params['order_dir']:'DESC');
        if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }

    /**
     * Display a listing of the bath subject.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getBatchSubjects($batchId = ''){

        $queryObj = self::select(['batch.id','batch.standard_id','standard_subjects.subject_id','subjects.subject_name']);

        $queryObj->where('subjects.status','1');

        if(!empty($batchId)){
            $queryObj->where('batch.id',$batchId);
        }

        $queryObj->leftjoin('standard_subjects','standard_subjects.standard_id','batch.standard_id');
        $queryObj->leftjoin('subjects','standard_subjects.subject_id','subjects.id');
        
        if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }
}