<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ExamStudents extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'exam_students';
    
    protected $hidden = ['updated_at'];

    /**
     * Display a listing of the students.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getExamStudents($param = []){
        
        $fields = ['exam_students.*','students.roll_number','students.student_name','exam.exam_category_id','exam.total_marks','exam.exam_date','exam.exam_time','exam.to_exam_time','exam.exam_duration','exam.chapter','batch.batch_code','batch.batch_name','subjects.id as subject_id','subjects.subject_name','subjects.subject_image','board.board_name','standards.standard_name','exam_type.exam_type_name'];

        $maxfields = [];
        if(isset($param['is_group_by']) && $param['is_group_by'] == '1'){
            $maxfields = [DB::RAW('MAX(exam_students.obtain_marks) as max_marks')];
        }

        $queryObj = self::select(array_merge($fields,$maxfields));

        if(isset($param['exam_id']) && !empty($param['exam_id'])){
            $queryObj->where('exam_students.exam_id','=',$param['exam_id']);
        }

        if(isset($param['student_id']) && !empty($param['student_id'])){
            $queryObj->where('exam_students.student_id','=',$param['student_id']);
        }

        if(isset($param['tab_type']) && !empty($param['tab_type']) && $param['tab_type'] == 2){
            $queryObj->where('exam.exam_date','>=',date('Y-m-d'));
        }
        
        $queryObj->leftjoin('exam','exam.id','exam_students.exam_id');
        $queryObj->leftjoin('students','students.id','exam_students.student_id');
        $queryObj->leftjoin('batch','batch.id','exam.batch_id');
        $queryObj->leftjoin('subjects','subjects.id','exam.subject_id');
        $queryObj->leftjoin('board','board.id','batch.board_id');
        $queryObj->leftjoin('standards','standards.id','batch.standard_id');
        $queryObj->leftjoin('exam_type','exam_type.id','exam.exam_category_id');
        
        if(isset($param['is_group_by']) && $param['is_group_by'] == '1'){
            $queryObj->groupby('exam.id');
        }

        if(isset($param['is_group_by']) && $param['is_group_by'] == '1'){
            $queryObj->groupby('exam_students.student_id');
        }

        if(isset($param['is_order_by']) && $param['is_order_by'] == '1'){
            $queryObj->orderBy('students.roll_number', 'ASC');
        }else{
            $queryObj->orderBy('exam.exam_date', 'DESC');
        }
        
        return $queryObj->get();
    }    
}