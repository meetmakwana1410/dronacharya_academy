<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class HomeworkImages extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'home_work_images';
}