<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StudentAttendance extends Model
{   
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'student_attendance';
}