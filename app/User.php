<?php

namespace app;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use app\Notifications\ResetPasswordNotification;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    CONST ROLE_TEACHER  = 'teacher';
    CONST ROLE_BRANCH   = 'branch-admin';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','updated_at','created_by','updated_by','created_at',
    ];
    
    /**
     * 
     * @param type $value
     * @return type
     */
    /*public function getCompanyNameAttribute($value){
        return ucwords(strtolower($value));
    }*/
    
    /**
     * Get all User Roles
     * @return Role
     */
    public function roles(){
        return $this->belongsToMany('app\Role','role_users');
    }

    /**
     * Get All user role name
     * @return string|Mixed
     */
    public function getRole(){
        $userRoles = $this->roles()->get();
        $assigneRoleName = [];
        if(count($userRoles)>1){
            foreach($userRoles as $userRole){
                $assigneRoleName[] = $userRole->role;
            }
            return $assigneRoleName;
        }else{
            return $userRoles[0]->role;
        }
    }
    
    /**
     * Check user has assigned roles
     * @param type $role
     * @return type
     */
    public function hasRole($role){
        $isRole = false;
        $userRoles = $this->roles()->get();
        if(count($userRoles)>=1){
            foreach($userRoles as $userRole){
                if(
                    ($userRole->role == SUPER_ADMIN_ROLE && $role == SUPER_ADMIN_ROLE) || 
                    ($userRole->role == ROLE_BRANCH && $role == ROLE_BRANCH)
                )
                {
                    $isRole = true;
                }
            }
        }
        return $isRole;
    }
    
    public function userRole(){
        return $this->hasMany('app\RoleUser');
    }

    

    //Send password reset notification
    public function sendPasswordResetNotification($token) {

        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Display a listing of the Contient.
     *
     * @param Int $page 
     * @param String $sorting_on 
     * @param String $sorting_by (Possible value ASC or DESC)
     * @return \Illuminate\Http\Response
     */
    public static function getAllUser($perPage = '',$params=array()){
        if(isset($params['user_role']) && !empty($params['user_role'])){
            $roleName = $params['user_role'];
        }
        else{
            $roleName = ROLE_TEACHER;
        }
        
        $queryObj = self::whereHas('roles', function($q) use($roleName) {
            $q->where('roles.role',$roleName);
        });
        
        if(!empty($params['contact_no'])){
            $queryObj->where('contact_no','like',"%".$params['contact_no']."%");
        }
        if(!empty($params['name'])){
            $queryObj->where('name','like',"%".$params['name']."%");
        }
        if(!empty($params['email'])){
            $queryObj->where('email','like',"%".$params['email']."%");
        }
        if(!empty($params['status'])){
            $queryObj->where('status','=',$params['status']);
        }
        if(!empty($params['id'])){
            $queryObj->where('id','=',$params['id']);
        }

        if(isset($params['device_token_check']) && $params['device_token_check'] == 1){
            $queryObj->where('device_token','!=','');
        }
        
        $queryObj->orderBy(!empty($params['order_column'])?$params['order_column']:'id', !empty($params['order_dir'])?$params['order_dir']:'DESC');

       if(empty($perPage)){
           return $queryObj->get();
        }else{
           return $queryObj->paginate($perPage);
        }
    }
}
