<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Get all users by role
     * @return Collection of User
     */
    public function users(){
        return $this->hasMany('app\User','role_users', 'user_id', 'role_id');
    }
}
