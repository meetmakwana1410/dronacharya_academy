<?php

namespace app\Rules;

use Illuminate\Contracts\Validation\Rule;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportEmployeeInvalidFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(in_array($value->getMimeType(), ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel'])){
            $rpath = $value->getPathname();
            $spreadsheet = IOFactory::load($rpath);
            $worksheet = $spreadsheet->getActiveSheet();
            $highestColumn = $worksheet->getHighestColumn();
            $sheetHeader = $worksheet->rangeToArray(
                'A1:' . $highestColumn . 1, NULL, TRUE, FALSE
            );
            if(!empty($sheetHeader)){
               $sheetHeader = $sheetHeader[0]; 
            }
            $header_count = count($sheetHeader);
            if(!empty($sheetHeader) && $sheetHeader >= 8){
                $null_header = array();
                foreach($sheetHeader as $sh)
                {
                    if($sh == '')
                    {
                        $null_header[] = 0;
                    }
                    else
                    {
                        $null_header[] = 1;
                    }
                }
                if(in_array('1',$null_header)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Data in the File is invalid. Please follow the format as per Template Spreadsheet.';
    }
}
