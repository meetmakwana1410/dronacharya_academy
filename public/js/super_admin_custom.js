(function ($)
{
    "user strict";
    var dronacharyaSuperAdminCommon = function ()
    {
        var c = this;
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaSuperAdminCommon.prototype;
    
    c._initialize = function ()
    {
        if ($(window).width() > 1023){
            c._menuCollapseAfter($('#user_menu_collapse').val());
        }else{
            $('.sub-menu').addClass('open');
            ////$('body').removeClass('sidebar-collapse');
        }
        c._mainNavigation();
    };
    
    c._menuCollapse = function(){
        if ($(window).width() > 1023){
            console.log('Web');
            var menu_collapse_url = $('#menu_collapse_url').val();
            $.ajax({
                type: 'POST',
                url: menu_collapse_url,
                beforeSend: function ()
                {
                    DronaApp.dronacharyaCommon._showLoader();
                },
                success: function (data) {
                    c._menuCollapseAfter(data.menu_collapse);
                    DronaApp.dronacharyaCommon._hideLoader();
                }
            });
        }else{
            c._menuCollapseAfter('0');
        }
    };
    
    c._menuCollapseAfter = function(status){
        if ($(window).width() > 1023){
        if(status == '1'){
            $('.sub-menu').addClass('open');
            //$('body').removeClass('sidebar-collapse');
        }else{
            $('.sub-menu').removeClass('open');
            //$('body').addClass('sidebar-collapse');
        }
        }else{
            $('.sub-menu').toggleClass('open');
            //$('body').toggleClass('sidebar-collapse');   
        }
    };
    
    c._mainNavigation = function(){
        /* Main Navigation Toggle */
        $('#toggle_nav').click(function() {
            c._menuCollapse();
        });

        /* Main Navigation More Option */
        $('.more').click(function() {
            $(this).parent().siblings().find('.sub-menu').removeClass('open');
            $(this).next().toggleClass('open');
        });

        /* Main Navigation Custom Scroll */
        var mav_nav = $('.main-nav');
        if (mav_nav.length) {
            mav_nav.slimscroll({
                height: 'auto',
                alwaysVisible: false,
                railVisible: false,
                color: '#008ecc',
                opacity: 0.5
            });
        }
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaSuperAdminCommon = new dronacharyaSuperAdminCommon();
})(jQuery);