(function ($)
{
    "user strict";
    var dronacharyaAcademyHomeWork = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyHomeWork.prototype;
    
    c._initialize = function ()
    {
        c._listingView();
        $(document).on('click','.action-status',function(e){
            c._changeStatusConfirmation(this); 
            return false;
        });
        $(document).on('click','.delete-action',function(e){
            c._deleteConfirmation(this); 
            return false;
        });
    };
    
    c._listingView = function(){

        var field_coloumns = [
            null,
            null,
            {"orderable": false, "searchable": false},
            {"orderable": false, "searchable": false},
        ];
        var order_coloumns = [[3, "desc"]];
       
        DronaApp.dronacharyaCommon.table = DronaApp.dronacharyaCommon._generateDataTable(DronaApp.dronacharyaCommon.table,'homework_table','homework',field_coloumns,order_coloumns);
    };
   
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyHomeWork = new dronacharyaAcademyHomeWork();
})(jQuery);