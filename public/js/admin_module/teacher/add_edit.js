(function ($)
{
    "user strict";
    var dronacharyaAcademyTeacherAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyTeacherprototype;

    c._initialize = function ()
    {
        $('#formAddUpdateTeacher').on('submit',function(event) {
            $('#btnUpdateTeacher').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyTeacherAddEdit = new dronacharyaAcademyTeacherAddEdit();
})(jQuery);