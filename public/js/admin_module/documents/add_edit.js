(function ($)
{
    "user strict";
    var dronacharyaAcademyAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyTeacherprototype;

    c._initialize = function ()
    {
        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyAddEdit = new dronacharyaAcademyAddEdit();
})(jQuery);