(function ($)
{
    "user strict";
    var dronacharyaAcademyBranchAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyBranchprototype;

    c._initialize = function ()
    {
        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyBranchAddEdit = new dronacharyaAcademyBranchAddEdit();
})(jQuery);