(function ($)
{
    "user strict";
    var dronacharyaAcademyExamAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
            $(function () {
                $('#exam_date').datetimepicker({
                    format: 'DD-MM-YYYY',
                    //minDate: 'now'
                });

                $('#exam_time').datetimepicker({
                    format: 'LT'
                });

                $('#to_exam_time').datetimepicker({
                    format: 'LT'
                });

                $("#batch_id").change(function(e) {
                    var batchCode = $('option:selected', this).val();
                    
                    if(batchCode != ''){
                        $.ajax({
                            type: 'POST',
                            url: $('#url').val()+'/students/getBatchSubject',
                            data:{'batch_code':batchCode},
                            beforeSend: function ()
                            {
                                $('.loader-outer').show();
                            },
                            success: function (data) {
                                $('.loader-outer').hide();
                                //console.log(data.flag)
                                if(data.flag == 1){
                                    $('#subject_id').html(data.html);
                                    //$('#teacher_id').html(data.teacherDropHtml);
                                }else{
                                    $('#subject_id').html('');
                                    //$('#teacher_id').html('');
                                }
                            }
                        });
                    }else{
                        $('.loader-outer').hide();
                        $('#subject_id').html('');
                    }
                });

                $("#subject_id").change(function(e) {
                    var subjectId = $('option:selected', this).val();
                    var batchId = $("#batch_id").children("option:selected").val();
                    //alert(subjectId)
                    if(subjectId != ''){
                        
                        $.ajax({
                            type: 'POST',
                            url: $('#url').val()+'/exam/getSubjectTeacher',
                            data:{'subject_id':subjectId,'batch_id':batchId},
                            beforeSend: function ()
                            {
                                $('.loader-outer').show();
                            },
                            success: function (data) {
                                $('.loader-outer').hide();
                                //console.log(data.flag)
                                if(data.flag == 1){
                                    $('#teacher_id').html(data.teacherDropHtml);
                                }else{
                                    $('#teacher_id').html('');
                                }
                            }
                        });
                    }else{
                        $('.loader-outer').hide();
                        $('#teacher_id').html('');
                    }
                });
            });
        });
    };
    var c = dronacharyaAcademyExamAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdateExam').on('submit',function(event) {
            $('#btnUpdateExam').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyExamAddEdit = new dronacharyaAcademyExamAddEdit();
})(jQuery);