(function ($)
{
    "user strict";
    var dronacharyaAcademyExam = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyExam.prototype;
    
    c._initialize = function ()
    {
        c._listingView();
        $(document).on('click','.action-status',function(e){
            c._changeStatusConfirmation(this); 
            return false;
        });
        $(document).on('click','.delete-action',function(e){
            c._deleteConfirmation(this); 
            return false;
        });
    };
    
    c._listingView = function(){

        var field_coloumns = [
            null,
            null,
            null,
            {"orderable": true, "searchable": false},
        ];
        var order_coloumns = [[3, "desc"]];
       
        DronaApp.dronacharyaCommon.table = DronaApp.dronacharyaCommon._generateDataTable(DronaApp.dronacharyaCommon.table,'exam_table','exam',field_coloumns,order_coloumns);
    };
    
    c._changeStatusConfirmation = function(that){
        if($(that).is(':checked')){
            var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+'Please confirm that you wish to activate this records.'+DronaApp.dronacharyaCommon.dynamicPopupEnd;
        }else{
            var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+'Please confirm that you wish to de-activate this records.'+DronaApp.dronacharyaCommon.dynamicPopupEnd;
        }
        var status_url = $(that).data('action-href');
        //c._changeStatus(status_url);
        DronaApp.dronacharyaCommon._displayConfirmationMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html, c._changeStatus,status_url);
        return false;
    };
    
    c._changeStatus = function(status_url){
        $.ajax({
            type: 'POST',
            url: status_url,
            beforeSend: function ()
            {
                DronaApp.dronacharyaCommon._showLoader();
            },
            success: function (data) {
                var ele_id = $('#post_status_'+data.id);
                if(ele_id.is(':checked')){
                    ele_id.prop('checked',false);
                }else{
                    ele_id.prop('checked',true);
                }
                setTimeout(function(){ 
                    DronaApp.dronacharyaCommon._hideLoader();
                    DronaApp.dronacharyaCommon._displaySuccesMessagePopup(DronaApp.dronacharyaCommon.successLabel,DronaApp.dronacharyaCommon.dynamicPopupStart+data.message+DronaApp.dronacharyaCommon.dynamicPopupEnd); 
                }, 500);
                
            }
        });
        return false;  
    };
    
    c._deleteConfirmation = function(that){
        var content_html = '<p class="confirm-lbl">Please confirm that you wish to delete this post</p>';
        var status_url = $(that).data('action-href');
        DronaApp.dronacharyaCommon._displayConfirmationMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html, c._delete,status_url);
        return false;
    };
    
    c._delete = function(delete_url){
        //alert(delete_url)
        $.ajax({
            type: 'POST',
            url: delete_url,
            beforeSend: function ()
            {
                //DronaApp.meopinCommon._showLoader();
            },
            success: function (data) {
                DronaApp.dronacharyaCommon._hideLoader();
                DronaApp.dronacharyaCommon.table.draw();
                setTimeout(function(){ DronaApp.dronacharyaCommon._displaySuccesMessagePopup(DronaApp.dronacharyaCommon.successLabel,DronaApp.dronacharyaCommon.dynamicPopupStart+data.message+DronaApp.dronacharyaCommon.dynamicPopupEnd);}, 500);
            }
        });
        return false;  
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyExam = new dronacharyaAcademyExam();
})(jQuery);