(function ($)
{
    "user strict";
    var dronacharyaAcademyAcademicYearAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyAcademicYearAddEdit.prototype;

    c._initialize = function ()
    {
        DronaApp.dronacharyaCommon._CkEditorIntializeAll();
        DronaApp.dronacharyaCommon._CkEditorEvent();
        $('#formAddUpdateAcademicYear').on('submit',function(event) {
            $('#btnUpdateAcademicYear').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyAcademicYearAddEdit = new dronacharyaAcademyAcademicYearAddEdit();
})(jQuery);