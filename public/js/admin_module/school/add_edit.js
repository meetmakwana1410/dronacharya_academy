(function ($)
{
    "user strict";
    var dronacharyaAcademyFromAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyFromAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyFromAddEdit = new dronacharyaAcademyFromAddEdit();
})(jQuery);