(function ($)
{
    "user strict";
    var dronacharyaAcademyExamTypeAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyExamTypeAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyExamTypeAddEdit = new dronacharyaAcademyExamTypeAddEdit();
})(jQuery);