(function ($)
{
    "user strict";
    var dronacharyaAcademyAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyAddEdit.prototype;

    Object.defineProperty(c, 'options', {
        get: function () {
            return { 
                    dragMode: 'move',
                    responsive: true,
                    background: false, };
        }
    });

    Object.defineProperty(c, 'img', {
        set:function($img){
          $this = $img;
        },
        get:function()
        {
          return $this;
        }
    });

    Object.defineProperty(c, 'canvas', {
        set:function($canvas){
          $this = $canvas;
        },
        get:function()
        {
          return $this;
        }
    });

    Object.defineProperty(c, 'context', {
        set:function($context){
          $this = $context;
        },
        get:function()
        {
          return $this;
        }
    });

    Object.defineProperty(c, 'cropper', {
        set:function($cropper){
          $this = $cropper;
        },
        get:function()
        {
          return $this;
        }
    });

    c._initialize = function ()
    {
        //Choose image
        $('#image').on('change', function () {
            
            
            if (!$(this).val().match(/(?:jpg|jpeg|png|JPG|JPEG|PNG|jfif)$/)) {

                var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+"Select file is not a valid image, please choose correct image"+DronaApp.dronacharyaCommon.dynamicPopupEnd;
                DronaApp.dronacharyaCommon._displayErrorMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html);

                $('#image').val('');
                return false;
            }
            
              //here I CHECK if the FILE SIZE is bigger than 5 MB (numbers below are in bytes)
             if (this.files[0].size > 5242880 || this.files[0].fileSize > 5242880)
            {
                //show an alert to the user
               alert("Allowed file size exceeded. (Max. 5 MB)")

               //reset file upload control
                $('#image').val('');
                return false;
            }
            

            if (this.files && this.files[0]) {
                if (this.files[0].type.match(/^image\//)) {
                    var reader = new FileReader();
                    $("#image-crop-modal").modal({backdrop: 'static',
                        keyboard: false,
                        display: true});
                    options = {
                        dragMode: 'move',
                        responsive: true,
                        background: false, };
                    $("#ajaxLoader").addClass('show');
                    reader.onload = function (evt) {
                        canvas = $("#image-crop-canvas");
                        context = canvas.get(0).getContext("2d");
                        img = new Image();
                        img.onload = function () {
                            
                            $('#image').val('');
                            context.canvas.height = img.height;
                            context.canvas.width = img.width;
                            context.drawImage(img, 0, 0);
                            cropper = canvas.cropper(options);
                            $("#ajaxLoader").removeClass('show');

                        };
                        img.src = evt.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                } else {

                    var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+"Invalid file type! Please select an image file."+DronaApp.dronacharyaCommon.dynamicPopupEnd;
                    DronaApp.dronacharyaCommon._displayErrorMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html, c._logoRemove);
                    return false;

                }
            } else {
                alert('No file(s) selected.');
            }
        });

        //Close Popup
        $(".close-popup").on('click', function () {
            canvas.cropper("destroy")
            img = null;
            $("#image-crop-modal").modal('hide');
        });
        
        //manage ration
        $(document).on('click', '#toggle-aspect-ratio .btn', function () {
            $('#toggle-aspect-ratio .btn').each(function (i, v) {
                $(this).removeClass('active')
            })
            $(this).addClass('active')
            options.aspectRatio = $(this).attr('data-value');
            canvas.cropper('destroy').cropper(options);
        });
        
        //Crop image
        $(document).on('click', '.crop-image', function (event) {
            event.preventDefault();
            // Get a string base 64 data url
            var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
            var grid = $(".image-grid .image-container");
            var html = '';
            html += '<div class="crop-section">';
            html += '<div class="img-previewbox">';
            html += '<div class="img-previewbox-in">';
            html += '<img class="img-fluid" src="' + croppedImageDataURL + '"/>';
            html += '<a href="javascript:;" class="close-btn new_logo_remove">&times;</a>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            grid.html(html);
            canvas.cropper("destroy")
            img = null;

            var image_base64='';
            $(".image-container .crop-section").each(function(){
              image_base64 +="#"+$(this).find('img').attr('src');
            });
            dataString='&images='+image_base64;

            $('#default_logo').remove();
            $('.remove_client_logo').remove();

            $('#edit_image').removeAttr('src');
            $("#edit_image").remove();

            $("#image_name").val(dataString);
            $("#image-crop-modal").modal('hide');

        });
        
        //remove image
        $(document).on('click','.new_logo_remove',function(event){
            event.preventDefault();
            c.delete_image_add(this);
        })

        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });

        $('.remove_client_logo').on('click',function(event) {
            c.delete_image_confirm(this);
        });
    };
    
    c.delete_image_confirm = function(that)
    {
        var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+"Are you sure you want to delete this image?"+DronaApp.dronacharyaCommon.dynamicPopupEnd;
        DronaApp.dronacharyaCommon._displayConfirmationMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html, c._logoRemove);
        return false;
    };

    c.delete_image_add = function(that)
    {
        var content_html = DronaApp.dronacharyaCommon.dynamicPopupStart+"Are you sure you want to delete this image?"+DronaApp.dronacharyaCommon.dynamicPopupEnd;
        DronaApp.dronacharyaCommon._displayConfirmationMessagePopup(DronaApp.dronacharyaCommon.confirmLabel, content_html, c._addLogoRemove,that);
        return false;
    };

    c._logoRemove = function()
    {
        //alert('_logoRemove');
        $('#edit_image').attr('src',BASE_URL+'img/client-logo.jpg');
        // $('#edit_image').remove();
        $('.remove_client_logo').remove();
        $('#logo_remove_flag').val('1');
        
    };

    c._addLogoRemove = function(that)
    {
        //alert('_addLogoRemove');
        $(that).closest(".crop-section").remove();        
        $('.add_logo').html('<img src="'+BASE_URL+'img/client-logo.jpg" id="default_logo">');
        
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyAddEdit = new dronacharyaAcademyAddEdit();
})(jQuery);