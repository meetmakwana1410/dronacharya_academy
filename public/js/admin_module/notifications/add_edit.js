(function ($)
{
    "user strict";
    var dronacharyaAcademyNotiAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
            $(function () {
                $("#batch_id").change(function(e) {
                    var  batchCode = $('option:selected', this).val();
                    if(batchCode != ''){
                        $.ajax({
                            type: 'POST',
                            url: $('#url').val()+'notifications/getBatchStudent',
                            data:{'batch_code':batchCode},
                            beforeSend: function ()
                            {
                                $('.loader-outer').show();
                            },
                            success: function (data) {
                                $('.loader-outer').hide();
                                //console.log(data.flag)
                                if(data.flag == 1){
                                    $('#student_id').html(data.html);
                                }else{
                                    $('#student_id').html('');
                                }
                            }
                        });
                    }else{
                        $('.loader-outer').hide();
                        $('#student_id').html('');
                    }
                });

                $("#checkbox").click(function(){
                    if($("#checkbox").is(':checked') ){
                        $('#student_id').select2('destroy').find('option').prop('selected', 'selected').end().select2();
                    }else{
                        $('#student_id').select2('destroy').find('option').prop('selected', false).end().select2();
                    }
                });
            });
        });
    };
    var c = dronacharyaAcademyNotiAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdate').on('submit',function(event) {
            $('#btnUpdate').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyNotiAddEdit = new dronacharyaAcademyNotiAddEdit();
})(jQuery);