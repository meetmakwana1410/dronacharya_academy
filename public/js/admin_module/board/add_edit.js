(function ($)
{
    "user strict";
    var dronacharyaAcademyBoardAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyBoardAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdateBoard').on('submit',function(event) {
            $('#btnUpdateBoard').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyBoardAddEdit = new dronacharyaAcademyBoardAddEdit();
})(jQuery);