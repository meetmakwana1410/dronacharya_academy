(function ($)
{
    "user strict";
    var dronacharyaAcademyStandardsAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyStandardsAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdateStandards').on('submit',function(event) {
            $('#btnUpdateStandards').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyStandardsAddEdit = new dronacharyaAcademyStandardsAddEdit();
})(jQuery);