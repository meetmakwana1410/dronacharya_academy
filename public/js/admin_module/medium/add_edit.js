(function ($)
{
    "user strict";
    var dronacharyaAcademyMediumAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaAcademyMediumAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdateMedium').on('submit',function(event) {
            $('#btnUpdateMedium').prop('disabled',true);
        });
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyMediumAddEdit = new dronacharyaAcademyMediumAddEdit();
})(jQuery);