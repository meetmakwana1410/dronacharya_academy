(function ($)
{
    "user strict";
    var dronacharyaAcademyStudentsAddEdit = function ()
    {
        $(document).ready(function ()
        {
            c._initialize();
            $(function () {
                $('#student_date_of_birth').datetimepicker({
                    format: 'DD-MM-YYYY',
                    maxDate: 'now'
                });

                $('#admission_date').datetimepicker({
                    format: 'DD-MM-YYYY',
                });

                $("#batch_id").change(function(e) {
                    var  batchCode = $('option:selected', this).val();
                    if(batchCode != ''){
                        $.ajax({
                            type: 'POST',
                            url: $('#url').val()+'/students/getBatchSubject',
                            data:{'batch_code':batchCode},
                            beforeSend: function ()
                            {
                                $('.loader-outer').show();
                            },
                            success: function (data) {
                                $('.loader-outer').hide();
                                DronaApp.dronacharyaCommon._hideLoader();
                                if(data.flag == 1){
                                    $('#subjects').html(data.html);
                                }else{
                                    $('#subjects').html('');
                                }
                            }
                        });
                    }else{
                        $('.loader-outer').hide();
                        $('#subjects').html('');
                    }
                });
            });
        });
    };
    var c = dronacharyaAcademyStudentsAddEdit.prototype;

    c._initialize = function ()
    {
        $('#formAddUpdateStudents').on('submit',function(event) {
            $('#btnUpdateStudents').prop('disabled',true);
        });
    };

    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaAcademyStudentsAddEdit = new dronacharyaAcademyStudentsAddEdit();
})(jQuery);