(function ($)
{
    "user strict";
    var dronacharyaCommon = function ()
    {
        var c = this;
        this.commonDatePickerFormat = 'DD-MM-YYYY';
        this.commonTimePickerFormat = 'HH:mm';
        this.page_number = 1;
        $(document).ready(function ()
        {
            c._initialize();
        });
    };
    var c = dronacharyaCommon.prototype;
    var _table;
    Object.defineProperty(c, 'table', {
        set:function($table){
          _table = $table;
        },
        get:function()
        {
          return _table;
        }
    });
    var _ckEditorCount;
    Object.defineProperty(c, 'ckEditorCount', {
        set:function($ckEditorCount){
          _ckEditorCount = $ckEditorCount;
        },
        get:function()
        {
          return _ckEditorCount;
        }
    });
    var _ckEditorIntializeCount;
    Object.defineProperty(c, 'ckEditorIntializeCount', {
        set:function($ckEditorCount){
          _ckEditorIntializeCount = $ckEditorCount;
        },
        get:function()
        {
          return _ckEditorIntializeCount;
        }
    });
    Object.defineProperty(c, 'confirmLabel', {
        get: function () {
            return 'Confirm';
        }
    });
    Object.defineProperty(c, 'successLabel', {
        get: function () {
            return 'Success';
        }
    });
    Object.defineProperty(c, 'dynamicPopupStart', {
        get: function () {
            return '<div class="note">';
        }
    });
    Object.defineProperty(c, 'dynamicPopupEnd', {
        get: function () {
            return '</div>';
        }
    });
    
    c._initialize = function (){
        c._disableCopyPaste();
        c.ckEditorCount = $('.initckeditor').length;
        if(c.ckEditorCount == 0){
            c._hideLoader();
        }else{
            c.ckEditorIntializeCount = 0;
        }
        c._commonValidationEvent();
        c._ajaxSetUp();
        c._scrollToSuccessMessage();
    };
    
    c._CkEditorIntializeAll = function(){
        // Selectively replace <textarea> elements, based on custom assertions.
        CKEDITOR.replaceAll( function( textarea, config )
        {
            config.toolbar = [
        		{ name: 'document', items: [ 'Source' ] },// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        		[ 'Cut', 'Copy', 'Paste', 'PasteText',  '-', 'Undo', 'Redo' ],// Defines toolbar group without name.
        		//'/', // Line break - next group will be placed in new line.
        		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
                { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList' ] },
            ];
        } );
    };
    
    //CKEDITOR EVENT INTIALIZE
    c._CkEditorEvent = function(){
        CKEDITOR.on('instanceReady', c._afterCKEditorLoad);
    };
    
    //AFTER fully loaded CKEDITOR
    c._afterCKEditorLoad = function(){
        c.ckEditorIntializeCount = c.ckEditorIntializeCount + 1;
        if(c.ckEditorCount == c.ckEditorIntializeCount){
            c._hideLoader();
        }
    };
    
    //Loader to be show
    c._showLoader = function (){
        $('.loader-outer').show();
    };

    //Loader to be hide
    c._hideLoader = function (){
        $('.loader-outer').hide();
    };
    
    //Generate data table
    c._generateDataTable = function(table,element_id_name,ajax_URL,field_coloumns,order_coloumns,need_pagination,need_search){
        console.log(field_coloumns);
        var bPaginate = true;
        var bInfo = true;
        var bSearching = true;
        if (field_coloumns === undefined) {
            field_coloumns = [];
        } 
        if (order_coloumns === undefined) {
            order_coloumns = [[0, "desc"]];
        }
        if (need_pagination !== undefined) {
            bPaginate = need_pagination;
            bInfo = need_pagination;
        }
        if (need_search !== undefined) {
            bSearching = need_search;
        }
        var intial_url = 'http://';
        var intial_url2 = 'https://';
        var final_ajax_url = '';
        if(ajax_URL.indexOf(intial_url) != -1){
            final_ajax_url = ajax_URL;
        }else if(ajax_URL.indexOf(intial_url2) != -1){
            final_ajax_url = ajax_URL;
        }else{
            final_ajax_url = BASE_URL + ajax_URL;
        }
        table = $('#'+element_id_name).DataTable({
            "processing": true,
            "order": order_coloumns,
            "oLanguage": {
                "sProcessing":  '<img src="'+BASE_URL +'img/loader.gif" width="40">',
                "sEmptyTable":"No Record Found",
            },
            "lengthMenu": [10, 25, 50, 75, 100 ],
            "serverSide": true,
            "bInfo": bInfo,
            "autoWidth": false,
            "searching": bSearching,
            "orderCellsTop": true,
            //"fixedHeader": true,
            "stateSave":true,
            "columns": field_coloumns,
            "bPaginate":bPaginate,
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $(this).find('tr:first th:last').removeClass('sorting_asc').removeClass('sorting_desc');
            },
            fnDrawCallback: function (oSettings, json) {
               $(this).find('tr:first th:last').removeClass('sorting_asc').removeClass('sorting_desc');
            },
            initComplete: function () {

            },
            "ajax": {
                url: final_ajax_url,
                type: "post", // method  , by default get
                "data": function (d) {
                    d.page=(d.start+d.length)/d.length;
                    d.megha=d.start;
                   // d.search = $(".search").val();
                },
                "error":function(){
                    //window.location.reload();
                }
            }
        });
        DronaApp.dronacharyaCommon.table = table;
        if(bSearching){
            c._tableSearchInput(element_id_name);
        }
        $('#'+element_id_name+'_length').parent().parent().addClass('d-none');
        c._tableRecordPerPage();
        c._tableResetFilter();
        return table;
    };
    
    //Event added for table search
    c._tableSearchInput = function(element_id_name){
        $('#'+element_id_name+' thead tr').clone(true).appendTo( '#'+element_id_name+' thead' );
        $('#'+element_id_name+' thead tr:eq(1) th').not('.non-searchable').each( function (i) {
            var title = $(this).text();
            var elemvalue  = DronaApp.dronacharyaCommon.table.column(i).search();
            $(this).html( '<input type="text" placeholder ="Search" class="form-control custom-input-style1 datatable-search" value="'+elemvalue+'"/>' );
            $(this).removeClass('sorting');
            $(this).off('click');
            
            $( 'input', this ).on( 'keyup change', function () {
                var table = DronaApp.dronacharyaCommon.table;
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        $('#'+element_id_name+' thead tr:eq(1) th.non-searchable').each( function (i) {
            $(this).html( '' );
            $(this).removeClass('sorting');
            $(this).off('click');
        } );
    };
    
    //Event added for record per page
    c._tableRecordPerPage = function(){
        $(document).on('change','#record_perpage', function(event) {
            c._tableRecordPerPageDraw($(this).val());
        });
    };
    
    //Table Draw after set updated record per page
    c._tableRecordPerPageDraw = function(per_page){
        DronaApp.dronacharyaCommon.table.page.len( parseInt(per_page)).draw();
    };
    
    //Event added for record per page
    c._tableResetFilter = function(){
        $(document).on('click','#reset_btn', function(event) {
            c._tableResetFilterDraw();
        });
    };
    
    //Table Draw after reset table
    c._tableResetFilterDraw = function(){
        $('.datatable-search').val('');
        //DronaApp.dronacharyaCommon.table.colReorder.reset();
        var columns = DronaApp.dronacharyaCommon.table.columns();
        columns.each(function(col){
            for ( var i = 0, l = col.length; i < l; i++ ) {
                var searchValue = DronaApp.dronacharyaCommon.table.column(i).search();
                if(searchValue != ''){
                    DronaApp.dronacharyaCommon.table.column(i).search('').draw();
                }
            };
        });
        DronaApp.dronacharyaCommon.table.clear().draw();
    };
    
    //Disable Copy Paste
    c._disableCopyPaste = function(){
        $(document).on("cut copy paste",'.disable-copy-paste',function(e) {
            e.preventDefault();
        });
    };
    
    //Error message display
    c._displayErrorMessagePopup = function(title, message){
        if($('#dynamic_modal').hasClass('in'))
        {
            $("#dynamic_modal").modal("hide");
        }
        $("#dynamic_modal .title").html(title);
        $("#dynamic_modal .modal-body #dynamic_modal_content").html(message);
        $("#dynamic_modal .primary_dynamic_btn").hide();
        $("#dynamic_modal .secondary_dynamic_btn").html('Ok');
        $("#dynamic_modal").modal("show");
    };

    //Success message display
    c._displaySuccesMessagePopup = function(title, message, confirm_callback_function, callback_arg1, callback_arg2){
        if($('#dynamic_modal').hasClass('in'))
        {
            $("#dynamic_modal").modal("hide");
        }
        $("#dynamic_modal .title").html(title);
        $("#dynamic_modal .modal-body #dynamic_modal_content").html(message);
        $("#dynamic_modal .primary_dynamic_btn").hide();
        $("#dynamic_modal .primary_dynamic_btn").unbind("click");
        $("#dynamic_modal .secondary_dynamic_btn").html('Ok');
        $("#dynamic_modal .secondary_dynamic_btn").bind('click', function ()
        {
            //$(this).unbind();
            if (confirm_callback_function != '')
            {
                $("#dynamic_modal").modal("hide");
                var callbacks = $.Callbacks();
                callbacks.add(confirm_callback_function);
                callbacks.fire(callback_arg1, callback_arg2);
            }
        });
        $("#dynamic_modal").modal("show");
    };

    //Confirmation message display
    c._displayConfirmationMessagePopup = function(title, message, confirm_callback_function, callback_arg1, callback_arg2){
        if($('#dynamic_modal').hasClass('in'))
        {
            $("#dynamic_modal").modal("hide");
        }
        $("#dynamic_modal .title").html(title);
        $("#dynamic_modal .modal-body #dynamic_modal_content").html(message);
        $("#dynamic_modal .primary_dynamic_btn").show();
        $("#dynamic_modal .primary_dynamic_btn").unbind("click");
        $("#dynamic_modal .primary_dynamic_btn").html('Yes');
        $("#dynamic_modal .secondary_dynamic_btn").html('No');
        $("#dynamic_modal .primary_dynamic_btn").bind('click', function ()
        {
            if (confirm_callback_function != '')
            {
                $("#dynamic_modal").modal("hide");
                var callbacks = $.Callbacks();
                callbacks.add(confirm_callback_function);
                callbacks.fire(callback_arg1, callback_arg2);
            }
        });
        $("#dynamic_modal").modal("show");
    };
    
    //Multiselect dropdown
    c._multiSelectDropDown = function(elem_id,non_select_text,changeCallback){
        $('#'+elem_id).multiselect({
            nonSelectedText: non_select_text,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            templates: {
                filterClearBtn: '<div class="input-group-append"><button class="btn btn btn-outline-secondary multiselect-clear-filter" type="button"><i class="fa fa-close"></i></button></div>'
            },
            selectedClass: 'bg-light',
            onInitialized: function(select, container) {
                // hide checkboxes
                container.find('input[type=checkbox]').addClass('d-none');
                container.find('input[type=radio]').addClass('d-none');
            },
            onChange: function(option, checked) {
                c._showLoader();
                var opselected = $(option).val();
                if (checked == true && opselected){
                    this.$select.next().find('.parsley-errors-list').remove();
                }
                if(changeCallback !== undefined){
                    var callbacks = $.Callbacks();
                    callbacks.add(changeCallback);
                    callbacks.fire();
                }
                setTimeout(function(){ 
                    c._removeMultiSelectDropDownSpace();
                    setTimeout(function(){c._hideLoader();}, 2);
                }, 2);
            },
            onDropdownHidden:function(event) {
                c._removeMultiSelectDropDownSpace();
                if($('#'+elem_id+' option:selected').length){
                    $('.parsley-errors-list').remove();
                }
            }
        });  
    };
    
    //Remove space after selecting multiselct n-level values
    c._removeMultiSelectDropDownSpace = function(){
        $('.multiselect-selected-text').each(function(){
            var valued = $(this).html().replace( /&nbsp;/g,'' );
            $(this).html(valued);
        });
    };
    
    //Custom parsley validation event
    c._customParsleyValidation = function(){
        window.Parsley.addValidator('excelFileAccept', {
            requirementType: 'string',
            validateString: function(value, requirement) {
                var allowedFiles = [".doc", ".docx", ".pdf"];
                return 0 === value % requirement;
            },
            messages: {
                en: 'This value should be a multiple of %s',
            }
        });  
    };
    
    //Common custom validation event
    c._commonValidationEvent = function(){
        $(document).on('keypress','.mobile_number_validation',function(e){
            return c._mobileNumberValidation(e,this);
        });
        
        $(document).on('keyup keypress','.alphanumeric', function(e) {
            var result = c._alphaNumericValidation(e,this);
            return result;
        });

        $(document).on('keyup keypress','.alphachar', function(e) {
            var result = c._alphaCharValidation(e,this);
            return result;
        });

        $(document).on('keyup keypress','.onlychar', function(e) {
            var result = c._onlyCharValidation(e,this);
            return result;
        });
        
        $(document).on('keyup keypress','.onlynumbers', function(e) {
            var result = c._onlyNumberValidation(e,this);
            return result;
        });
        
        $(document).on('keyup keypress','.restrictspecialcharexcept', function() {
            var $th = $(this);
            $th.val( $th.val().replace(/[^a-zA-Z0-9-_+-,. ]/g, function(str) {  return ''; } ) );
        });
        
        $(document).on('keyup keypress','.restrictspecialcharexceptemail', function() {
            var $th = $(this);
            $th.val( $th.val().replace(/[^a-zA-Z0-9-_+-,.@ ]/g, function(str) {  return ''; } ) );
        });

        $(document).on('keyup keypress','.onlycharwithnumber', function() {
            var $th = $(this);
            $th.val( $th.val().replace(/[^a-zA-Z0-9 ]/g, function(str) {  return ''; } ) );
        });

        $('.onlyfloatvalue').keypress(function(eve) {
            if (eve.which != 8 && (eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0)) {
                eve.preventDefault();
            }
            // this part is when left part of number is deleted and leaves a . in the leftmost position. For example, 33.25, then 33 is deleted
            $('.onlyfloatvalue').keyup(function(eve) {
                if ($(this).val().indexOf('.') == 0) {
                    $(this).val($(this).val().substring(1));
                }
            });

            $('.onlyfloatvalue').focusout(function(eve) {
                if ($(this).val().indexOf('.') == 0) {
                    $(this).val($(this).val().substring(1));
                }
            });
        });

        $(document).on('keyup keypress','.notallowallzero', function() {
            var $th = $(this);
            $th.val( $th.val().replace(/^[0|\D]*/, function(str) {  return ''; } ) );
        });

        $(document).on('keypress','.notallowspace', function(e) {
            if(e.which === 32) 
                return false;
        });

        $(document).on('keypress keyup','.muststartwithalpha', function(e) {
            var $th = $(this);
            if($th.val().length <= 1){
              $th.val( $th.val().replace(/[^a-zA-Z]/g, function(str) {  return ''; } ) );
            }
        });

    };

    //Alpha Numeric validation 
    c._alphaNumericValidation = function(evt , that){
        var regex = new RegExp("^[a-zA-Z0-9 _-]+$");
        if(evt.charCode == 0){
            return true;
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(evt.charCode == 13){
            return true;
        }
        var key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
        if (!regex.test(key)) {
            evt.preventDefault();
            return false;
        }
        return true;
    };

    // Only char validation
    c._onlyCharValidation = function(evt , that){
        var regex = new RegExp("^[a-zA-Z ]+$");
        if(evt.charCode == 0){
            return true;
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(evt.charCode == 13){
            return true;
        }
        var key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
        if (!regex.test(key)) {
            evt.preventDefault();
            return false;
        }
        return true;
    };
    
    //Only number validation
    c._onlyNumberValidation = function(evt , that){
        var regex = new RegExp("^[0-9]+$");
        if(evt.charCode == 0){
            return true;
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(evt.charCode == 13){
            return true;
        }
        var key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
        if (!regex.test(key)) {
            evt.preventDefault();
            return false;
        }
        return true;
    };

    //Alpha char validation
    c._alphaCharValidation = function(evt , that){
        var regex = new RegExp("^[a-zA-Z -]+$");
        if(evt.charCode == 0){
            return true;
        }
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(evt.charCode == 13){
            return true;
        }
        var key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
        if (!regex.test(key)) {
            evt.preventDefault();
            return false;
        }
        return true;
    };
    
    //Mobile Number validation
    c._mobileNumberValidation = function(evt,that){
        var charCode = (evt.which) ? evt.which : evt.keyCode 
        var mobile_length = $(that).val().length;
        if (mobile_length == 0 && charCode == 48 )
        {
            return false;
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        {
            return false;
        }
        return true;
    };
    
    //Common ajax set up for csrf token pass and on error reload page
    c._ajaxSetUp = function(){
        //Global ajax csrf token pass
        $.ajaxSetup({
            data: {
                _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //c._hideLoader();
                //window.location.reload();
            }
        });
    };
    
    //Parsley form validation true or false
    c._parsleyFormValidate = function(form_name){
        var $form = $(form_name);
        return $form.parsley().validate();
    };
    
    c._scrollToSuccessMessage = function(){
        if($('#session_alert').is(':visible')){
            if(!$('#session_alert').isOnScreen()){
                $('html, body').animate({
                    scrollTop: $("#session_alert").offset().top
                }, 1000);
            }
        }
    };
    
    window.DronaApp = window.DronaApp || {};
    window.DronaApp.dronacharyaCommon = new dronacharyaCommon();
})(jQuery);

$.fn.isOnScreen = function(){
    
    var win = $(window);
    
    var viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    
};

function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode != 45  && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }