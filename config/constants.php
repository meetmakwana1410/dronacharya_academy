<?php 

define('SITE_URL','/');
define('PUBLIC_FOLDER_URL','public');
define('CSS_FOLDER_URL','css/');
define('ADMIN_CSS_FOLDER_URL','css/admin/');
define('FRONT_CSS_FOLDER_URL','css/front/');
define('JS_FOLDER_URL','js/');
define('JS_ADMIN_MODULE_FOLDER_URL','js/admin_module/');
define('JS_FRONT_FOLDER_URL','js/front/');
define('IMAGES_FOLDER_URL','img/');
define('UPLOADS_FOLDER_URL','uploads');
define('VERIFY_USER_URL','verifyuser');
define('PASSWORD_RESET_URL','password/reset');
define('DATE_TIME_FORMAT','m/d/Y h:i a');
define('EMPLOYEE_IMPORT_FORM_ADD_EDIT_NAME','add_edit_form');
define('EMPLOYEE_IMPORT_FORM_IMPORT_NAME','import_form');
define('SUPER_ADMIN_ROLE','super-admin');
define('ROLE_TEACHER','teacher');
define('ROLE_BRANCH','branch-admin');
define('ERROR_STATUS','0');
define('SUCCESS_STATUS','1');

define("SECOND", 1);
define("MINUTE", 60 * SECOND);
define("HOUR", 60 * MINUTE);
define("DAY", 24 * HOUR);
define("MONTH", 30 * DAY);
define("WEEK", 7 * DAY);
define("ADMIN_ID", '1');

define("PAGINATION_LIMIT", 10);
?>